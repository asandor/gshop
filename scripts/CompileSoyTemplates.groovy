import com.google.template.soy.SoyFileSet
import com.google.template.soy.jssrc.SoyJsSrcOptions
import groovy.io.FileType
import org.apache.commons.io.FilenameUtils

target(main: "Compile all Soy template files in web-app directory to Javascript") {
	new File('web-app').eachFileRecurse(FileType.FILES, { File templateFile ->
		if (templateFile.name =~ /.+\.soy$/) {
			println "Compiling template $templateFile.canonicalPath"

			def builder = new SoyFileSet.Builder()
			builder.add templateFile
			def sfs = builder.build()
			List<String> compileToJsSrc = sfs.compileToJsSrc(new SoyJsSrcOptions(), null)

			File target = new File(templateFile.parent, FilenameUtils.getBaseName(templateFile.name) + '.js')
			def w = target.newWriter()
			w << compileToJsSrc.join("\n")
			w.close()
		}
	})
}

setDefaultTarget(main)
