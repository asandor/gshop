import gshop.i18n.LocalizationJsSrcGenerator

target(main: "Compile Javascript localization files from resource bundles") {
	i18nDir = new File('grails-app/i18n')
	if (!i18nDir.exists()) throw new Exception("Cannot find i18n (grails-app/i18n) directory, script is probably running with incorrect work dir (${new File('.').canonicalPath})")
	basePackage = new File('src/groovy/gshop')
	if (!basePackage.exists()) throw new Exception("Cannot base package (src/groovy/gshop), script is probably running with incorrect work dir (${new File('.').canonicalPath})")
	jsI18nDir = new File(basePackage, "i18n")
	jsI18nDir.mkdir()

	LocalizationJsSrcGenerator generator = new LocalizationJsSrcGenerator()
	generator.generateJavascriptLocalizationFiles(i18nDir, jsI18nDir)
}

setDefaultTarget(main)