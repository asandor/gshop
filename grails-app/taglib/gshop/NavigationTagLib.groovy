package gshop

import gshop.catalogcache.CCCategory
import gshop.navigation.SeoLink
import gshop.navigation.SeoService
import gshop.product.Category
import gshop.product.Product
import org.springframework.util.Assert
import org.springframework.web.servlet.support.RequestContextUtils as RCU

class NavigationTagLib {

	static namespace = 'shop'

	SeoService seoService

	def path = { params ->
		String path
		SeoLink seoLink = seoService.getSeoLink(params.target, RCU.getLocale(request))
		if (seoLink) {
			path = seoLink.path
		} else {
			switch (params.target.class) {
				case Product: path = "product/$params.target.id"; break;
				case Category: path = "category/$params.target.id"; break;
			}
		}

		out << path
	}

	def link = { params, body ->
		Assert.notNull(params.target, "Parameter 'target' not defined")
		if (params.target instanceof CCCategory) params.target = Category.read(params.target.dbId)

		out << '<a href="'
		out << g.createLink(uri: '/')
		out << shop.path(target: params.target)
		out << '">'
		out << body()
		out << '</a>'
	}

	def breadcrumb = { params ->
		Assert.notNull(params.category)
		Assert.isInstanceOf(Category, params.category)

		List<Category> path = getPathToRoot(params.category)
		out << render(template: '/main/breadcrumb', model: [path: path])
	}

	private List<Category> getPathToRoot(Category current) {
		List<Category> path = []
		while (current != null) {
			path << current
			current = current.parent
		}
		return path.reverse()
	}

}
