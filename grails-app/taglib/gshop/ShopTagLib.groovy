package gshop

class ShopTagLib {

	static namespace = "shop"

	CountryService countryService

	def pageTitle = { params, body ->
		out << g.message(code: 'shop.name')
		out << ' - '
		out << body()
	}

	def countries = {
		out << 'countries = ['
		countryService.getCountries().eachWithIndex { country, index ->
			out << "{code: '${country.name()}', display: '${countryDisplay(country: country)}'}"
			if (index < countryService.getCountries().size() - 1) out << ", "
		}
		out << '];'
	}

	def countryDisplay = { params ->
		out << g.message(code: 'countrycode.' + params.country.name())
	}
}
