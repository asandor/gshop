package gshop

import org.springframework.util.Assert
import gshop.product.Product
import gshop.shop.CurrencyService

class ProductTagLib {

	static namespace = "shop"

	CurrencyService currencyService

	def productTile = { params, body ->
		Assert.notNull(params.product)
		Assert.isInstanceOf(Product, params.product)

		def model = [
				product: params.product,
				favouriteProduct: false
		]
		out << render(template: '/product/productTile', model: model)
	}

	def productTileView = { params, body ->
		def model = [
				products: params.products,
				pageSize: 15
		]
		out << render(template: '/product/productTileView', model: model)
	}

	def productPrice = { params ->
		Assert.notEmpty(params.product)

		Product product = params.product
		List<BigDecimal> prices = [product.price]
		List<Range<Integer>> ranges = [(1..1)] //will be ignored if there are no volume discounts
		product.volumeDiscounts.sort { it.fromAmount }.eachWithIndex { vd, i ->
			if (vd.percentage) prices.add(product.price - (product.price * (vd.percentage / 100)))
			else prices.add(vd.fixValue)

			if (i == 0) ranges[0].to = vd.fromAmount - 1 //set the to value of the first range (range for base price)

			ranges << (vd.fromAmount..(vd.toAmount ?: vd.fromAmount))
		}

		def model = [
				prices: prices,
				ranges: ranges,
				currency: currencyService.getCurrency()
		]
		out << render(template: '/product/productPrice', model: model)
	}

	def basketPanel = { params ->
		Assert.notNull(params.product)

		def model = [
				product: params.product
		]
		out << render(template: '/product/basketPanel', model: model)
	}
}
