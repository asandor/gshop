package gshop

import gshop.catalog.CatalogService
import org.springframework.util.Assert
import org.springframework.web.servlet.support.RequestContextUtils

class CategoryTagLib {

	CatalogService databaseCatalogService

	static namespace = "shop"

	def categoryMenu = { params ->
		def model = [
				categories: databaseCatalogService.getTopCategories(null, RequestContextUtils.getLocale(request).language),
				selectedCategory: params.selectedCategory
		]
		out << render(template: '/category/categoryMenu', model: model)
	}

	def topCategory = { params ->
		Assert.notNull(params.category, "Parameter 'category' not defined")

		def model = [
				category: params.category,
				subCategories: databaseCatalogService.getSubCategories(params.category, null, RequestContextUtils.getLocale(request).language),
				selectedCategory: params.selectedCategory
		]
		out << render(template: '/category/topCategory', model: model)
	}

	def categoryTileView = { params ->
		def model = [
				categories: databaseCatalogService.getSubCategories(params.category, null, RequestContextUtils.getLocale(request).language)
		]
		out << render(template: '/category/categoryTileView', model: model)
	}

	def categoryTile = { params ->
		Assert.notNull(params.category)

		def model = [
				category: params.category
		]
		out << render(template: '/category/categoryTile', model: model)
	}
}
