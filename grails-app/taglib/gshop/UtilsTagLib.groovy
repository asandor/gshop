package gshop

class UtilsTagLib {

	static namespace = "shop"

	def flashError = { params ->
		out << render(template: '/other/flashError')
	}
}
