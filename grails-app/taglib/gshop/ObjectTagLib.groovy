package gshop

import gshop.order.OrderProduct
import gshop.product.Category
import gshop.product.ObjectImageType
import gshop.product.Product
import gshop.product.ShopObject
import org.springframework.util.Assert
import org.springframework.web.servlet.support.RequestContextUtils as RCU

class ObjectTagLib {

	static namespace = 'shop'

	ObjectImageService objectImageService

	def objectImageUrl = { params ->
		Assert.notNull params.object
		Assert.notNull params.type
		def object = params.object
		ObjectImageType imageType = params.type

		String imageName = null
		if (object instanceof Product || object instanceof OrderProduct) {
			imageName = object.code + '.' + (object.imageExtension ?: 'jpg')
		} else if (object instanceof Category) {
			imageName = (imageType == ObjectImageType.SMALL) ? object.smallImageName : object.mediumImageName
		}
		assert imageName, "Unknown ShopObject subtype: ${object.getClass().simpleName}"

		File imageFile = objectImageService.resolveImageLocalPath(imageName, imageType)
		if (imageFile.exists()) {
			def urlParams = [type: imageType.code]
			out << g.createLink(controller: 'objectImage', action: 'getImage', id: imageName, params: urlParams)
		} else {
			out << g.createLink(controller: 'objectImage', action: 'getImage', id: ObjectImageService.NO_IMAGE_ID)
		}
	}

	def objectImage = { params ->
		Assert.notNull params.object
		Assert.notNull params.type
		ShopObject object = params.object
		ObjectImageType imageType = params.type

		String imageName = null
		if (object instanceof Product) {
			imageName = object.code + '.' + object.imageExtension
		} else if (object instanceof Category) {
			imageName = (imageType == ObjectImageType.SMALL) ? object.smallImageName : object.mediumImageName
		}
		assert imageName, "Unknown ShopObject subtype: ${object.getClass()}"

		File imageFile = objectImageService.resolveImageLocalPath(imageName, imageType)
		if (imageFile.exists()) {
//			def style = "background: url($imgUrl) no-repeat center center; height: ${imageType.height}px; width: ${imageType.width}px;"
			out << "<div class=\"object-image\" style=\"background-image: url("
			out << shop.objectImageUrl(params)
			out << "); height: ${imageType.height}px;\"></div>"
		} else {
			out << '<img src="' + g.createLink(controller: 'objectImage', action: 'getImage', id: ObjectImageService.NO_IMAGE_ID) + '"/>'
		}
	}

	def title = { params ->
		Assert.notNull params.object

		Locale locale = RCU.getLocale(request)
		out << params.object.getTitle(locale.language)
	}

	def longDescription = { params ->
		Assert.notNull params.object

		Locale locale = RCU.getLocale(request)
		out << params.object.getLongDescription(locale.language)
	}

	def shortDescription = { params ->
		Assert.notNull params.object

		Locale locale = RCU.getLocale(request)
		out << params.object.getShortDescription(locale.language)
	}
}
