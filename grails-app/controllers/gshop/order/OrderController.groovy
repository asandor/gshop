package gshop.order

import grails.converters.JSON
import gshop.ClientDataTransformer
import gshop.basket.Basket
import gshop.basket.BasketItem
import gshop.basket.BasketService
import gshop.basket.Price
import gshop.product.ObjectImageType
import gshop.security.Customer
import gshop.security.UserService
import gshop.shop.PricingService
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.servlet.support.RequestContextUtils

class OrderController {

	PricingService pricingService

	BasketService basketService

	UserService userService

	OrderService orderService

	ClientDataTransformer clientDataTransformer

	@Value('${shop.vat}')
	BigDecimal vat

	def placeOrder() {
		log.debug "Received request for new order"

		Basket basket = basketService.getCurrentBasket(session)
		Customer customer = userService.getCurrentCustomer()
		if (customer == null) throw new IllegalStateException("No customer in session")
		if (basket == null) throw new IllegalStateException("Order placed with no basket active")
		if (!basket.basketItems) throw new IllegalArgumentException("Ordering with empty basket")
		if (!customer.deliveryAddresses) throw new IllegalStateException("Ordering without a delivery address")

		ShipmentMethod shipmentMethod = ShipmentMethod.findByUid(params.shipmentMethodId)
		if (shipmentMethod == null) throw new IllegalArgumentException("ShipmentMethod with uid '$orderData.shipmentMethod' not found")
		PaymentMethod paymentMethod = PaymentMethod.findByUid(params.paymentMethodId)
		if (paymentMethod == null) throw new IllegalArgumentException("PaymentMethod with uid '$orderData.paymentMethod' not found")

		pricingService.calculateBasketPrices(basket, getClientDataTransformer())
		Order order = orderService.placeOrder(basket, customer, shipmentMethod, paymentMethod, RequestContextUtils.getLocale(request))

		if (basket.id) basket.delete()
		session.basket = null

		render order.uid
	}

	def orderMethods() {
		def methods = [:]
		methods.shipmentMethods = ShipmentMethod.list().collect { sm ->
			Price price = pricingService.calculateShipmentMethodPrice(sm, basketService.getCurrentBasket(session))
			return getClientDataTransformer().transformShipmentMethod(sm)
		}
		methods.paymentMethods = PaymentMethod.list().collect { pm ->
			Price price = pricingService.calculatePaymentMethodPrice(pm, basketService.getCurrentBasket(session))
			return getClientDataTransformer().transformPaymentMethod(pm)
		}
		methods.validCombinations = ValidPaymentShipmentCombination.list().collect { combo ->
			[
					paymentMethod: combo.paymentMethod.uid,
					shipmentMethod: combo.shipmentMethod.uid
			]
		}
		render methods as JSON
	}


	def summary() {
		log.debug "Received request for order summary"

		Basket basket = basketService.getCurrentBasket(session)
		Customer customer = userService.getCurrentCustomer()
		if (customer == null) throw new IllegalStateException("No customer in session")
		if (basket == null) throw new IllegalStateException("Missing basket in session for creating order summary")
		if (!basket.basketItems) throw new IllegalArgumentException("Cannot render order summary with empty basket")

		ShipmentMethod shipmentMethod = ShipmentMethod.findByUid(params.shipmentMethodId)
		if (shipmentMethod == null) throw new IllegalArgumentException("ShipmentMethod with uid '$orderData.shipmentMethod' not found")
		PaymentMethod paymentMethod = PaymentMethod.findByUid(params.paymentMethodId)
		if (paymentMethod == null) throw new IllegalArgumentException("PaymentMethod with uid '$orderData.paymentMethod' not found")

		pricingService.calculateBasketPrices(basket, getClientDataTransformer())
		pricingService.calculateShipmentMethodPrice(shipmentMethod, basket)
		pricingService.calculatePaymentMethodPrice(paymentMethod, basket)
		Price finalPrice = pricingService.calculateOrderPrice(basket, shipmentMethod, paymentMethod)
		def orderSummary = [
				products: [
						amount: basket.basketItems.size(),
						price: getClientDataTransformer().renderPriceValue(basket.basketItems.sum { it.price.baseWithVat })
				],
				finalPrice: getClientDataTransformer().renderPrice(finalPrice),
				shipmentMethod: getClientDataTransformer().transformShipmentMethod(shipmentMethod),
				paymentMethod: getClientDataTransformer().transformPaymentMethod(paymentMethod),
				billingAddress: getClientDataTransformer().transformAddress(customer.getBillingAddress())
		]
		if (customer.deliveryAddress) {
			orderSummary.deliveryAddress = getClientDataTransformer().transformAddress(customer.deliveryAddress)
		}

		render orderSummary as JSON
	}

	def overview() {
		Customer customer = userService.getCurrentCustomer()
		if (customer == null) throw new IllegalStateException("No customer in session")

		def orders = Order.executeQuery("SELECT o FROM Order o WHERE o.customer.username = :username", [username: customer.username])

		def orderList = orders.collect { order ->
			[
					g.formatDate(date: order.dateCreated, type: "datetime", style: "SHORT"),
					order.uid,
					order.productCount,
					order.price.baseWithVat,
					[
							value: order.state.name(),
							text: g.message(code: 'order.state.' + order.state.name())
					]
			]
		}

		render orderList as JSON
	}

	def details() {
		Customer customer = userService.getCurrentCustomer()
		if (customer == null) throw new IllegalStateException("No customer in session")

		def order = Order.findByUid(params.orderId)
		if (order == null) throw new IllegalArgumentException("Cannot find order '${params.orderId}'")

		def result = [
				dateCreated: g.formatDate(date: order.dateCreated, type: "datetime", style: "SHORT"),
				uid: order.uid,
				shipmentMethodTitle: order.shipmentMethodTitle,
				shipmentMethodPrice: order.shipmentMethodPrice,
				paymentMethodTitle: order.paymentMethodTitle,
				paymentMethodPrice: order.paymentMethodPrice,
				discountInfo: "",
				billingAddress: getClientDataTransformer().transformAddress(order.billingAddress),
				state: [
						value: order.state.name(),
						text: g.message(code: 'order.state.' + order.state.name())
				],
				paymentReceived: g.message(code: order.paymentReceived.toString()),
				price: [
						base: order.price.base,
						vatPercent: order.price.vatPercent,
						vatAmount: order.price.vatAmount,
						baseWithVat: order.price.baseWithVat
				]
		]
		if (order.deliveryAddress) {
			result.deliveryAddress = getClientDataTransformer().transformAddress(order.deliveryAddress)
		}
		result.products = order.products.collect { prod ->
			[
					shop.objectImageUrl(object: prod, type: ObjectImageType.SMALL),
					prod.amount,
					prod.code,
					prod.title,
					prod.price.baseWithVat,
					""
			]
		}

		render result as JSON
	}

	private ClientDataTransformer getClientDataTransformer() {
		clientDataTransformer.grailsTagLib = g
		clientDataTransformer.shopTagLib = shop
		return clientDataTransformer
	}
}
