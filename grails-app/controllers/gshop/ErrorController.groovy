package gshop

import org.codehaus.groovy.grails.web.errors.GrailsWrappedRuntimeException
import gshop.basket.BasketController
import gshop.security.UserController

class ErrorController {

	/**
	 * Controllers that shouldn't generate an error page for exceptions but just return 'Internal Server Error'
	 */
	static final List<String> ajaxControllers = [BasketController.simpleName, UserController.simpleName]

	def handleGenericError() {
		GrailsWrappedRuntimeException exception = request.getAttribute('exception') as GrailsWrappedRuntimeException
		if (exception.className in ajaxControllers) {
			render "Internal Server Error"
		} else {
			render view: '/error'
		}
	}
}
