package gshop

import grails.converters.JSON
import gshop.basket.Basket
import gshop.basket.BasketService
import gshop.navigation.ClientUrlMappings
import gshop.security.User
import gshop.security.UserService
import gshop.shop.PricingService
import org.springframework.core.io.ClassPathResource

class InitController {

	UserService userService

	BasketService basketService

	PricingService pricingService

	ClientDataTransformer clientDataTransformer

	def fetchInitData() {
		List moduleNames = request.JSON.modules

		Map result = [:]

		User user = userService.getCurrentUser()

		moduleNames.each { name ->
			switch (name) {
				case 'user':
					if (user) {
						result.user = clientDataTransformer.transformUser(user)
					} else {
						result.user = [:]
					}
					break
				case 'basket':
					Basket basket = basketService.getCurrentBasket(session)
					pricingService.calculateBasketPrices(basket, getClientDataTransformer())
					result.basket = clientDataTransformer.transformBasket(basket)
					break
			}
		}

		render result as JSON
	}

	def urlMappingScript() {
		new ClassPathResource("/gshop/navigation/UrlMappings.js").inputStream.withReader { reader ->
			response.setContentType('text/javascript')
			response.setCharacterEncoding('UTF-8')

			def mappings = ClientUrlMappings.getClientUrlMappings(g, userService.getCurrentUser())
			def jsonMappings = new JSON(mappings).toString(true)
			render reader.text.replace('${mappings}', jsonMappings)
		}
	}

	private ClientDataTransformer getClientDataTransformer() {
		clientDataTransformer.grailsTagLib = g
		clientDataTransformer.shopTagLib = shop
		return clientDataTransformer
	}
}