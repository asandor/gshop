package gshop

import gshop.navigation.CategorySeoLink
import gshop.navigation.ErrorMessage
import gshop.navigation.ProductSeoLink
import gshop.navigation.SeoLink
import gshop.product.Product

class MainController {

	def index() {
		if (!params.path) {
			render(view: 'index')
			return
		}

		Object urlObject = getObjectAtUrl(params.path)
		if (urlObject == null) {
			flash.error = new ErrorMessage('seo.error.unknown-link', params.path)
			response.setStatus(301)
			redirect url: '/'
		} else if (urlObject instanceof gshop.product.Category) {
			forward(controller: 'category', action: 'index', id: urlObject.id, params: [path: params.path])
		} else if (urlObject instanceof Product) {
			throw new Exception("Implement this by highlighting the selected product on the category screen")
		}
	}

	private Object getObjectAtUrl(String path) {
		SeoLink seoLink = SeoLink.findByPath(path, [cache: true])
		if (seoLink instanceof CategorySeoLink) {
			return seoLink.category
		} else if (seoLink instanceof ProductSeoLink) {
			return seoLink.product
		} else if (seoLink == null) {
			return null
		} else {
			throw new IllegalStateException("Unknown SeoLink type: ${seoLink.class}")
		}
	}
}
