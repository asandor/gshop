package gshop.admin

import groovy.xml.MarkupBuilder

class SettingsController {

	def grailsApplication

	def show() {
		StringWriter writer = new StringWriter()
		def mb = new MarkupBuilder(writer)
		def printSetting = { name, func ->
			tr {
				td(name + ': ')
				td(func() ?: '---')
			}
		}
		printSetting.delegate = mb

		mb.table {
			printSetting('shop.object_image_directory', { grailsApplication.config.shop.object_image_directory })
		}
		render writer.toString()
	}

}