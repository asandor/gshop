package gshop.admin

import grails.converters.JSON
import org.apache.commons.lang.StringUtils
import gshop.DatabaseCatalogService
import gshop.localization.ProductStrings
import gshop.product.Category
import gshop.product.Product
import org.springframework.web.servlet.support.RequestContextUtils

class CatalogManagementController {

	DatabaseCatalogService databaseCatalogService

	def index() {
		render(view: '/admin/catalog/index')
	}

	def categories() {
		def renderedTree = [
				id: 0,
				item: databaseCatalogService.getTopCategories(null, RequestContextUtils.getLocale(request).language)[4..4].collect { Category category ->
					renderCategory(category)
				}
		]

		render renderedTree as JSON
	}

	private renderCategory(Category category) {
		boolean hasChildren = category.children.size() > 0
		def result = [
				id: category.getExtId(),
				text: shop.title(object: category),
				child: hasChildren ? 1 : 0
		]
		if (hasChildren) {
			result.item = databaseCatalogService.getSubCategories(category, null, RequestContextUtils.getLocale(request).language).collect { Category child ->
				renderCategory(child)
			}
		}
		return result
	}

	def categoryDetails(String catId) {
		if (StringUtils.isBlank(catId)) throw new AssertionError("Missing parameter catId");

		Category category = databaseCatalogService.getCategoryByExtId(catId)
		if (category == null) throw new AssertionError("Category $catId not found")

		Collection<Product> products = databaseCatalogService.getProductsInCategory(category, null, RequestContextUtils.getLocale(request).language)
		List<Map> productData = products.collect { Product product ->
			renderProduct(product)
		}
		Map response = [products: productData]
		render response as JSON
	}

	private Map renderProduct(Product product) {
		ProductStrings strings = product.strings.find { it.localeId == RequestContextUtils.getLocale(request).language }
		[
				title: strings.title,
				code: product.code,
				shortDescription: strings.shortDescription,
				longDescription: strings.longDescription,
				imageExtension: product.imageExtension,
				price: renderPrice(product)
		]
	}

	private String renderPrice(Product product) {
		return product.price as String
	}
}
