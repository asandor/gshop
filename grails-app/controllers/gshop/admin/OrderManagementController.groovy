package gshop.admin

import gshop.order.Order
import gshop.order.OrderState
import org.springframework.dao.DataIntegrityViolationException

import javax.servlet.http.HttpServletResponse

class OrderManagementController {

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
		params.max = Math.min(params.max ? params.int('max') : 100, 100)

		println params

		String nameFilter = params['filter.name'] ? '%' + params['filter.name'] + '%' : null

		List<Order> orders = Order.withCriteria {
			if (nameFilter) {
				or {
					ilike('customer.firstName', nameFilter)
					ilike('customer.lastName', nameFilter)
					ilike('customer.companyName', nameFilter)
				}
			}

			maxResults(params.max)
		}

		render view: '/admin/order/list', model: [orderInstanceList: orders, orderInstanceTotal: Order.count()]
	}

	def save() {
		def orderInstance = new Order(params)
		if (!orderInstance.save(flush: true)) {
			render(view: "create", model: [orderInstance: orderInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'order.label', default: 'Order'), orderInstance.id])
		redirect(action: "show", id: orderInstance.id)
	}

	def show() {
		def orderInstance = Order.findByUid(params.id)
		if (!orderInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), params.id])
			redirect(action: "list")
			return
		}

		render view: '/admin/order/show', model: [orderInstance: orderInstance]
	}

	def edit() {
		def orderInstance = Order.findByUid(params.id)
		if (!orderInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), params.id])
			redirect(action: "list")
			return
		}

		render view: '/admin/order/list', model: [orderInstance: orderInstance]
	}

	def update() {
		def orderInstance = Order.findByUid(params.id)
		if (!orderInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), params.id])
			redirect(action: "list")
			return
		}

		if (params.version) {
			def version = params.version.toLong()
			if (orderInstance.version > version) {
				orderInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						[message(code: 'order.label', default: 'Order')] as Object[],
						"Another user has updated this Order while you were editing")
				render(view: "edit", model: [orderInstance: orderInstance])
				return
			}
		}

		orderInstance.properties = params

		if (!orderInstance.save(flush: true)) {
			render(view: "edit", model: [orderInstance: orderInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'order.label', default: 'Order'), orderInstance.id])
		redirect(action: "show", id: orderInstance.id)
	}

	def delete() {
		def orderInstance = Order.findByUid(params.id)
		if (!orderInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), params.id])
			redirect(action: "list")
			return
		}

		try {
			orderInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'order.label', default: 'Order'), params.id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'order.label', default: 'Order'), params.id])
			redirect(action: "show", id: params.id)
		}
	}

	def updateState() {
		try {
			log.info "Updating Order '$params.orderId' to state '$params.state'"
			def order = Order.findByUid(params.orderId)
			if (!order) throw new IllegalArgumentException("Invalid Order uid $params.orderId")
			def state = OrderState.valueOf(params.state)
			if (!state) throw new IllegalArgumentException("Invalid Order state: $params.state")

			order.state = state
			render 'OK'
		} catch (IllegalArgumentException ex) {
			log.error ex
			render ex.message
			response.sendError(HttpServletResponse.SC_BAD_REQUEST)
		}
	}
}
