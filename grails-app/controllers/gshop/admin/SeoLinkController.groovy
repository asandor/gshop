package gshop.admin

import gshop.navigation.CategorySeoLink
import gshop.navigation.ProductSeoLink
import gshop.navigation.SeoLink
import gshop.navigation.SeoService

class SeoLinkController {

	SeoService seoService

	def index() {
		def model = [
				categorySeoCount: CategorySeoLink.count(),
				productSeoCount: ProductSeoLink.count(),
				seoCount: SeoLink.count()
		]
		render(view: '/admin/seoLink/index', model: model)
	}

	def generateAll() {
		seoService.generateSeoLinks()
		redirect(action: 'index')
	}

	def generateCategory() {
		seoService.generateCategorySeoLinks()
		redirect(action: 'index')
	}

	def generateProduct() {
		seoService.generateProductSeoLinks()
		redirect(action: 'index')
	}
}
