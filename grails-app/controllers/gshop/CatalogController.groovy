package gshop

import grails.converters.JSON
import gshop.catalog.CatalogService
import gshop.catalog.Paging
import gshop.localization.ProductStrings
import gshop.product.Category
import gshop.product.Product
import gshop.shop.PricingService
import org.springframework.web.servlet.support.RequestContextUtils

import javax.annotation.Resource

class CatalogController {

	@Resource
	CatalogService catalogService

	PricingService pricingService

	ClientDataTransformer clientDataTransformer

	def listProducts(Paging paging) {
		if (params.catId) {
			Category category = Category.findByExtId(params.catId)
			if (category != null) {
				Collection<Product> products = catalogService.getProductsInCategory(category, paging, RequestContextUtils.getLocale(request).language)
				List<Map> productData = products.collect { p ->
					ProductStrings strings = p.strings.find { it.localeId == RequestContextUtils.getLocale(request).language }
					[
							title: strings.title,
							code: p.code,
							shortDescription: strings.shortDescription,
							longDescription: strings.longDescription,
							imageExtension: p.imageExtension,
							url: g.createLink(controller: 'product', id: p.code),
							prices: mapVolumeDiscounts(p)
					]
				}
				Map response = [products: productData]
				render response as JSON
			}
		}

		render emptyResult() as JSON
	}

	private static Map emptyResult() {
		return [products: []]
	}

	private List<Map> mapVolumeDiscounts(Product product) {
		pricingService.calculateProductPrice(product, getClientDataTransformer())
		List<Map> prices = [[price: product.calculatedPrice, from: 0]]
		product.volumeDiscounts?.sort { it.fromAmount }.eachWithIndex { vd, i ->
			def price = [from: vd.fromAmount]
			if (vd.percentage) price.price = product.calculatedPrice - (product.price * (vd.percentage / 100))
			else price.price = vd.fixValue

			prices << price
		}
		return prices
	}

	private ClientDataTransformer getClientDataTransformer() {
		clientDataTransformer.grailsTagLib = g
		clientDataTransformer.shopTagLib = shop
		return clientDataTransformer
	}
}