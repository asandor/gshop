package gshop.basket

import grails.converters.JSON
import gshop.ClientDataTransformer
import gshop.product.Product
import gshop.shop.PricingService
import org.springframework.util.Assert

class BasketController {

	BasketService basketService

	PricingService pricingService

	ClientDataTransformer clientDataTransformer

	def content() {
		response.setContentType('application/json')
		response.setCharacterEncoding('UTF-8')
		render shop.renderBasket(basket: basket)
	}

	def addProduct() {
		Assert.hasText(params.productCode, "Parameter 'productCode' missing from request")
		Assert.hasText(params.amount, "Parameter 'amount' missing from request")
		String productCode = params.productCode
		Integer amount = params.amount.toInteger()

		Product product = Product.findByCode(productCode)
		Assert.notNull(product, "Cannot find product for code '$productCode'")

		Basket basket = basketService.getCurrentBasket(session)
		basketService.addProductToBasket(product, amount, basket)

		pricingService.calculateBasketPrices(basket, getClientDataTransformer())
		def jsonBasket = getClientDataTransformer().transformBasket(basket, false, product.code)
		render jsonBasket as JSON
	}

	def changeItemAmount() {
		Assert.hasText(params.productCode, "Parameter 'productCode' missing from request")
		String productCode = params.productCode

		Product product = Product.findByCode(productCode)
		Assert.notNull(product, "Cannot find product for code '$productCode'")

		Basket basket = basketService.getCurrentBasket(session)

		BasketItem item = basket.getItemForProduct(product)
		Assert.notNull(item, "No item in basket for product $product")

		if (params.inc) {
			Assert.isTrue((params.inc as int) > 0)
			basketService.changeItemAmount(item, item.amount + (params.inc as int), basket)
		} else if (params.dec) {
			Assert.isTrue((params.dec as int) > 0)
			basketService.changeItemAmount(item, item.amount - (params.dec as int), basket)
		} else if (params.amount) {
			Assert.isTrue((params.amount as int) > 0)
			basketService.changeItemAmount(item, params.amount as int, basket)
		} else {
			throw new IllegalArgumentException("Neither of inc, dec or amount parameters defined in request")
		}

		pricingService.calculateBasketPrices(basket, getClientDataTransformer())
		def jsonBasket = clientDataTransformer.transformBasket(basket, false, product.code)
		render jsonBasket as JSON
	}

	def removeProduct() {
		Assert.hasText(params.productCode, "Parameter 'productCode' missing from request")

		Basket basket = basketService.getCurrentBasket(session)
		basketService.removeProductFromBasket(params.productCode, basket)

		pricingService.calculateBasketPrices(basket, getClientDataTransformer())
		def jsonBasket = clientDataTransformer.transformBasket(basket, true)
		render jsonBasket as JSON
	}

	def clearBasket() {
		Basket basket = basketService.getCurrentBasket(session)
		basketService.clearBasket(basket)

		pricingService.calculateBasketPrices(basket, getClientDataTransformer())
		def jsonBasket = clientDataTransformer.transformBasket(basket, true)
		render jsonBasket as JSON
	}

	private ClientDataTransformer getClientDataTransformer() {
		clientDataTransformer.grailsTagLib = g
		clientDataTransformer.shopTagLib = shop
		return clientDataTransformer
	}

}
