package gshop

import org.apache.commons.io.IOUtils
import org.springframework.core.io.ClassPathResource
import org.springframework.web.servlet.support.RequestContextUtils

class LocalizationController {

	private static final String MESSAGE_FILE_NAME_BASE = "messages"

	private static final String MESSAGE_FILE_PACKAGE = "gshop/i18n/"

	def messages() {
		Locale locale = RequestContextUtils.getLocale(request)
		String messageFileName = MESSAGE_FILE_NAME_BASE + '_' + locale.language + ".js"
		ClassPathResource resource = new ClassPathResource(MESSAGE_FILE_PACKAGE + messageFileName)
		try {
			response.setContentType('text/javascript')
			response.setCharacterEncoding('UTF-8')
			response.setContentLength(resource.contentLength() as int)

			response.outputStream << resource.inputStream
			response.flushBuffer()
		} finally {
			IOUtils.closeQuietly(resource.inputStream)
		}
	}
}
