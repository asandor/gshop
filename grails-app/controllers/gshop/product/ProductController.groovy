package gshop.product

import gshop.catalog.CatalogService

class ProductController {

	CatalogService databaseCatalogService

	def index() {
		def product = databaseCatalogService.getProductById(params.id.toLong())

		[product: product]
	}
}
