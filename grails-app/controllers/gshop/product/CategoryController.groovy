package gshop.product

import gshop.catalog.CatalogService

class CategoryController {

	CatalogService databaseCatalogService

	final int pageSize = 13

	def index() {
		def category = databaseCatalogService.getCategoryById(params.id.toLong())
		int pageNumber = params.page ? params.page.toInteger() : 1

		if (log.traceEnabled) log.trace("Rendering Category '$category' at '$params.path'")

		return [
				category: category,
				pageNumber: pageNumber
		]
	}

}
