package gshop.product

import org.apache.commons.io.FileUtils
import gshop.ObjectImageService

class ObjectImageController {

	ObjectImageService objectImageService

	def getImage() {
		if (params.id != ObjectImageService.NO_IMAGE_ID) {
			File imgFile = objectImageService.resolveImageLocalPath(params.id, ObjectImageType.getByCode(params.type))
			if (!imgFile.exists()) {
				sendMissingImageImage()
			} else {
				try {
					response.setContentType(servletContext.getMimeType(imgFile.name))
					response.setContentLength(imgFile.length() as int);
					response.setDateHeader("Expires", (new Date() + 4).time)
					response.setDateHeader("Last-Modified", imgFile.lastModified())
					FileUtils.copyFile(imgFile, response.outputStream)
					response.flushBuffer()
				} catch (Exception ex) {
					log.error("Error while rendering image [params.id=$params.id, params.type=$params.type]", ex)
					sendMissingImageImage()
				}
			}
		} else {
			sendMissingImageImage()
		}
	}

	private void sendMissingImageImage() {
		response.setHeader('Content-Type', servletContext.getMimeType("file.png"))
		response.setDateHeader("Expires", (new Date() + 4).time)
		InputStream inputStream = servletContext.getResourceAsStream("/images/missing-product-image.png")
		response.outputStream << inputStream
		inputStream.close()
		response.flushBuffer()
	}
}
