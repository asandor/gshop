package gshop.security

import grails.converters.JSON
import grails.util.Environment
import gshop.Address
import gshop.ClientDataTransformer
import gshop.CountryCode
import gshop.basket.BasketService
import gshop.shop.PricingService
import org.apache.commons.lang.StringUtils
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.UsernamePasswordToken
import org.springframework.util.Assert

import javax.servlet.http.HttpServletResponse

class UserController {

	static allowedMethods = [
			myAccount: 'GET',
			myOrders: 'GET',
			customerDetails: 'POST',
			registerCustomer: 'POST',
			updateCustomer: 'POST',
			removeDeliveryAddress: 'POST',
			updatePassword: 'POST',
			signIn: 'POST',
			signOut: 'POST'
	]

	UserService userService

	BasketService basketService

	PricingService pricingService

	ClientDataTransformer clientDataTransformer

	def myAccount() {
		Customer customer = userService.currentCustomer
		if (!customer) {
			redirect(uri: '/')
			return
		}

		[customer: customer]
	}

	def myOrders() {
		Customer customer = userService.currentCustomer
		if (!customer) {
			redirect(uri: '/')
			return
		}

		[customer: customer]
	}

	def customerDetails() {
		log.debug "UserController.customerDetails"
		response.setContentType('application/json')
		response.setCharacterEncoding('UTF-8')
		render shop.renderUser()
	}

	def registerCustomer() {
		def userData = request.JSON
		log.info "Registering new customer '$userData.email'"
		try {
			Customer customer = userData.isCompany ? new BusinessCustomer() : new PrivateCustomer();
			customer.email = userData.email?.trim()
			customer.phoneNumber = userData.phoneNumber
			if (customer instanceof PrivateCustomer) {
				customer.firstName = userData.firstName
				customer.lastName = userData.lastName
			}
			if (customer instanceof BusinessCustomer) {
				customer.companyName = userData.companyName
				customer.companyId = userData.companyId
				customer.taxId = userData.taxId
				customer.vat = userData.vat
				customer.contactPerson = userData.contactPerson
			}

			userService.registerNewCustomer(customer, userData.password?.trim())

			userService.loginCustomer(customer, userData.password?.trim())

			renderUserAndBasket()
		} catch (UsernameExistsException ex) {
			log.error ex.getMessage()
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST)
			def message = [code: 'DUPLICATE_USERNAME', message: message(code: 'registration.duplicateUsername', args: [ex.username])]
			render message as JSON
		}
	}

	def updateCustomer() {
		log.info "UserController.updateCustomer"
		Customer customer = getCustomer()

		def userData = request.JSON.user
		log.info("Updating data of customer '$customer.username': $userData")

		if (userData.containsKey('phoneNumber')) customer.phoneNumber = userData.phoneNumber
		if (customer instanceof PrivateCustomer) {
			if (userData.containsKey('firstName')) customer.firstName = userData.firstName
			if (userData.containsKey('lastName')) customer.lastName = userData.lastName
		}
		if (customer instanceof BusinessCustomer) {
			if (userData.containsKey('companyName')) customer.companyName = userData.companyName
			if (userData.containsKey('companyId')) customer.companyId = userData.companyId
			if (userData.containsKey('taxId')) customer.taxId = userData.taxId
			if (userData.containsKey('vat')) customer.vat = userData.vat
			if (userData.containsKey('contactPerson')) customer.contactPerson = userData.contactPerson
		}

		if (userData.containsKey('billingAddress')) {
			if (customer.billingAddress) {
				parseAddress(userData.billingAddress, customer.billingAddress)
			} else {
				customer.billingAddress = parseAddress(userData.billingAddress).save()
			}
		}
		if (userData.containsKey('deliveryAddress')) {
			if (customer.deliveryAddresses.size() > 0) {
				parseAddress(userData.deliveryAddress, customer.deliveryAddresses.iterator().next())
			} else {
				customer.addToDeliveryAddresses(parseAddress(userData.deliveryAddress).save())
			}
		}

		customer.save(failOnError: true)

		render "success"
	}

	def removeDeliveryAddress() {
		log.info "UserController.removeDeliveryAddress"

		Customer customer = userService.currentCustomer
		if (!customer) {
			throw new IllegalStateException("UpdateCustomer failed - no customer logged in")
		}

		if (customer.deliveryAddresses.size() > 0) {
			log.info("Removing delivery address of customer '$customer.username'")
			Address deliveryAddress = customer.deliveryAddresses.iterator().next()
			customer.removeFromDeliveryAddresses(deliveryAddress)
		}

		render "success"
	}

	def updatePassword() {
		log.info "UserController.updatePassword"

		if (StringUtils.isBlank(params.oldPwd)) throw new IllegalArgumentException("Missing parameter: oldPwd")
		if (StringUtils.isBlank(params.newPwd)) throw new IllegalArgumentException("Missing parameter: newPwd")

		Customer customer = getCustomer()

		userService.updateCustomerPassword(customer, params.oldPwd.toString(), params.newPwd.toString())

		render "success"
	}

	def signIn = {
		log.debug "UserController.signIn"
		try {
			def authToken = new UsernamePasswordToken(params.username?.trim(), params.password?.trim())

			if (params.rememberMe) {
				authToken.rememberMe = true
			}

			SecurityUtils.subject.login(authToken)

			User user = userService.getCurrentUser()
			log.info "Authenticated user '${user.username}' (previous login at: ${user.lastLogin})"
			user.lastLogin = new Date()

			if (session.basket && user instanceof Customer) {
				basketService.persistBasket(session.basket)
				session.basket = null //force reinitialization of basket from DB
			}

			renderUserAndBasket()
		} catch (AuthenticationException ex) {
			log.warn "Failed to authenticate user '$params.username': " + ex.toString()
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED)
		}
	}

	private renderUserAndBasket() {
		User user = userService.getCurrentUser()

		def data = [user: clientTransformer.transformUser(user)]
		if (user instanceof Customer) {
			def basket = basketService.getCurrentBasket(session)
			pricingService.calculateBasketPrices(basket, clientDataTransformer)
			data.basket = clientTransformer.transformBasket(basket)
		}

		render data as JSON
	}

	def signOut = {
		log.debug "UserController.signOut"
		// Log the user out of the application.
		SecurityUtils.subject?.logout()

		render "success"
	}

	def deleteUser = {
		//http://localhost:8080/grails-shop/user/deleteUser?username=adam.sandor@test.sk
		if (Environment.current == Environment.DEVELOPMENT) {
			User user = User.findByUsername(params.username)
			if (!user) throw new IllegalArgumentException("Cannot find user '$params.username'")
			user.delete(flush: true)
			log.info "Deleted user '$params.username'"
			render "success"
		} else {
			throw new IllegalStateException("Available only in DEV mode")
		}
	}

	def activateCustomer() {
		Assert.notNull(params.activationCode)

		Customer customer = getCustomer()
		userService.activateCustomer(customer, params.activationCode?.trim())

		render "success"
	}

	private Customer getCustomer() {
		Customer customer = userService.currentCustomer
		if (!customer) {
			throw new SecurityException("Logged in customer needed for this operation: ${params.controller}.${params.action}")
		}
		return customer
	}

	private Address parseAddress(data, Address address = null) {
		if (address == null) address = new Address()
		address.with {
			if (data.containsKey('firstName')) firstName = data.firstName
			if (data.containsKey('lastName')) lastName = data.lastName
			if (data.containsKey('country')) country = Enum.valueOf(CountryCode, data.country.toString())
			if (data.containsKey('city')) city = data.city
			if (data.containsKey('postalCode')) postalCode = data.postalCode
			if (data.containsKey('street')) street = data.street
			if (data.containsKey('number')) number = data.number
			if (data.containsKey('comment')) comment = data.comment
		}
		return address
	}

	private ClientDataTransformer getClientTransformer() {
		clientDataTransformer.grailsTagLib = g
		clientDataTransformer.shopTagLib = shop
		return clientDataTransformer
	}

}