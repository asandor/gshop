package gshop.basket

import org.apache.shiro.SecurityUtils
import gshop.product.Product
import gshop.security.Customer
import gshop.security.UserService
import gshop.shop.PricingService
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.Assert

import javax.servlet.http.HttpSession

class BasketService {

	UserService userService

	/**
	 * @param product Product to create a new BasketItem for or add to existing BasketItem with this Product.
	 * @param amount Amount of the Product to add to the BasketItem.
	 * @param basket Optional Basket instance to use - used to handle session-only basket for no-user scenario.
	 * @return The created/updated BasketItem
	 */
	@Transactional
	BasketItem addProductToBasket(Product product, int amount, Basket basket) {
		Assert.notNull(basket)
		log.debug("Adding [product:$product, amount:$amount] to basket")

		BasketItem basketItem = basket.getItemForProduct(product)
		if (basketItem == null) {
			log.trace("Creating new BasketItem in Basket $basket while adding [product:$product, amount:$amount]")
			basketItem = new BasketItem()
			basketItem.product = product
			basketItem.amount = amount
			basket.addToBasketItems(basketItem)
			if (basket.id != null) { //if basket is persisted
				basketItem.save()
			}
		} else {
			log.trace("Adding $amount pieces of Product $product to already existing BasketItem $basketItem in Basket $basket")
			basketItem.amount += amount
			if (basket.id != null) {
				basketItem.save()
			}
		}

		return basketItem
	}

	@Transactional
	BasketItem changeItemAmount(BasketItem basketItem, int amount, Basket basket) {
		Assert.notNull(basket)
		Assert.isTrue(amount >= 0)
		log.debug("Changing amount of BasketItem $basketItem to $amount")

		basketItem.amount = amount
		if (basket.id != null) {
			basketItem.save()
		}

		return basketItem
	}

	@Transactional
	void removeProductFromBasket(String productCode, Basket basket) {
		Assert.notNull(basket)
		log.debug("Removing [product:$productCode] from basket")

		BasketItem basketItem = basket.basketItems.find { bi -> bi.product.code == productCode }
		if (basketItem == null) throw new IllegalStateException("BasketItem for product $product not found in basket")
		basket.removeFromBasketItems(basketItem)

		if (basket.id != null) {
			basketItem.delete()
		}
	}

	@Transactional
	Basket clearBasket(Basket basket) {
		Assert.notNull(basket)
		log.debug("Clearing basket")

		List<BasketItem> deletedItemList = new ArrayList<BasketItem>(basket.basketItems)
		deletedItemList.each { item ->
			basket.removeFromBasketItems(item)
			if (item.id != null) item.delete()
		}
		return basket
	}

	/**
	 * Used to persist a session-only Basket
	 * @return whether a merge with an existing basket needed to be done
	 */
	@Transactional
	boolean persistBasket(Basket sessionBasket) {
		Assert.notNull(sessionBasket)

		Customer customer = userService.getCurrentCustomer()
		log.debug("Persisting session-only Basket of Customer '$customer'")
		if (customer.getDefaultBasket() && sessionBasket.basketItems) {
			log.trace("Customer '$customer' has a saved basket - merging with items in session")
			Basket customerDefaultBasket = customer.getDefaultBasket()
			sessionBasket.basketItems.each { sessionBasketItem ->
				BasketItem existingItem = customerDefaultBasket.basketItems.find { it.product.code == sessionBasketItem.product.code }
				if (existingItem) {
					existingItem.amount += sessionBasketItem.amount
				} else {
					customerDefaultBasket.addToBasketItems(sessionBasketItem)
					sessionBasketItem.save(failOnError: true)
				}
			}
			return true
		} else if (sessionBasket.basketItems) {
			log.trace("Session-only basket set as default basket for Customer '$customer'")
			customer.addToBaskets(sessionBasket)
			sessionBasket.save(failOnError: true)
			return false
		} else {
			return false
		}
	}

	@Transactional
	Basket getDefaultBasketOfCurrentUser() {
		Customer customer = userService.getCurrentCustomer()

		Basket basket = customer.getDefaultBasket()
		if (basket == null) {
			log.trace("Default basket created for customer '$customer'")
			basket = createDefaultBasket()
			customer.addToBaskets(basket)
		}
		return basket
	}

	Basket createDefaultBasket() {
		new Basket(type: Basket.DEFAULT_BASKET_TYPE)
	}

	Basket getCurrentBasket(HttpSession session) {
		Basket basket = session.basket
		if (basket == null) {
			if (SecurityUtils.subject.authenticated) {
				log.debug("Getting basket of authenticated user")
				basket = getDefaultBasketOfCurrentUser()
				session.basket = basket
			} else {
				log.debug("Creating session-only basket for non-authenticated user")
				basket = createDefaultBasket()
				session.basket = basket
			}
		}
		return basket
	}

}
