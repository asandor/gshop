package gshop.navigation

import gshop.localization.SessionLocale
import gshop.product.Category
import gshop.product.Product
import gshop.product.ShopObject
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.springframework.transaction.annotation.Transactional

import javax.annotation.PostConstruct

class SeoService {

	GrailsApplication grailsApplication

	Map<Locale, ResourceBundle> seoCharacterMappings = [:]

	@PostConstruct
	private void loadSeoCharacterMapping() {

	}

	@Transactional
	void generateSeoLinks(Locale locale = Locale.getDefault()) {
		//TODO old links must be preserved, and a redirect issued
		generateCategorySeoLinks(locale)
		generateProductSeoLinks(locale)
	}

	@Transactional
	void generateCategorySeoLinks(Locale locale = Locale.getDefault()) {
		log.info("Deleting existing Category SeoLinks")
		SeoLink.executeUpdate("DELETE FROM CategorySeoLink")
		log.info("Generating SeoLinks for all Categories")
		Category.list().each { Category category ->
			CategorySeoLink seoLink = createSeoLinkForCategory(category, locale)
			if (seoLink == null) {
				log.warn("Cannot create SeoLink for Category: $category")
			} else {
				log.debug "Saving SeoLink for Category $category: $seoLink.path"
				category.addToSeoLinks(seoLink)
				seoLink.save(failOnError: true)
			}
		}
	}

	@Transactional
	void generateProductSeoLinks(Locale locale = Locale.getDefault()) {
		log.info("Deleting existing Product SeoLinks")
		SeoLink.executeUpdate("DELETE FROM ProductSeoLink")
		log.info("Generating SeoLinks for all Products")
		Product.list().each { Product product ->
			ProductSeoLink seoLink = createSeoLinkForProduct(product, locale)
			if (seoLink == null) {
				log.warn("Cannot create SeoLink for Product: $product")
			} else {
				log.debug "Saving SeoLink for Product $product: $seoLink.path"
				product.addToSeoLinks(seoLink)
				seoLink.save(failOnError: true)
			}
		}
	}

	/**
	 * Return the SEO link for a certain Category
	 * @param category
	 * @return A new or existing SEO link
	 */
	@Transactional
	SeoLink getSeoLink(ShopObject shopObject, Locale locale) {
		if (shopObject == null) return null
		SeoLink seoLink = shopObject.seoLinks.find { SeoLink seoLink -> seoLink.localeId == locale.language }
		if (seoLink) {
			return seoLink
		} else {
			seoLink = createSeoLinkForShopObject(shopObject, locale)
			shopObject.addToSeoLinks(seoLink)
			return seoLink.save(failOnError: true)
		}
	}

	private SeoLink createSeoLinkForShopObject(ShopObject shopObject, Locale locale) {
		switch (shopObject.class) {
			case Category: return createSeoLinkForCategory(shopObject as Category, locale)
			case Product: return createSeoLinkForProduct(shopObject as Product, locale)
			default: throw new IllegalArgumentException("Unknown ShopObject subclass: ${shopObject.class}")
		}
	}

	/**
	 * Create a new SEO link for a Category
	 * @param category
	 * @return
	 */
	private CategorySeoLink createSeoLinkForCategory(Category category, Locale locale) {
		List<String> seoStringList = []
		Category currentCategory = category
		while (currentCategory) {
			if (!currentCategory.getTitle(locale.language)) return null //title may be missing for current locale
			seoStringList << transformPathToSeo(currentCategory.getTitle(locale.language), locale)
			currentCategory = currentCategory.parent
		}
		seoStringList = seoStringList.reverse()

		StringBuilder path = new StringBuilder()
		seoStringList.eachWithIndex { seoStr, i ->
			path.append(seoStr);
			if (i < seoStringList.size() - 1) path.append('/')
		}

		return new CategorySeoLink(
				path: path,
				localeId: SessionLocale.get().language
		)
	}

	/**
	 * Create a new SEO link for a Product
	 * @param category
	 * @return
	 */
	@Transactional
	private ProductSeoLink createSeoLinkForProduct(Product product, Locale locale) {
		SeoLink categoryLink = getSeoLink(product.category, locale)
		if (!categoryLink) return null
		if (!product.getTitle(locale.language)) return null //title may be missing for current locale
		return new ProductSeoLink(
				path: categoryLink.path + '/' + transformPathToSeo(product.getTitle(locale.language), locale),
				localeId: SessionLocale.get().language
		)
	}

	private String transformPathToSeo(String string, Locale locale) {
		String seo = string.replaceAll(' ', '_').toLowerCase()
		ResourceBundle characterMapping = getSeoCharacterMappingsForLocale(locale)
		characterMapping.keySet().each { String key ->
			seo = seo.replace(key, characterMapping.getString(key))
		}
		return seo
	}

	private ResourceBundle getSeoCharacterMappingsForLocale(Locale locale) {
		if (!seoCharacterMappings.containsKey(locale)) {
			ClassLoader classLoader = grailsApplication.getClassLoader()
			return ResourceBundle.getBundle('gshop/seo/seo_character_mapping', locale, classLoader)
		}
		return null
	}
}
