package gshop.security

import org.apache.shiro.SecurityUtils
import org.apache.shiro.UnavailableSecurityManagerException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.grails.ShiroSecurityService
import org.apache.shiro.subject.Subject
import gshop.NotificationService

class UserService {

	static transactional = true

	ShiroSecurityService shiroSecurityService

	NotificationService notificationService

	User getCurrentUser() {
		try {
			Subject subject = SecurityUtils.getSubject()
			if (subject == null || subject.principal == null) return null

			return User.findByUsername(subject.principal.toString())
		} catch (UnavailableSecurityManagerException ex) {
			return null
		}
	}

	Customer getCurrentCustomer() {
		User currentUser = getCurrentUser()
		if (currentUser == null) return null

		if (!(currentUser instanceof Customer)) throw new SecurityException("Current user ${currentUser} is not a Customer (class=${currentUser.class.simpleName})")
		return currentUser as Customer
	}

	void updateCustomerPassword(Customer customer, String oldPassword, String newPassword) {
		String oldEncodedPassword = shiroSecurityService.encodePassword(oldPassword, customer.username)
		if (customer.passwordHash != oldEncodedPassword) throw new PasswordMatchException("Entered previous password doesn't match database record")

		String newEncodedPassword = shiroSecurityService.encodePassword(newPassword, customer.username)
		customer.passwordHash = newEncodedPassword

		notificationService.passwordModified(customer)
		log.info "Password of customer $customer.username updated successfully"
	}

	void registerNewCustomer(Customer customer, String password) {
		customer.passwordHash = shiroSecurityService.encodePassword(password, customer.username)
		customer.activated = false
		customer.username = customer.email

		if (Customer.findByUsername(customer.username)) {
			throw new UsernameExistsException(customer.username)
		}

		customer.activationCode = generateActivationString()
		customer.save(failOnError: true)

		notificationService.customerRegistered(customer)
		if (log.isTraceEnabled()) {
			log.trace "Customer $customer.username registered successfully (activationCode='$customer.activationCode')"
		} else {
			log.info "Customer $customer.username registered successfully"
		}
	}

	void loginCustomer(Customer customer, String password) {
		log.debug "Logging in customer $customer.username"
		def authToken = new UsernamePasswordToken(customer.username, password)
		SecurityUtils.subject.login(authToken)
	}

	void activateCustomer(Customer customer, String activationCode) {
		log.info "Activating customer '$customer.username'"

		if (customer.activationCode == activationCode) {
			customer.activated = true
		} else {
			throw new SecurityException("Incorrect activation code '$activationCode'")
		}
	}

	static String generateActivationString() {
		Random rnd = new Random()
		def randomInInterval = { interval ->
			switch (interval) {
				case 0: return (char) (48 + rnd.nextInt(10))
				case 1: return (char) (65 + rnd.nextInt(26))
				case 2: return (char) (97 + rnd.nextInt(26))
			}
		}
		char[] string = new char[5];
		for (int i = 0; i < 5; i++) {
			int interval = rnd.nextInt(3)
			string[i] = randomInInterval(interval)
		}
		return new String(string)
	}
}
