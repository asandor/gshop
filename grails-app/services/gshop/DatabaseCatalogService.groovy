package gshop

import gshop.catalog.CatalogService
import gshop.catalog.Paging
import gshop.product.Category
import gshop.product.Product
import org.codehaus.groovy.grails.commons.GrailsApplication

class DatabaseCatalogService implements CatalogService {

	GrailsApplication grailsApplication

	Category getCategoryByExtId(String extId) {
		return Category.findByExtId(extId);
	}

	Category getCategoryById(Long id) {
		return Category.get(id)
	}

	Category getCategoryBySEO(String seoLink, String locale) {
		return Category.where {
			seoLinks { path == seoLink && localeId == locale }
		}.find()
	}

	Collection<Category> getTopCategories(Paging paging, String locale) {
		log.trace "Listing top Categories"
		if (paging == null) paging = new Paging(sortField: Paging.SortableField.INDEX, sortOrder: Paging.SortOrder.ASC)

		String query = "FROM Category c LEFT JOIN FETCH c.strings str LEFT JOIN FETCH c.seoLinks WHERE c.parent = NULL AND c.active = true AND str.localeId = :localeId";
		query = applyCategoryPaging(query, paging)
		Collection<Category> categories = Category.findAll(query, [localeId: locale], toPagingParams(paging))
		return categories;
	}

	Collection<Category> getSubCategories(Category parent, Paging paging, String locale) {
		log.trace "Listing subcategories of category $parent"
		if (paging == null) paging = new Paging(sortField: Paging.SortableField.INDEX, sortOrder: Paging.SortOrder.ASC)

		String query = "FROM Category c LEFT JOIN FETCH c.strings str LEFT JOIN FETCH c.seoLinks WHERE c.parent = :parent AND c.active = true AND str.localeId = :localeId";
		query = applyCategoryPaging(query, paging)
		return Category.findAll(query, [parent: parent, localeId: locale], toPagingParams(paging))
	}

	Product getProductById(Long id) {
		return Product.get(id)
	}

	Product getProductBySEO(String seoLink, String locale) {
		return Product.where {
			seoLinks { path == seoLink && localeId == locale }
		}.find()
	}

	Collection<Product> getProductsInCategory(Category category, Paging paging, String locale) {
		if (paging == null) paging = new Paging(sortField: Paging.SortableField.TITLE, sortOrder: Paging.SortOrder.ASC)

		String query = """SELECT p FROM Product p LEFT JOIN FETCH p.strings str
WHERE p.category = :cat AND str.localeId = :localeId AND p.active = true AND
(p.activeFrom IS NULL OR p.activeFrom <= current_date()) AND (p.activeTo IS NULL OR p.activeTo >= current_date()) """;
		switch (paging?.sortField) {
			case Paging.SortableField.TITLE: query += "ORDER BY str.title "; break;
			case Paging.SortableField.CREATED: query += "ORDER BY p.dateCreated "; break;
			case Paging.SortableField.PRICE: query += "ORDER BY p.price "; break; //TODO: this will not take discounts into account
		}
		if (paging.sortField) {
			switch (paging?.sortOrder) {
				case Paging.SortOrder.DESC: query += "DESC"; break;
				default: query += "ASC"; break;
			}
		}
		return Product.executeQuery(query, [cat: category, localeId: locale], toPagingParams(paging))
	}

	private String applyCategoryPaging(String query, Paging paging) {
		switch (paging.sortField) {
			case Paging.SortableField.TITLE: query += " ORDER BY str.title "; break;
			case Paging.SortableField.CREATED: query += " ORDER BY c.dateCreated "; break;
			default: query += " ORDER BY c.index "
		}
		if (paging.sortField) {
			switch (paging?.sortOrder) {
				case Paging.SortOrder.DESC: query += "DESC"; break;
				default: query += "ASC"; break;
			}
		}
		return query
	}

	private Map toPagingParams(Paging paging) {
		int pageNumber = paging.pageNumber ?: 0
		int pageSize = paging.pageSize ?: grailsApplication.config.shop.catalog.product.defaultPageSize
		return [max: pageSize, offset: pageNumber * pageSize]
	}

//	void loadFromDatabase() {
//		log.info("Loading CatalogCache from database")
//
//		categories = Category.findAllByParentIsNull().collect { cat ->
//			CCCategory ccCat = new CCCategory(cat)
//			categoriesByDbId[cat.id] = ccCat
//		}
//
//		categories.each { ccCat -> loadChildren(ccCat, Category.read(ccCat.dbId)) }
//
//		log.debug("Catalog cache loaded from database successfully")
//	}
//
//	private void loadChildren(CCCategory ccCategory, Category category) {
//		ccCategory.children = Category.findByParent(category).collect { cat ->
//			CCCategory ccCat = new CCCategory(cat)
//			categoriesByDbId[cat.id] = ccCat
//			loadChildren(ccCat, cat)
//			return ccCat
//		}
//	}
}
