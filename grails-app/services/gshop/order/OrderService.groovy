package gshop.order

import gshop.Address
import gshop.basket.Basket
import gshop.security.Customer
import gshop.shop.PricingService

class OrderService {

	static transactional = true

	Order placeOrder(Basket basket, Customer customer, ShipmentMethod shipmentMethod, PaymentMethod paymentMethod, Locale locale) {
		Order order = new Order()
		order.products = basket.basketItems.collect { item ->
			new OrderProduct(
					code: item.product.code,
					imageExtension: item.product.imageExtension,
					title: item.product.getTitle(locale.language),
					amount: item.amount,
					price: item.price
			)
		}
		order.productCount = basket.basketItems.size()

		order.price = basket.price

		order.shipmentMethodPrice = shipmentMethod.price
		order.shipmentMethodTitle = shipmentMethod.name.find { it.localeId = locale.language }.string
		order.paymentMethodPrice = paymentMethod.price
		order.paymentMethodTitle = paymentMethod.name.find { it.localeId == locale.language }.string

		order.billingAddress = Address.clone(customer.billingAddress)
		if (customer.deliveryAddresses?.size() > 0) {
			order.deliveryAddress = Address.clone(customer.deliveryAddresses.iterator().next())
		}

		order.customer = OrderCustomer.fromCustomer(customer)

		order.state = OrderState.WAITING

		log.info "Placing order new order for customer '$customer.username'"
		order = order.saveOrder()
		log.debug "Order placed for customer '$customer.username' with number $order.orderNumber, uid '$order.uid'"
		return order
	}
}
