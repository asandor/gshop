package gshop.shop

import gshop.ClientDataTransformer
import gshop.basket.Basket
import gshop.basket.BasketItem
import gshop.basket.Price
import gshop.discount.Discount
import gshop.order.PaymentMethod
import gshop.order.ShipmentMethod
import gshop.product.Product
import gshop.security.Customer
import gshop.security.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value

class PricingService {

	UserService userService

	@Autowired
	List<Discount> discounts;

	@Value('${shop.vat}')
	BigDecimal vat;

	/**
	 * Calculates and sets price property of all BasketItems and the Basket itself.
	 * @param basket The passed-in Basket will be updated.
	 */
	void calculateBasketPrices(Basket basket, ClientDataTransformer clientDataTransformer) {
		Price basketPrice = new Price(0, getVat(), 0, 0)
		basket.basketItems.each { item ->
			calculateBasketItemPrice(item, clientDataTransformer)

			basketPrice.base += item.price.base
			basketPrice.vatAmount += item.price.vatAmount
			basketPrice.baseWithVat += item.price.baseWithVat
		}
		basket.noDiscountPrice = new Price(basketPrice)
		basket.price = basketPrice
		basket.appliedDiscounts = []

		if (basket.price.base > 0) {
			Customer customer = userService.getCurrentCustomer()
			discounts.each { it.apply(basket, customer, customer?.locale ?: Locale.getDefault(), clientDataTransformer) }
		}
	}

	void calculateBasketItemPrice(BasketItem item, ClientDataTransformer clientDataTransformer) {
		item.price = Price.createPrice(item.product.price * item.amount, getVat(), null, null)

		Customer customer = userService.getCurrentCustomer()
		discounts.each {
			it.apply(item, customer, customer?.locale ?: Locale.getDefault(), clientDataTransformer)
		}
	}

	void calculateProductPrice(Product product, ClientDataTransformer clientDataTransformer) {
		product.calculatedPrice = product.price

		Customer customer = userService.getCurrentCustomer()
		discounts.each {
			it.apply(product, customer, customer?.locale ?: Locale.getDefault(), clientDataTransformer)
		}
	}

	void calculateShipmentMethodPrice(ShipmentMethod sm, Basket basket) {
		BigDecimal baseWithVat = sm.price * (1.0 + getVat())
		sm.calculatedPrice = new Price(sm.price, getVat(), baseWithVat - sm.price, baseWithVat)
	}

	void calculatePaymentMethodPrice(PaymentMethod pm, Basket basket) {
		BigDecimal baseWithVat = pm.price * (1.0 + getVat())
		pm.calculatedPrice = new Price(pm.price, getVat(), baseWithVat - pm.price, baseWithVat)
	}

	/**
	 * Calculates the final price of an Order with shipment and payment methods included.
	 * All parameters are expected to have their prices calculated.
	 */
	Price calculateOrderPrice(Basket basket, ShipmentMethod sm, PaymentMethod pm) {
		assert basket.price != null
		assert sm.calculatedPrice != null
		assert pm.calculatedPrice != null

		BigDecimal finalPriceBase = basket.price.base + sm.calculatedPrice.base + pm.calculatedPrice.base
		return Price.createPrice(finalPriceBase, vat, null, null)
	}

	BigDecimal getVat() {
		return vat.toBigDecimal()
	}
}
