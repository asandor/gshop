package gshop

/**
 * Handles which countries are available for this shop.
 */
class CountryService {

	Collection<CountryCode> getCountries() {
		CountryCode.values()
	}
}
