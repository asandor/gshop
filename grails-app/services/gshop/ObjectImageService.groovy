package gshop

import gshop.product.ObjectImageType
import org.springframework.beans.factory.annotation.Value
import org.springframework.util.Assert

class ObjectImageService {

	@Value('${shop.object_image_directory}')
	String objectImageDirectory

	static final NO_IMAGE_ID = "NO_IMAGE"

	File resolveImageLocalPath(String imageName, ObjectImageType imageType) {
		Assert.notNull(imageName, "imageName not specified")
		Assert.notNull(imageType, "imageType not specified")
		File imgDir = new File(objectImageDirectory)
		File imgTypeDir = new File(imgDir, imageType.directory)
		File imgFile = new File(imgTypeDir, imageName)
		if (log.traceEnabled && !imgFile.exists()) {
			log.trace("Cannot find object image file '${imgFile.canonicalPath}'")
		}
		return imgFile
	}
}
