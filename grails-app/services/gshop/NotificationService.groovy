package gshop

import grails.gsp.PageRenderer
import grails.plugin.mail.MailService
import grails.util.Environment
import org.apache.commons.lang.exception.ExceptionUtils
import gshop.security.Customer
import org.springframework.context.MessageSource

class NotificationService {

	MailService mailService

	MessageSource messageSource

	PageRenderer groovyPageRenderer

	void passwordModified(Customer customer) {
	}

	void customerRegistered(Customer customer) {
		Locale customerLocale = new Locale(customer.languageCode)
		sendEmail {
			to customer.email
			subject messageSource.getMessage('activation.email.subject', null, customerLocale)
			html groovyPageRenderer.render(view: '/email/registrationSuccessEmail',
					model: [customer: customer, code: customer.activationCode])
		}
	}

	private void sendEmail(Closure closure) {
		try {
			mailService.sendMail(closure)
		} catch (Exception ex) {
			if (Environment.current == Environment.DEVELOPMENT) {
				log.warn("Cannot send email: " + ExceptionUtils.getRootCause(ex).toString())
			} else {
				log.error("Cannot send email", ex)
			}
		}
	}
}
