import gshop.ClientDataTransformer
import gshop.security.MultiAlgorithmHashedCredentialsMatcher

beans = {

	groupDiscount(gshop.discount.GroupDiscount) {
	}

	volumeDiscount(gshop.discount.VolumeDiscount) {
	}

	clientDataTransformer(ClientDataTransformer) { bean ->
		bean.scope = "prototype"
	}

	localeResolver(org.springframework.web.servlet.i18n.SessionLocaleResolver) {
		java.util.Locale.setDefault(Locale.ENGLISH)
	}

	credentialMatcher(MultiAlgorithmHashedCredentialsMatcher) {
		storedCredentialsHexEncoded = true
		hashAlgorithmName = "SHA-256"
	}

}
