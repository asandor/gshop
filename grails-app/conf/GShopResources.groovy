import grails.util.Environment

modules = {

	catalog {
		dependsOn(['common', 'appCommon', 'category', 'product', 'basket', 'user'])

		resource url: '/css/xbreadcrumbs.css'
		resource url: '/js/xbreadcrumbs.js'

		resource url: '/js/catalog/CatalogController.js'
		resource url: '/js/catalog/ObjectImageController.js'

		resource url: '/js/app/Application.js'
	}

	seoLinkAdmin {
		resource url: '/admin/seoLink/SeoLink.js'
	}

	catalogAdmin {
		dependsOn(['admin', 'dhtmlxgrid', 'dhtmlxtree'])

		resource url: '/admin/catalog/CatalogManagementController.js'
		resource url: '/admin/catalog/CategoryTree.js'
		resource url: '/admin/catalog/ProductTable.js'
		resource url: '/admin/catalog/Catalog.js'
	}

	admin {
		dependsOn(['common'])

		resource url: '/admin/admin.css'
		resource url: '/admin/admin-layout.css'
		resource url: '/progressbar/ProgressBar.js'
	}

	myAccount {
		dependsOn(['appCommon', 'form', 'user', 'basket'])

		resource url: '/js/myaccount/MyAccount.js'
		resource url: '/css/myAccount.css'
	}

	myOrders {
		dependsOn(['appCommon', 'form', 'user', 'datatables', 'basket'])

		resource url: '/js/myaccount/MyOrders.js'
		resource url: '/js/myaccount/OrderController.js'
		resource url: '/js/myaccount/OrderTable.js'
		resource url: '/js/myaccount/OrderDetailWidget.js'
		resource url: '/js/myaccount/OrderDetailWidget.css'
		resource url: '/js/myaccount/OrderProductTable.js'
		resource url: '/js/myaccount/OrderDetailWidget-layout.js', attrs: [type: 'js']
		resource url: '/js/myaccount/Address-layout.js'
	}

	basket {
		dependsOn(['common', 'wizard'])

		resource url: '/js/basket/Basket.js'
		resource url: '/js/basket/BasketPanel.js'
		resource url: '/js/basket/ProductListPanel.js'
		resource url: '/js/basket/BasketDialog.js'
		resource url: '/js/basket/SummaryPanel.js'
		resource url: '/js/basket/SummaryPanel-layout.js'
		resource url: '/js/basket/SummaryPanel-message-layout.js'
		resource url: '/js/basket/BasketPrice-layout.js'
		resource url: '/js/myaccount/OrderController.js'
		resource url: '/js/basket/PaymentPanel.js'
		resource url: '/js/basket/PaymentPanel-layout.js'
		resource url: '/js/basket/UserDetailsPanel.js'
		resource url: '/js/basket/UserDetailsPanel-layout.js'
		resource url: '/js/basket/SignInPanel.js'
		resource url: '/js/basket/SignInPanel-layout.js'

		resource url: '/css/BasketDialog.css'
		resource url: '/css/myAccount.css'

		resource url: '/css/jquery.jscrollpane.css'
		resource url: '/js/jquery.mousewheel.js'
	}

	user {
		dependsOn(['wizard', 'form'])

		resource url: '/js/user/User.js'

		resource url: '/js/user/UserDataForm-layout.js'
		resource url: '/js/user/PasswordForm-layout.js'
		resource url: '/js/user/UserAddressForm-layout.js'
		resource url: '/js/user/UserAddressForm.js'
		resource url: '/js/user/UserDataForm.js'
		resource url: '/js/user/PasswordForm.js'
		resource url: '/js/user/UserController.js'
		resource url: '/js/user/SignInForm.js'
		resource url: '/js/user/SignInForm-layout.js'

		resource url: '/js/user/RegistrationDialog.js'
		resource url: '/js/user/RegistrationPanel.js'
		resource url: '/js/user/RegistrationPanel-layout.js'
		resource url: '/js/user/ActivationPanel.js'
		resource url: '/js/user/ActivationPanel-layout.js'
		resource url: '/js/user/RegistrationForm.js'
		resource url: '/js/user/RegistrationForm-layout.js'

	}

	category {
		resource url: '/css/category.css'
	}

	product {
		dependsOn('common')

		resource url: '/css/product.css'
		if (Environment.current == Environment.DEVELOPMENT) {
			resource url: '/js/jquery.ba-outside-events.js'
		} else {
			resource url: '/js/jquery.ba-outside-events.min.js'
		}

		resource url: 'js/catalog/ProductPriceDisplay.js'
		resource url: 'js/catalog/ProductPricePanel-layout.js'
		resource url: '/js/catalog/ProductTile.js'
		resource url: '/js/catalog/ProductTileList.js'
		resource url: '/js/catalog/ProductTile-layout.js'
	}

	orderManagement {
		dependsOn('common')

		resource url: '/admin/order/OrderManagementController.js'
		resource url: '/admin/order/OrderManagement.js'
	}

	wizard {
		dependsOn('common')

		resource url: '/js/dialog/WizardDialog.js'
		resource url: '/js/dialog/WizardPanel.js'
		resource url: '/js/dialog/StaticPanel.js'
	}

	form {
		dependsOn('common')

		resource url: '/js/form/ViewEditForm.js'
		resource url: '/js/form/ValidationBuilder.js'

		if (Environment.current == Environment.DEVELOPMENT) {
			resource url: '/js/jquery.validate.js'
		} else {
			resource url: '/js/jquery.validate.min.js'
		}
	}

	/**
	 * commons not used by admin part
	 */
	appCommon {
		resource url: '/css/layout.css'
		resource url: '/css/login.css'
		resource url: '/js/app/NavigationPanel.js'
		resource url: '/js/app/AuthenticationPanel.js'
	}

	common {
		dependsOn(['jquery', 'gritter', 'closure_templates'])

		resource url: '/js/inherit.js'
		resource url: '/module/InitController.js'
		resource url: '/module/Module.js'
		resource url: '/module/ModuleManager.js'
		resource url: '/module/CommonModuleManager.js'

		resource url: '/css/pagestyle.css'
	}

	jquery {
		if (Environment.current == Environment.DEVELOPMENT) {
			resource url: '/js/jquery-1.10.0.js'
			resource url: '/js/jquery-ui-1.10.3.custom.js'
			resource url: '/css/jquery-ui-1.10.3.custom.css'
		} else {
			resource url: '/js/jquery-1.10.0.min.js'
			resource url: '/js/jquery-ui-1.10.3.custom.min.js'
			resource url: '/css/jquery-ui-1.10.3.custom.min.css'
		}
	}

	gritter {
		resource url: '/css/jquery.gritter.css'
		if (Environment.current == Environment.DEVELOPMENT) {
			resource url: '/js/jquery.gritter.js'
		} else {
			resource url: '/js/jquery.gritter.min.js'
		}
	}

	closure_templates {
		resource url: '/js/soyutils.js'
	}

	datatables {
		resource url: '/datatables/css/jquery.dataTables.css'
		resource url: '/css/myOrders.css'

		if (Environment.current == Environment.DEVELOPMENT) {
			resource url: '/datatables/jquery.dataTables.js'
		} else {
			resource url: '/datatables/jquery.dataTables.min.js'
		}
	}

	dhtmlxgrid {
		dependsOn('dhtmlxcommon')

		resource url: '/dhtmlx/grid/dhtmlxgrid.js'
		resource url: '/dhtmlx/grid/dhtmlxgridcell.js'
		resource url: '/dhtmlx/grid/dhtmlxgrid.css'
	}

	dhtmlxtree {
		dependsOn('dhtmlxcommon')

		resource url: '/dhtmlx/tree/dhtmlxtree_json.js'
		resource url: '/dhtmlx/tree/dhtmlxtree_dragin.js'
		resource url: '/dhtmlx/tree/dhtmlxtree_ed.js'
		resource url: '/dhtmlx/tree/dhtmlxtree_start.js'
		resource url: '/dhtmlx/tree/dhtmlxtree.js'
		resource url: '/dhtmlx/tree/dhtmlxtree.css'
	}

	dhtmlxcommon {

		resource url: '/dhtmlx/dhtmlxcommon.js'
	}
}