class GshopUrlMappings {

	static excludes = ["/images/*", "/css/*", "/js/*"]

	static mappings = {

		"/email"(view: 'email')

		"/init/$action"(controller: 'init')

		"/init/UrlMappings"(controller: 'init', action: 'urlMappingScript')

		"/localization/$action"(controller: 'localization')

		"/catalog/$action"(controller: 'catalog')

		"/searchable"(controller: 'searchable')

		"/order/$action"(controller: 'order')

		"/basket/$action"(controller: 'basket')

		"/cache/$action"(controller: 'cache')

		"/user/$action"(controller: 'user')

		"/auth/$action"(controller: 'auth')

		"/category/$id"(controller: 'category', action: 'index')

		"/product/$id"(controller: 'product', action: 'index')

		"/oimg/$id"(controller: 'objectImage', action: 'getImage')

		"/oimg"(controller: 'objectImage', action: 'getImage')

		"/admin"(controller: "admin", action: 'index')
		"/admin/seoLink/$action"(controller: 'seoLink')
		"/admin/order/$action"(controller: 'orderManagement')
		"/admin/settings/$action"(controller: 'settings')
		"/admin/catalog/$action"(controller: 'catalogManagement')

		"/$path**"(controller: "main")

		"500"(controller: 'error', action: 'handleGenericError')
	}
}
