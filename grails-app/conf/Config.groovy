shop.vat = 0.2
shop.catalog.product.defaultPageSize = 40
shop.currency = Currency.getInstance("EUR")

log4j = {
	appenders {
		console name: 'stdout', layout: pattern(conversionPattern: '[%p] %d %X{user} %c %m%n')
	}

	error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
			'org.codehaus.groovy.grails.web.pages', //  GSP
			'org.codehaus.groovy.grails.web.sitemesh', //  layouts
			'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
			'org.codehaus.groovy.grails.web.mapping', // URL mapping
			'org.codehaus.groovy.grails.commons', // core / classloading
			'org.codehaus.groovy.grails.plugins', // plugins
			'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
			'org.springframework',
			'org.hibernate',
			'net.sf.ehcache.hibernate'

	warn 'org.mortbay.log'
}
