package gshop.basket

import gshop.product.Product
import gshop.security.Customer

class Basket {

	static belongsTo = [
			customer: Customer
	]

	static hasMany = [
			basketItems: BasketItem
	]

	Date dateCreated
	Date lastUpdated
	Price price
	Price noDiscountPrice
	List<DiscountInfo> appliedDiscounts
	String uid = UUID.randomUUID().toString()

	String type = DEFAULT_BASKET_TYPE

	static final String DEFAULT_BASKET_TYPE = 'default'

	static mapping = {
		basketItems sort: 'dateCreated', order: 'asc', cache: 'read-write'
	}

	static constraints = {
		uid nullable: true, size: 36..36
	}

	static transients = ['price', 'appliedDiscounts', 'noDiscountPrice']

	BasketItem getItemForProduct(Product product) {
		return basketItems.find { bi -> bi.product.code == product.code }
	}

	String toString() {
		"[type:$type, dateCreated:$dateCreated, basketItems:${basketItems ? basketItems.size() : 0}]"
	}

	static class DiscountInfo {
		String title
		String description
	}

}
