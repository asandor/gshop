package gshop.basket

import javax.persistence.Embeddable
import java.math.RoundingMode

@Embeddable
class Price {

	private BigDecimal base
	private BigDecimal vatPercent
	private BigDecimal vatAmount
	private BigDecimal baseWithVat

	Price() {
	}

	Price(Price price) {
		this.base = price.base
		this.baseWithVat = price.baseWithVat
		this.vatPercent = price.vatPercent
		this.vatAmount = price.vatAmount
	}

	Price(BigDecimal base, BigDecimal vatPercent, BigDecimal vatAmount, BigDecimal baseWithVat) {
		this.base = base.setScale(2, RoundingMode.HALF_UP)
		this.baseWithVat = baseWithVat.setScale(2, RoundingMode.HALF_UP)
		this.vatPercent = vatPercent.setScale(2, RoundingMode.HALF_UP)
		this.vatAmount = vatAmount.setScale(2, RoundingMode.HALF_UP)
	}

	static Price createPrice(BigDecimal base, BigDecimal vatPercent, BigDecimal vatAmount, BigDecimal baseWithVat) {
		if (base && vatPercent && vatAmount && baseWithVat) {
			return new Price(base, vatPercent, vatAmount, baseWithVat)
		} else if (base && baseWithVat) {
			Price price = new Price()
			price.base = base
			price.baseWithVat = baseWithVat

			if (vatAmount && vatPercent) {
				price.vatPercent = vatPercent
				price.vatAmount = vatAmount
			} else {
				price.vatAmount = price.baseWithVat - price.base
				price.vatPercent = price.vatAmount / price.baseWithVat
			}
			return price
		} else if (base != null && vatPercent != null) {
			Price price = new Price()
			price.base = base
			price.vatPercent = vatPercent
			price.baseWithVat = base + (base * vatPercent)
			price.vatAmount = price.baseWithVat - price.base
			return price
		} else {
			return null
		}
	}

	void modifyBase(BigDecimal base) {
		Price newPrice = createPrice(base, this.vatPercent, null, null)
		setBase(newPrice.base)
		setBaseWithVat(newPrice.baseWithVat)
		setVatPercent(newPrice.vatPercent)
		setVatAmount(newPrice.vatAmount)
	}

	BigDecimal getBase() {
		return base
	}

	void setBase(BigDecimal base) {
		this.base = scale(base)
	}

	BigDecimal getVatPercent() {
		return scale(vatPercent)
	}

	void setVatPercent(BigDecimal vatPercent) {
		this.vatPercent = vatPercent
	}

	BigDecimal getVatAmount() {
		return scale(vatAmount)
	}

	void setVatAmount(BigDecimal vatAmount) {
		this.vatAmount = scale(vatAmount)
	}

	BigDecimal getBaseWithVat() {
		return baseWithVat
	}

	void setBaseWithVat(BigDecimal baseWithVat) {
		this.baseWithVat = scale(baseWithVat)
	}

	private static BigDecimal scale(BigDecimal value) {
		return value.setScale(2, RoundingMode.HALF_UP)
	}

	static constraints = {
		vatPercent nullable: true
		vatAmount nullable: true
		baseWithVat nullable: true
	}
}
