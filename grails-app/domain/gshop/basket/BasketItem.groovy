package gshop.basket

import gshop.product.Product

class BasketItem {

	static belongsTo = [
			basket: Basket
	]

	Product product
	Integer amount
	Price price
	String discountDescription

	Date dateCreated
	Date lastUpdated

	String toString() {
		"[product:$product.code, amount:$amount]"
	}

	static mapping = {
	}

	static transients = ['price', 'discountDescription']
}
