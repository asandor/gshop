package gshop

class LocalizedString {

	String string
	String localeId

	static constraints = {
		localeId nullable: false, size: 1..10
	}
}
