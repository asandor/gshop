package gshop.localization

import gshop.product.Product

class ProductStrings extends ShopObjectStrings {

	static belongsTo = [product: Product]

}
