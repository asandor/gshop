package gshop.localization

import gshop.product.Category

class CategoryStrings extends ShopObjectStrings {

	static belongsTo = [category: Category]

}
