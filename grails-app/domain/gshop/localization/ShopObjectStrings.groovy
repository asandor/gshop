package gshop.localization
/**
 * Localized string of ShopObjects (Products and Categories)
 */
class ShopObjectStrings {

	String localeId

	String title

	String shortDescription

	String longDescription

	static constraints = {
		longDescription nullable: true, maxSize: 2000
		title maxSize: 100
		shortDescription nullable: true, maxSize: 300
	}

}
