package gshop.order

import org.apache.commons.codec.binary.Base32
import gshop.Address

import gshop.basket.Price
import gshop.security.BusinessCustomer
import gshop.security.Customer
import gshop.security.PrivateCustomer

class Order {

	static hasMany = [products: OrderProduct]

	Date dateCreated

	String uid;

	String shipmentMethodTitle
	BigDecimal shipmentMethodPrice

	String paymentMethodTitle
	BigDecimal paymentMethodPrice

	Price price

	Address deliveryAddress

	Address billingAddress

	OrderCustomer customer

	int orderNumber

	OrderState state

	boolean paymentReceived

	int productCount = 0

	static embedded = ['price', 'deliveryAddress', 'billingAddress', 'customer']

	static constraints = {
		deliveryAddress nullable: true
		shipmentMethodTitle nullable: true
		shipmentMethodPrice nullable: true
		paymentMethodTitle nullable: true
		paymentMethodPrice nullable: true
		uid maxSize: 50
	}

	static mapping = {
		table "gsorder"
	}

	Order saveOrder() {
		Integer largestOrderNumber = executeQuery("SELECT MAX(orderNumber) FROM Order")[0] as Integer
		if (largestOrderNumber == null) largestOrderNumber = 1;
		orderNumber = largestOrderNumber + 1
		Random rnd = new Random(orderNumber)

		boolean unique = false
		while (!unique) {
			byte[] bytes = new byte[5]
			rnd.nextBytes(bytes)
			uid = new Base32(false).encodeToString(bytes)
			if (executeQuery("SELECT COUNT(id) FROM Order WHERE uid = :uid", [uid: uid])[0] == 0) {
				unique = true
			}
		}
		return save(failOnError: true)
	}
}

class OrderCustomer {

	String username
	String email
	String phoneNumber

	String companyName
	String taxId
	String companyId
	String vat
	String contactPerson

	String firstName
	String lastName

	static constraints = {
		phoneNumber nullable: true, maxSize: 80
		email maxSize: 80
		username size: 5..80

		companyName maxSize: 50, nullable: true
		taxId maxSize: 30, nullable: true
		vat maxSize: 30, nullable: true
		companyId maxSize: 30, nullable: true
		contactPerson maxSize: 100, nullable: true

		firstName maxSize: 50, nullable: true
		lastName maxSize: 50, nullable: true
	}

	static OrderCustomer fromCustomer(Customer customer) {
		OrderCustomer orderCustomer = new OrderCustomer()
		orderCustomer.with {
			username = customer.username
			email = customer.email
			phoneNumber = customer.phoneNumber

			if (customer instanceof BusinessCustomer) {
				companyName = customer.companyName
				taxId = customer.taxId
				companyId = customer.companyId
				vat = customer.vat
				contactPerson = customer.contactPerson
			}

			if (customer instanceof PrivateCustomer) {
				firstName = customer.firstName
				lastName = customer.lastName
			}
		}
		return orderCustomer
	}
}
