package gshop.order


import gshop.basket.Price

class OrderProduct {

	static belongsTo = Order

	String code

	String title

	Price price

	Integer amount

	String imageExtension

	static embedded = ['price']

}
