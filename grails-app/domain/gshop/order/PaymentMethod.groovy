package gshop.order

import gshop.LocalizedString
import gshop.basket.Price

class PaymentMethod {

	static hasMany = [name: LocalizedString]

	BigDecimal price

	Price calculatedPrice

	String uid = UUID.randomUUID().toString()

	static transients = ['calculatedPrice']

	static constraints = {
		uid nullable: true, size: 36..36
	}
}
