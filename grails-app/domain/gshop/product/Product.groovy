package gshop.product

import gshop.localization.ProductStrings
import gshop.navigation.ProductSeoLink
import gshop.product.discount.VolumeDiscount

class Product extends ShopObject {

	static hasMany = [
			volumeDiscounts: VolumeDiscount,
			strings: ProductStrings,
			seoLinks: ProductSeoLink
	]

	static belongsTo = [category: Category]

	String code

	String imageExtension = 'jpg'

	BigDecimal price
	BigDecimal calculatedPrice
	String discountInfo

	Date activeFrom
	Date activeTo

	Integer stockAmount
	StockMode stockMode

	Date dateCreated
	Date lastUpdated

	static constraints = {
		category nullable: true
		activeFrom nullable: true
		activeTo nullable: true
		extId nullable: true
		code unique: true
	}

	static mapping = {
		volumeDiscounts sort: 'fromAmount', order: 'asc'
	}

	static transients = ['title', 'shortDescription', 'longDescription', 'calculatedPrice', 'discountInfo']

	String toString() { "[code:$code]" }

	boolean hasVolumnDiscounts() {
		volumeDiscounts != null && !volumeDiscounts.isEmpty()
	}

}
