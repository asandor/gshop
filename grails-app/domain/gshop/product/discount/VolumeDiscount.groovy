package gshop.product.discount

import gshop.product.Product

class VolumeDiscount {

	static belongsTo = [product: Product]

	Integer fromAmount
	Integer toAmount
	BigDecimal fixValue
	BigDecimal percentage

	static constraints = {
		toAmount nullable: true
		fixValue nullable: true
		percentage nullable: true
	}

}
