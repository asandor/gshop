package gshop.product

import gshop.localization.CategoryStrings
import gshop.navigation.CategorySeoLink

class Category extends ShopObject {

	static hasMany = [
			products: Product,
			strings: CategoryStrings,
			seoLinks: CategorySeoLink,
			children: Category
	]

	Category parent

	Integer index = 0

	String smallImageName
	String mediumImageName

	static constraints = {
		parent nullable: true
		extId nullable: true
		smallImageName nullable: true
		mediumImageName nullable: true
	}

	static transients = ['title', 'shortDescription', 'longDescription']

	static mapping = {
		children sort: 'index', order: 'asc'
		strings lazy: false
		seoLinks lazy: false
		index column: 'idx'
	}

	String toString() { getTitle('sk') }
}
