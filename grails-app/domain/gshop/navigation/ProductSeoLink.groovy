package gshop.navigation

import gshop.product.Product

class ProductSeoLink extends SeoLink {

	static belongsTo = [product: Product]

}
