package gshop.navigation

import groovy.transform.ToString

@ToString(includes = 'path,localeId')
class SeoLink {

	String path

	String localeId

	Date dateCreated

}
