package gshop.navigation

import gshop.product.Category

class CategorySeoLink extends SeoLink {

	static belongsTo = [category: Category]

}
