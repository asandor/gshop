package gshop.security

class User {
	String username
	String passwordHash
	String passwordSalt
	String hashAlgorithm

	Date lastLogin

	static hasMany = [
			permissions: String
	]

	static constraints = {
		username blank: false, unique: true, size: 5..80
		passwordHash maxSize: 128, nullable: true
		passwordSalt maxSize: 128, nullable: true
		hashAlgorithm maxSize: 10, nullable: true

		lastLogin nullable: true
	}

	static mapping = {
		table name: 'gsuser'
		tablePerHierarchy false
	}

	String toString() { username }
}
