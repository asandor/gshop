package gshop.security

import gshop.LocalizedString

class DiscountGroup {

	String name

	static hasMany = [customers: Customer, localizedNames: LocalizedString]

	Double discountPercentage

	String getName(String localeId) {
		String localizedName = localizedNames.find { locStr -> locStr.localeId == localeId }?.string
		if (localizedName) return localizedName
		else return name
	}
}
