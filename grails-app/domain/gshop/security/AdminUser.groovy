package gshop.security

class AdminUser extends User {

	static mapping = {
		table name: 'administrator'
		tablePerHierarchy false
	}

}
