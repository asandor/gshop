package gshop.security

import gshop.Address
import gshop.basket.Basket

abstract class Customer extends User {

	static hasMany = [
			baskets: Basket,
			deliveryAddresses: Address
	]

	Address billingAddress
	String phoneNumber
	String email
	Boolean activated
	String languageCode = "sk"
	String activationCode
	String additionalInfo
	Locale locale

	static constraints = {
		phoneNumber nullable: true, maxSize: 30
		email maxSize: 80
		billingAddress nullable: true
		activationCode nullable: true
		additionalInfo nullable: true, maxSize: 150
	}

	static mapping = {
		tablePerHierarchy false
	}

	static transients = ['locale', 'deliveryAddress']

	Basket getDefaultBasket() { baskets?.find { basket -> basket.type == Basket.DEFAULT_BASKET_TYPE } }

	Address getDeliveryAddress() {
		if (deliveryAddresses) {
			deliveryAddresses.iterator().next();
		} else {
			return null
		}
	}

	Locale getLocale() {
		if (locale == null) {
			locale = new Locale(languageCode)
		}
		return locale
	}
}
