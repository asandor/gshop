package gshop.security

class PrivateCustomer extends Customer {

	String firstName
	String lastName

	static constraints = {
		firstName maxSize: 50, nullable: true
		lastName maxSize: 50, nullable: true
	}

	static mapping = {
		table name: 'private_customer'
		tablePerHierarchy false
	}

}
