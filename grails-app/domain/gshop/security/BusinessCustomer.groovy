package gshop.security

class BusinessCustomer extends Customer {

	String companyName
	String taxId //ICO
	String vat   //IC DPH
	String companyId //DIC
	String contactPerson

	static constraints = {
		companyName maxSize: 50, blank: false
		taxId nullable: true, maxSize: 30
		vat nullable: true, maxSize: 30
		companyId nullable: true, maxSize: 30
		contactPerson nullable: true, maxSize: 100
	}

	static mapping = {
		table name: 'business_customer'
		tablePerHierarchy false
	}

}