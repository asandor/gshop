package gshop

class Address {

	String firstName
	String lastName
	String companyName
	CountryCode country
	String city
	String postalCode
	String street
	String number

	String comment

	static constraints = {
		city maxSize: 50
		postalCode maxSize: 16
		street maxSize: 80
		number maxSize: 20
		comment nullable: true, maxSize: 200
		firstName nullable: true, maxSize: 50
		lastName nullable: true, maxSize: 50
		companyName nullable: true, maxSize: 100
	}

	static Address clone(Address otherAddress) {
		Address address = new Address()
		address.with {
			country = otherAddress.country
			city = otherAddress.city
			postalCode = otherAddress.postalCode
			street = otherAddress.street
			number = otherAddress.number
			comment = otherAddress.comment
			firstName = otherAddress.firstName
			lastName = otherAddress.lastName
			companyName = otherAddress.companyName
		}
		return address
	}

}
