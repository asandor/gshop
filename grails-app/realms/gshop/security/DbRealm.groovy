package gshop.security

import org.apache.commons.codec.binary.Hex
import org.apache.shiro.authc.AccountException
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.IncorrectCredentialsException
import org.apache.shiro.authc.UnknownAccountException
import org.apache.shiro.util.ByteSource

class DbRealm {
	static authTokenClass = org.apache.shiro.authc.UsernamePasswordToken

	def credentialMatcher
	def shiroPermissionResolver

	def authenticate(authToken) {
		log.debug "Attempting to authenticate ${authToken.username} in DB realm..."
		try {
			def username = authToken.username

			// Null username is invalid
			if (username == null) {
				throw new AccountException("Null usernames are not allowed by this realm.")
			}

			// Get the user with the given username. If the user is not
			// found, then they don't have an account and we throw an
			// exception.
			def user = User.findByUsername(username)
			if (!user) {
				throw new UnknownAccountException("No account found for user [${username}]")
			}

			String salt = new String(Hex.decodeHex(user.passwordSalt as char[]))
			def account = new GSAccount(username, user.passwordHash, "DbRealm", ByteSource.Util.bytes(salt), user.hashAlgorithm)

			if (authToken.password) {
				if (!credentialMatcher.doCredentialsMatch(authToken, account)) {
					throw new IncorrectCredentialsException("Invalid password for user '${username}'")
				}
			} else if (authToken.activationCode && user instanceof Customer) {
				if (user.activationCode != authToken.activationCode) {
					throw new IncorrectCredentialsException("Invalid activation code for user '${username}")
				}
			} else {
				throw new IncorrectCredentialsException("Neither password nor activationCode defined in authentication token")
			}

			return account
		} catch (AuthenticationException ex) {
			throw ex;
		} catch (Exception ex) {
			log.error("Unexpected error while authenticating user", ex)
			throw ex;
		}
	}

	def hasRole(principal, roleName) {
		def roles = User.withCriteria {
			roles {
				eq("name", roleName)
			}
			eq("username", principal)
		}

		return roles.size() > 0
	}

	def hasAllRoles(principal, roles) {
		def r = User.withCriteria {
			roles {
				'in'("name", roles)
			}
			eq("username", principal)
		}

		return r.size() == roles.size()
	}

	def isPermitted(principal, requiredPermission) {
		// Does the user have the given permission directly associated
		// with himself?
		//
		// First find all the permissions that the user has that match
		// the required permission's type and project code.
		def user = User.findByUsername(principal)
		def permissions = user.permissions

		// Try each of the permissions found and see whether any of
		// them confer the required permission.
		def retval = permissions?.find { permString ->
			// Create a real permission instance from the database
			// permission.
			def perm = shiroPermissionResolver.resolvePermission(permString)

			// Now check whether this permission implies the required
			// one.
			if (perm.implies(requiredPermission)) {
				// User has the permission!
				return true
			} else {
				return false
			}
		}

		if (retval != null) {
			// Found a matching permission!
			return true
		}

		// If not, does he gain it through a role?
		//
		// Get the permissions from the roles that the user does have.
		def results = User.executeQuery("select distinct p from User as user join user.roles as role join role.permissions as p where user.username = '$principal'")

		// There may be some duplicate entries in the results, but
		// at this stage it is not worth trying to remove them. Now,
		// create a real permission from each result and check it
		// against the required one.
		retval = results.find { permString ->
			// Create a real permission instance from the database
			// permission.
			def perm = shiroPermissionResolver.resolvePermission(permString)

			// Now check whether this permission implies the required
			// one.
			if (perm.implies(requiredPermission)) {
				// User has the permission!
				return true
			} else {
				return false
			}
		}

		if (retval != null) {
			// Found a matching permission!
			return true
		} else {
			return false
		}
	}
}
