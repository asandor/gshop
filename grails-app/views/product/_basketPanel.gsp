<%@ page import="gshop.product.Product" %>
<% Product product = product %>

<div class="wizard-dialog-panel ui-widget-content">
	<span class="amount">
		<input type="text" name="amount" value="1" size="3" class="basket-input"/>
		%{--<input type="button" class="basket-input" value="${message(code: 'product.basketpanel.add')}"--}%
		%{--onclick="submitAddProductToBasket($(this), '${product.code}', $(this).prev().val()); return false;"/>--}%
		<button class="basket-input" onclick="Application.getInstance().getBasket().addProduct('${product.code}', $(this).prev().val());">
			<g:message code='product.basketpanel.add'/>
		</button>
	</span>
</div>