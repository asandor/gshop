<% List<BigDecimal> prices = prices %>
<% List<Range> ranges = ranges %>

<div class="product-price">
	<table>
		<tr>
			<g:if test="${prices.size() == 1}">
				<td class="price-title">
					<g:message code="product.price.pricetitle"/>:
				</td>
			</g:if>
			<g:else>
				<g:each in="${prices}" var="price" status="index">
					<td class="price-title">
						<g:if test="${index == 0}">
							1 - ${ranges[index].to}
						</g:if>
						<g:elseif test="${index < prices.size() - 1}">
							${ranges[index].from} - ${ranges[index].to}
						</g:elseif>
						<g:else>
							${ranges[index].from} ...
						</g:else>
					</td>
				</g:each>
			</g:else>
		</tr>
		<tr>
			<g:each in="${prices}" var="price">
				<td class="price-value">
					<g:formatNumber number="${price}" type="currency" currencySymbol="€"/>
				</td>
			</g:each>
		</tr>
	</table>
</div>