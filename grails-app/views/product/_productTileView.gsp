<div id="products-tile-list">
</div>

<r:script disposition="defer">
	$(document).ready(function(){
		var tileList = new ProductTileList($('#products-tile-list'), '${category.extId}');
		tileList.fetchNextPage();
	});
</r:script>