<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<meta name="layout" content="main"/>
	<title><shop:pageTitle><shop:title object="${product}"/></shop:pageTitle></title>
</head>

<body>
	<div class="product-detail">
		<h1><shop:title object="${product}"/></h1>

		<table>
			<tr>
				<td>
					<div class="image">
						<shop:objectImage object="${product}" type="${gshop.product.ObjectImageType.LARGE}"/>
					</div>
				</td>
				<td>
					<div class="details">
						<table>
							<tr>
								<td><g:message code="product.code"/>:</td><td style="text-align: right">${product.code}</td>
							</tr>
							<tr>
								<td style="padding-top: 15px">
									<shop:longDescription object="${product}"/>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="actions">
						<shop:productPrice product="${product}"/>
						<shop:basketPanel product="${product}"/>
					</div>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>