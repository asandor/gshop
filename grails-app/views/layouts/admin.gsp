<!doctype html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><g:layoutTitle"/></title>
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<script src="${createLink(uri: '/init/UrlMappings')}" type="text/javascript"></script>
	<g:layoutHead/>
	<r:layoutResources/>
	<r:script>
		<shop:renderLocalizedStrings/>
	</r:script>
</head>

<body>
	<div id="body-content">
		<div id="page-body">
			<div id="admin-navigation">
				<ul>
					<li><g:link controller="orderManagement" action="list"><g:message code="orderManagement.link" default="Orders"/></g:link></li>
					<li><g:link controller="catalogManagement" action="index"><g:message code="catalogManagement.link" default="Catalog"/></g:link></li>
					<li><g:link controller="seoLink" action="index"><g:message code="seoManagement.link" default="SEO Links"/></g:link></li>
				</ul>
			</div>

			<div id="content">
				<g:layoutBody/>
			</div>
		</div>
	</div>

	<div class="footer" role="contentinfo"></div>
	<r:layoutResources/>
</body>