<%@ page import="java.text.DecimalFormatSymbols; gshop.product.ObjectImageType; gshop.basket.Basket; org.springframework.web.servlet.support.RequestContextUtils as RCU" %>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><g:layoutTitle/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">

	<script src="${createLink(uri: '/init/UrlMappings')}" type="text/javascript"></script>

	<script src="${createLink(controller: 'localization', action: 'messages')}" type="text/javascript"></script>

	<g:layoutHead/>
	<r:require modules="catalog"/>
	<r:layoutResources/>
</head>

<body>
	<div id="body-content">

		<div id="top-panel" role="banner">

			<div id="top-panel-top">
				<div id="logo">
					<a href="${createLink(uri: '/')}">
						<g:img dir="images" file="logo.png"/>
					</a>
				</div>

				<div id="basket" class="basket-small">
					<button id="basket-button" title="<g:message code="basket.open"/>"></button>
				</div>
			</div>

			<div id="my-navigation">
			</div>

			<div id="authentication-panel">
				<% //has to be here on page load othewise browser autocomplete won't work                                %>
				<div id="signin-panel" style="display: none;">
					<div id="signin-form">
						<div class="message"></div>

						<form>
							<table>
								<tr>
									<td><g:message code="login.username"/></td>
									<td colspan="2"><input type="text" name="username"></td>
								</tr>
								<tr>
									<td><g:message code="login.password"/></td>
									<td><input type="password" name="password"></td>
									<td id="forgottenPassword">Nepamätám si</td>
								</tr>
								<tr>
									<td colspan="3"><g:message code="login.rememberme"/>
										<input type="checkbox" name="rememberMe"/></td>
								</tr>
								<tr>
									<td colspan="3"><button type="button" class="submit"><g:message
											code="login.loginbtn"/></button>
										<a href="#" id="signin-form-hide-link">&nbsp;</a>
									</td>
								</tr>
							</table>

						</form>
					</div>

					<div id="signin-info-panel">
						<a href="#" id="login-link" onclick=""><g:message code="login.loginbtn"/></a>
						<a href="#" id="registration-link" onclick=""><g:message code="login.register"/></a>
					</div>
				</div>

				<div id="signout-panel" style="display: none;">
					<button id="signout-button"><g:message code="login.logoutbtn"/></button>
				</div>
			</div>
		</div>

		<div id="topnav" class="topnav">

			<nav id="breadcrumb-container" role="nav">
				<g:if test="${category}">
					<shop:breadcrumb category="${category}"/>
				</g:if>
				<g:elseif test="${product}">
					<shop:breadcrumb category="${product.category}"/>
				</g:elseif>
			</nav>
		</div>

		<div id="page-body" role="main">

			<g:if test="${!hideLeftPanel}">
				<div id="category-nav">
					<shop:categoryMenu selectedCategory="${category}"/>
				</div>

				<div id="content" role="content">
					<shop:flashError/>

					<g:layoutBody/>
				</div>
			</g:if>
			<g:else>
				<div id="my-content" role="content">
					<shop:flashError/>

					<g:layoutBody/>
				</div>
			</g:else>
			<div class="footer" role="contentinfo"></div>
			<r:layoutResources/>
		</div>
	</div>

</body>
</html>