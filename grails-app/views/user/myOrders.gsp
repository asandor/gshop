<%@ page import="gshop.security.PrivateCustomer" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <g:set var="hideLeftPanel" value="${true}" scope="request"/>
    <meta name="layout" content="main"/>
    <title><shop:pageTitle><g:message code="mydata.title"/></shop:pageTitle></title>
    <r:require modules="myOrders"/>
</head>

<body>
<div id="orderTable"></div>

<div id="orderDetailWidget"></div>
</body>
</html>