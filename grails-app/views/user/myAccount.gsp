<%@ page import="gshop.security.PrivateCustomer" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <g:set var="hideLeftPanel" value="${true}" scope="request"/>
    <meta name="layout" content="main"/>
    <title><shop:pageTitle><g:message code="mydata.title"/></shop:pageTitle></title>
    <r:require modules="myAccount"/>
</head>

<body>
<h1 class="my-account"><g:message code="mydata.user.name"/>:<span id="username"></span></h1>

<div class="user-details-panel">

    <div class="user-data-wrapper">
        <div class="user-data-form"></div>

        <div class="user-password-form"></div>
    </div>

    <div class="user-address-wrapper">
        <div class="user-billing-address-wrapper">
            <div class="user-billing-address-form"></div>

            <g:message code="userdata.deliveryAddressNotAsBillingAddress"/>
            <g:checkBox name="deliveryAddressActive" value="${customer.deliveryAddresses.size() > 0}"/>
        </div>

        <div class="user-delivery-address-form" style="display: none"></div>
    </div>

</div>
</body>
</html>