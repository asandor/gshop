<ul class="xbreadcrumbs" id="breadcrumb">
  <g:each in="${path}" var="category">
    <li>
      <shop:link target="${category}"><shop:title object="${category}"/></shop:link>
      <g:if test="${category.parent}">
        <ul>
          <g:each in="${category.parent.children.sort({it.index})}" var="sibling">
            <li>
              <shop:link target="${sibling}"><shop:title object="${sibling}"/></shop:link>
            </li>
          </g:each>
        </ul>
      </g:if>
    </li>
  </g:each>
</ul>