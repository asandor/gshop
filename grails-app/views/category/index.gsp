<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>
    <shop:pageTitle><shop:title object="${category}"/></shop:pageTitle>
  </title>
</head>

<body>
<g:hiddenField name="categoryId" value="${category.extId}"/>
<div class="category-title">
  <h1><shop:title object="${category}"/></h1>
</div>
<g:if test="${category.children}">
  <shop:categoryTileView category="${category}"/>
</g:if>
<g:else>
  <div id="products-tile-list"></div>
</g:else>
</body>
</html>