<%@ page import="gshop.product.Category" %>
<% Collection<Category> categories = categories %>
<g:set var="columnCount" value="${6}"/>
<g:set var="rowCount" value="${Math.ceil(categories.size() / columnCount) as int}"/>

<div class="category-tile-view">
    <table>
        <tbody>
        <g:set var="iterator" value="${categories.iterator()}"/>
        <g:each in="${1..rowCount}">
            <tr>
                <g:each in="${1..columnCount}">
                    <td>
                        <g:if test="${iterator.hasNext()}">
                            <shop:categoryTile category="${iterator.next()}"/>
                        </g:if>
                    </td>
                </g:each>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>