<%@ page import="gshop.product.ObjectImageType; gshop.product.Category" %>
<% Category category = category %>

<div class="category-tile">
	<div class="mouse-event-catcher"></div>

	<shop:link target="${category}">
		<shop:objectImage object="${category}" type="${ObjectImageType.MEDIUM}"/>
	</shop:link>

	<div class="title">
		<shop:link target="${category}"><shop:title object="${category}"/></shop:link>
	</div>
</div>