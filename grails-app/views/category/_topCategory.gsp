<div class="category top-category ui-widget-content ui-corner-all">
    <div class="category-header">
        <shop:link target="${category}"><shop:title object="${category}"/></shop:link>
    </div>
    <ul>
        <g:each in="${category.children.sort({ it.index })}" var="subCategory">
            <li>
                <span class="subcategory">
                    <shop:link target="${subCategory}"><shop:title object="${subCategory}"/></shop:link>
                </span>
            </li>
        </g:each>
    </ul>
</div>
