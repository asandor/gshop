<g:if test="${flash.error}">
	<span id="error-message">
		<g:message code="${flash.error.code}" args="${flash.error.arguments}"/>
	</span>
</g:if>