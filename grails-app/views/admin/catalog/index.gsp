<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
	<title><g:message code="catalog.management.index.title" default="Catalog Management"/></title>
	<meta name="layout" content="admin">
	<r:require modules="catalogAdmin"/>
</head>

<body>
	<div id="left-panel">
		<div id="category-tree"></div>
	</div>

	<div id="right-panel">
		<div id="selected-category"></div>

		<div id="product-grid"></div>
	</div>
</body>
</html>