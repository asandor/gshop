<%@ page import="gshop.order.OrderState; java.text.DecimalFormat; gshop.order.Order; org.springframework.web.servlet.support.RequestContextUtils as RCU" %>
<!doctype html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'order.label', default: 'Order')}"/>
	<title><g:message code="default.list.label" args="[entityName]"/></title>
	<r:require modules="catalog, orderManagement"/>
</head>

<body>
	<div id="list-order" class="content scaffold-list" role="main">
		<h1><g:message code="default.list.label" args="[entityName]"/></h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<g:form class="filter-panel">
			<table>
				%{--<tr>
					<td><g:message code="order.filter.date_from" default="Date from"/></td>
					<td><g:datePicker name="filter.date_from" precision="day"/></td>
				</tr>
				<tr>
					<td><g:message code="order.filter.date_to" default="Date to"/></td>
					<td><g:datePicker name="filter.date_to" precision="day"/></td>
				</tr>--}%
				<tr>
					<td><g:message code="order.filter.name" default="Name"/></td>
					<td><g:textField name="filter.name" value="${params['filter.name']}"/></td>
				</tr>
			</table>
			<g:submitButton name="list"/>
		</g:form>
		<table>
			<thead>
				<tr>
					<g:sortableColumn property="dateCreated" title="${message(code: 'order.dateCreated.label', default: 'Date Created')}"/>

					<g:sortableColumn property="orderNumber" title="${message(code: 'order.orderNumber.label', default: 'Order Number')}"/>

					<th><g:message code="order.uid.label" default="UID"/></th>

					<th><g:message code="order.customer.label" default="Customer"/></th>

					<th><g:message code="order.price.label" default="Price"/></th>

					<g:sortableColumn property="state" title="${message(code: 'order.state.label', default: 'State')}"/>

					<th></th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${orderInstanceList}" status="i" var="orderInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td><g:formatDate date="${orderInstance.dateCreated}" type="datetime" style="MEDIUM"/></td>

						<td><g:fieldValue field="orderNumber" bean="${orderInstance}"/></td>

						<td><g:fieldValue field="uid" bean="${orderInstance}"/></td>

						<g:if test="${orderInstance.customer.companyName}">
							<td>${orderInstance.customer.companyName}</td>
						</g:if>
						<g:else>
							<td>${orderInstance.customer.firstName} ${orderInstance.customer.lastName}</td>
						</g:else>

						<td>&euro; <g:formatNumber number="${orderInstance.price.baseWithVat}"/></td>

						<td><g:link action="show" id="${orderInstance.uid}"><g:message code="order.action.open" default="Open"/></g:link></td>

						<td>
							<g:hiddenField name="orderId" value="${orderInstance.uid}"/>
							<g:select name="orderState" from="${OrderState.values()}" value="${orderInstance.state}"/>
							<g:submitButton name="updateStateButton" value="${g.message(code: 'orderManagement.stateChange.submit', default: 'Update state')}"/>
						</td>
					</tr>
				</g:each>
			</tbody>
		</table>

		<div class="pagination">
			<g:paginate total="${orderInstanceTotal}"/>
		</div>
	</div>
</body>
</html>
