<%@ page import="gshop.order.Order" %>


<fieldset class="embedded"><legend><g:message code="order.discountInfo.label" default="Discount Info"/></legend>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'discountInfo.id', 'error')} required">
		<label for="discountInfo.id">
			<g:message code="order.discountInfo.id.label" default="Id"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="id" required="" value="${discountInfoInstance.id}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'discountInfo.longDescription', 'error')} ">
		<label for="discountInfo.longDescription">
			<g:message code="order.discountInfo.longDescription.label" default="Long Description"/>

		</label>
		<g:textField name="longDescription" value="${discountInfoInstance?.longDescription}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'discountInfo.shortDescription', 'error')} ">
		<label for="discountInfo.shortDescription">
			<g:message code="order.discountInfo.shortDescription.label" default="Short Description"/>

		</label>
		<g:textField name="shortDescription" value="${discountInfoInstance?.shortDescription}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'discountInfo.version', 'error')} required">
		<label for="discountInfo.version">
			<g:message code="order.discountInfo.version.label" default="Version"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="version" required="" value="${discountInfoInstance.version}"/>
	</div>
</fieldset><fieldset class="embedded"><legend><g:message code="order.billingAddress.label" default="Billing Address"/></legend>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.city', 'error')} ">
		<label for="billingAddress.city">
			<g:message code="order.billingAddress.city.label" default="City"/>

		</label>
		<g:textField name="city" value="${addressInstance?.city}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.comment', 'error')} ">
		<label for="billingAddress.comment">
			<g:message code="order.billingAddress.comment.label" default="Comment"/>

		</label>
		<g:textField name="comment" value="${addressInstance?.comment}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.country', 'error')} ">
		<label for="billingAddress.country">
			<g:message code="order.billingAddress.country.label" default="Country"/>

		</label>
		<g:textField name="country" value="${addressInstance?.country}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.designation', 'error')} ">
		<label for="billingAddress.designation">
			<g:message code="order.billingAddress.designation.label" default="Designation"/>

		</label>
		<g:textField name="designation" value="${addressInstance?.designation}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.id', 'error')} required">
		<label for="billingAddress.id">
			<g:message code="order.billingAddress.id.label" default="Id"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="id" required="" value="${addressInstance.id}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.number', 'error')} ">
		<label for="billingAddress.number">
			<g:message code="order.billingAddress.number.label" default="Number"/>

		</label>
		<g:textField name="number" value="${addressInstance?.number}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.postalCode', 'error')} ">
		<label for="billingAddress.postalCode">
			<g:message code="order.billingAddress.postalCode.label" default="Postal Code"/>

		</label>
		<g:textField name="postalCode" value="${addressInstance?.postalCode}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.street', 'error')} ">
		<label for="billingAddress.street">
			<g:message code="order.billingAddress.street.label" default="Street"/>

		</label>
		<g:textField name="street" value="${addressInstance?.street}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'billingAddress.version', 'error')} required">
		<label for="billingAddress.version">
			<g:message code="order.billingAddress.version.label" default="Version"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="version" required="" value="${addressInstance.version}"/>
	</div>
</fieldset><fieldset class="embedded"><legend><g:message code="order.customer.label" default="Customer"/></legend>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'customer.firstName', 'error')} ">
		<label for="customer.firstName">
			<g:message code="order.customer.firstName.label" default="First Name"/>

		</label>
		<g:textField name="firstName" value="${orderCustomerInstance?.firstName}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'customer.id', 'error')} required">
		<label for="customer.id">
			<g:message code="order.customer.id.label" default="Id"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="id" required="" value="${orderCustomerInstance.id}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'customer.lastName', 'error')} ">
		<label for="customer.lastName">
			<g:message code="order.customer.lastName.label" default="Last Name"/>

		</label>
		<g:textField name="lastName" value="${orderCustomerInstance?.lastName}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'customer.phoneNumber', 'error')} ">
		<label for="customer.phoneNumber">
			<g:message code="order.customer.phoneNumber.label" default="Phone Number"/>

		</label>
		<g:textField name="phoneNumber" value="${orderCustomerInstance?.phoneNumber}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'customer.username', 'error')} ">
		<label for="customer.username">
			<g:message code="order.customer.username.label" default="Username"/>

		</label>
		<g:textField name="username" value="${orderCustomerInstance?.username}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'customer.version', 'error')} required">
		<label for="customer.version">
			<g:message code="order.customer.version.label" default="Version"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="version" required="" value="${orderCustomerInstance.version}"/>
	</div>
</fieldset><fieldset class="embedded"><legend><g:message code="order.deliveryAddress.label" default="Delivery Address"/></legend>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.city', 'error')} ">
		<label for="deliveryAddress.city">
			<g:message code="order.deliveryAddress.city.label" default="City"/>

		</label>
		<g:textField name="city" value="${addressInstance?.city}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.comment', 'error')} ">
		<label for="deliveryAddress.comment">
			<g:message code="order.deliveryAddress.comment.label" default="Comment"/>

		</label>
		<g:textField name="comment" value="${addressInstance?.comment}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.country', 'error')} ">
		<label for="deliveryAddress.country">
			<g:message code="order.deliveryAddress.country.label" default="Country"/>

		</label>
		<g:textField name="country" value="${addressInstance?.country}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.designation', 'error')} ">
		<label for="deliveryAddress.designation">
			<g:message code="order.deliveryAddress.designation.label" default="Designation"/>

		</label>
		<g:textField name="designation" value="${addressInstance?.designation}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.id', 'error')} required">
		<label for="deliveryAddress.id">
			<g:message code="order.deliveryAddress.id.label" default="Id"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="id" required="" value="${addressInstance.id}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.number', 'error')} ">
		<label for="deliveryAddress.number">
			<g:message code="order.deliveryAddress.number.label" default="Number"/>

		</label>
		<g:textField name="number" value="${addressInstance?.number}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.postalCode', 'error')} ">
		<label for="deliveryAddress.postalCode">
			<g:message code="order.deliveryAddress.postalCode.label" default="Postal Code"/>

		</label>
		<g:textField name="postalCode" value="${addressInstance?.postalCode}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.street', 'error')} ">
		<label for="deliveryAddress.street">
			<g:message code="order.deliveryAddress.street.label" default="Street"/>

		</label>
		<g:textField name="street" value="${addressInstance?.street}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'deliveryAddress.version', 'error')} required">
		<label for="deliveryAddress.version">
			<g:message code="order.deliveryAddress.version.label" default="Version"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="version" required="" value="${addressInstance.version}"/>
	</div>
</fieldset>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'orderNumber', 'error')} required">
	<label for="orderNumber">
		<g:message code="order.orderNumber.label" default="Order Number"/>
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="orderNumber" required="" value="${orderInstance.orderNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'paymentMethodPrice', 'error')} required">
	<label for="paymentMethodPrice">
		<g:message code="order.paymentMethodPrice.label" default="Payment Method Price"/>
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="paymentMethodPrice" step="any" required="" value="${orderInstance.paymentMethodPrice}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'paymentMethodTitle', 'error')} ">
	<label for="paymentMethodTitle">
		<g:message code="order.paymentMethodTitle.label" default="Payment Method Title"/>

	</label>
	<g:textField name="paymentMethodTitle" value="${orderInstance?.paymentMethodTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'paymentReceived', 'error')} ">
	<label for="paymentReceived">
		<g:message code="order.paymentReceived.label" default="Payment Received"/>

	</label>
	<g:checkBox name="paymentReceived" value="${orderInstance?.paymentReceived}"/>
</div>
<fieldset class="embedded"><legend><g:message code="order.price.label" default="Price"/></legend>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'price.base', 'error')} required">
		<label for="price.base">
			<g:message code="order.price.base.label" default="Base"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="base" step="any" required="" value="${priceInstance.base}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'price.baseWithVat', 'error')} required">
		<label for="price.baseWithVat">
			<g:message code="order.price.baseWithVat.label" default="Base With Vat"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="baseWithVat" step="any" required="" value="${priceInstance.baseWithVat}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'price.id', 'error')} required">
		<label for="price.id">
			<g:message code="order.price.id.label" default="Id"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="id" required="" value="${priceInstance.id}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'price.vatAmount', 'error')} required">
		<label for="price.vatAmount">
			<g:message code="order.price.vatAmount.label" default="Vat Amount"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="vatAmount" step="any" required="" value="${priceInstance.vatAmount}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'price.vatPercent', 'error')} required">
		<label for="price.vatPercent">
			<g:message code="order.price.vatPercent.label" default="Vat Percent"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="vatPercent" step="any" required="" value="${priceInstance.vatPercent}"/>
	</div>

	<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'price.version', 'error')} required">
		<label for="price.version">
			<g:message code="order.price.version.label" default="Version"/>
			<span class="required-indicator">*</span>
		</label>
		<g:field type="number" name="version" required="" value="${priceInstance.version}"/>
	</div>
</fieldset>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'products', 'error')} ">
	<label for="products">
		<g:message code="order.products.label" default="Products"/>

	</label>
	<g:select name="products" from="${gshop.order.OrderProduct.list()}" multiple="multiple" optionKey="id" size="5" value="${orderInstance?.products*.id}"
	          class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'shipmentMethodPrice', 'error')} required">
	<label for="shipmentMethodPrice">
		<g:message code="order.shipmentMethodPrice.label" default="Shipment Method Price"/>
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="shipmentMethodPrice" step="any" required="" value="${orderInstance.shipmentMethodPrice}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'shipmentMethodTitle', 'error')} ">
	<label for="shipmentMethodTitle">
		<g:message code="order.shipmentMethodTitle.label" default="Shipment Method Title"/>

	</label>
	<g:textField name="shipmentMethodTitle" value="${orderInstance?.shipmentMethodTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'state', 'error')} required">
	<label for="state">
		<g:message code="order.state.label" default="State"/>
		<span class="required-indicator">*</span>
	</label>
	<g:select name="state" from="${gshop.order.OrderState?.values()}" keys="${gshop.order.OrderState.values()*.name()}" required=""
	          value="${orderInstance?.state?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: orderInstance, field: 'uid', 'error')} ">
	<label for="uid">
		<g:message code="order.uid.label" default="Uid"/>

	</label>
	<g:textField name="uid" value="${orderInstance?.uid}"/>
</div>

