<%@ page import="gshop.order.Order" %>
<!doctype html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'order.label', default: 'Order')}"/>
	<title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
	<a href="#show-order" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

	<div class="nav" role="navigation">
		<ul>
			<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
			<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link></li>
		</ul>
	</div>

	<div id="show-order" class="content scaffold-show" role="main">
		<h1><g:message code="default.show.label" args="[entityName]"/></h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<ol class="property-list order">

			<g:if test="${orderInstance?.discountInfo}">
				<li class="fieldcontain">
					<span id="discountInfo-label" class="property-label"><g:message code="order.discountInfo.label" default="Discount Info"/></span>

					<span class="property-value" aria-labelledby="discountInfo-label"><g:fieldValue bean="${orderInstance}" field="discountInfo"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.billingAddress}">
				<li class="fieldcontain">
					<span id="billingAddress-label" class="property-label"><g:message code="order.billingAddress.label" default="Billing Address"/></span>

					<span class="property-value" aria-labelledby="billingAddress-label"><g:fieldValue bean="${orderInstance}" field="billingAddress"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.customer}">
				<li class="fieldcontain">
					<span id="customer-label" class="property-label"><g:message code="order.customer.label" default="Customer"/></span>

					<span class="property-value" aria-labelledby="customer-label"><g:fieldValue bean="${orderInstance}" field="customer"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="order.dateCreated.label" default="Date Created"/></span>

					<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${orderInstance?.dateCreated}"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.deliveryAddress}">
				<li class="fieldcontain">
					<span id="deliveryAddress-label" class="property-label"><g:message code="order.deliveryAddress.label" default="Delivery Address"/></span>

					<span class="property-value" aria-labelledby="deliveryAddress-label"><g:fieldValue bean="${orderInstance}" field="deliveryAddress"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.orderNumber}">
				<li class="fieldcontain">
					<span id="orderNumber-label" class="property-label"><g:message code="order.orderNumber.label" default="Order Number"/></span>

					<span class="property-value" aria-labelledby="orderNumber-label"><g:fieldValue bean="${orderInstance}" field="orderNumber"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.paymentMethodPrice}">
				<li class="fieldcontain">
					<span id="paymentMethodPrice-label" class="property-label"><g:message code="order.paymentMethodPrice.label"
					                                                                      default="Payment Method Price"/></span>

					<span class="property-value" aria-labelledby="paymentMethodPrice-label"><g:fieldValue bean="${orderInstance}"
					                                                                                      field="paymentMethodPrice"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.paymentMethodTitle}">
				<li class="fieldcontain">
					<span id="paymentMethodTitle-label" class="property-label"><g:message code="order.paymentMethodTitle.label"
					                                                                      default="Payment Method Title"/></span>

					<span class="property-value" aria-labelledby="paymentMethodTitle-label"><g:fieldValue bean="${orderInstance}"
					                                                                                      field="paymentMethodTitle"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.paymentReceived}">
				<li class="fieldcontain">
					<span id="paymentReceived-label" class="property-label"><g:message code="order.paymentReceived.label" default="Payment Received"/></span>

					<span class="property-value" aria-labelledby="paymentReceived-label"><g:formatBoolean boolean="${orderInstance?.paymentReceived}"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.price}">
				<li class="fieldcontain">
					<span id="price-label" class="property-label"><g:message code="order.price.label" default="Price"/></span>

					<span class="property-value" aria-labelledby="price-label"><g:fieldValue bean="${orderInstance}" field="price"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.products}">
				<li class="fieldcontain">
					<span id="products-label" class="property-label"><g:message code="order.products.label" default="Products"/></span>

					<g:each in="${orderInstance.products}" var="p">
						<span class="property-value" aria-labelledby="products-label"><g:link controller="orderProduct" action="show"
						                                                                      id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
					</g:each>

				</li>
			</g:if>

			<g:if test="${orderInstance?.shipmentMethodPrice}">
				<li class="fieldcontain">
					<span id="shipmentMethodPrice-label" class="property-label"><g:message code="order.shipmentMethodPrice.label"
					                                                                       default="Shipment Method Price"/></span>

					<span class="property-value" aria-labelledby="shipmentMethodPrice-label"><g:fieldValue bean="${orderInstance}"
					                                                                                       field="shipmentMethodPrice"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.shipmentMethodTitle}">
				<li class="fieldcontain">
					<span id="shipmentMethodTitle-label" class="property-label"><g:message code="order.shipmentMethodTitle.label"
					                                                                       default="Shipment Method Title"/></span>

					<span class="property-value" aria-labelledby="shipmentMethodTitle-label"><g:fieldValue bean="${orderInstance}"
					                                                                                       field="shipmentMethodTitle"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.state}">
				<li class="fieldcontain">
					<span id="state-label" class="property-label"><g:message code="order.state.label" default="State"/></span>

					<span class="property-value" aria-labelledby="state-label"><g:fieldValue bean="${orderInstance}" field="state"/></span>

				</li>
			</g:if>

			<g:if test="${orderInstance?.uid}">
				<li class="fieldcontain">
					<span id="uid-label" class="property-label"><g:message code="order.uid.label" default="Uid"/></span>

					<span class="property-value" aria-labelledby="uid-label"><g:fieldValue bean="${orderInstance}" field="uid"/></span>

				</li>
			</g:if>

		</ol>
		<g:form>
			<fieldset class="buttons">
				<g:hiddenField name="id" value="${orderInstance?.id}"/>
				<g:link class="edit" action="edit" id="${orderInstance?.id}"><g:message code="default.button.edit.label" default="Edit"/></g:link>
				<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}"
				                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
