<!doctype html>
<html>
<head>
	<meta name="layout" content="main">
	<title><g:message code="admin.seo.title" default="Webshop Administration - SEO Links"/></title>
</head>

<body>
	<table id="seo-state">
		<tr>
			<td><g:message code="admin.seo.numberof" default="Number of SEO links"/>:</td>
			<td>${seoCount}</td>
			<td><button id="generateAll"><g:message code="admin.seo.generate-button" default="Regenerate"/></button></td>
		</tr>
		<tr>
			<td><g:message code="admin.seo.numberofCategories" default="Number of Category SEO links"/>:</td>
			<td>${categorySeoCount}</td>
			<td><button id="generateCategories"><g:message code="admin.seo.generate-button" default="Regenerate"/></button></td>
		</tr>
		<tr>
			<td><g:message code="admin.seo.numberofProducts" default="Number of Product SEO links"/>:</td>
			<td>${productSeoCount}</td>
			<td><button id="generateProducts"><g:message code="admin.seo.generate-button" default="Regenerate"/></button></td>
		</tr>
	</table>
</body>
</html>
