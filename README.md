# gshop #

a shopping cart and item catalogue plugin for Grails

### Description ###

gshop is a shopping cart and item catalogue implementation. It is implemented as a Grails plugin so it can be imported into any Grails application and extended. It provides a full-fledged HTML5 shopping cart (with user management) and item catalogue management.
What it's missing is an admin interface to manage the catalogue and other aspects of the shop.