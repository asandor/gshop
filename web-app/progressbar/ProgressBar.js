function ProgressBar() {
	this.rootElement = $('<div id="progress-bar">');

	this.rootElement.progressbar({
		value: false
	});

	this.rootElement.hide();
}

ProgressBar.prototype.hide = function () {
	console.debug("Hiding ProgressBar");
	this.rootElement.detach();
};

ProgressBar.prototype.show = function () {
	console.debug("Displaying ProgressBar");
	$('body').append(this.rootElement);
	this.rootElement.show();
};
