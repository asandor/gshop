$(document).ready(function () {
	new OrderManagement().startup();
});

function OrderManagement() {
	CommonModuleManager.call(this, []);
}

inherit(OrderManagement, CommonModuleManager);

OrderManagement.prototype.onModuleInitializationComplete = function () {
	var me = this;
	$('[name="updateStateButton"]').button().click(function () {
		me.updateOrderState(this);
	});
};

OrderManagement.prototype.updateOrderState = function (button) {
	var orderId = $(button).parent().find('[name="orderId"]').val();
	var orderState = $(button).parent().find('[name="orderState"]').val();
	OrderManagementController.getInstance().updateState(orderId, orderState, function () {
		$.gritter.add({
			title: '',
			text: document.locstr['order.state_update.success'],
			class_name: 'congratulations'
		});
	}, function () {
		$.gritter.add({
			title: '',
			text: document.locstr['order.state_update.fail'],
			class_name: 'warning'
		});
	});
};
