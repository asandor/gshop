"use strict";

function OrderManagementController() {

}

OrderManagementController.prototype.updateState = function (orderId, state, successCallback, failureCallback) {
	$.post(UrlMappings.getUrl('OrderManagementController.updateState'), { orderId: orderId, state: state })
			.error(function (e) {
				if (failureCallback) failureCallback(e)
			})
			.success(function (resp) {
				if (successCallback) successCallback();
			});
};

OrderManagementController.getInstance = function () {
	return orderManagementController;
};

var orderManagementController = new OrderManagementController();