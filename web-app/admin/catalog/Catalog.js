function Catalog() {

	this.progressBar = new ProgressBar();

	this.productTable = new ProductTable();

	this.categoryTree = new CategoryTree(this.progressBar);
	this.categoryTree.loadCategories();
	var me = this;
	this.categoryTree.selectionListener = function (categoryId) {
		if (categoryId != null) {
			CatalogManagementController.getInstance().categoryDetails(categoryId, function (resp) {
				me.productTable.displayProducts(resp.products);
			});
		} else {
			me.productTable.displayProducts([]);
		}
	};
}

$('document').ready(function () {
	var catalog = new Catalog();
});