function ProductTable() {
	this.grid = new dhtmlXGridObject('product-grid');
	this.grid.setImagePath(UrlMappings.getUrl('dhtmlXGrid.imagePath') + "/");
	this.grid.setHeader("Code,Title,Price");
	this.grid.setColumnIds("code,title,price");
	this.grid.setInitWidths("*,150,150");
	this.grid.setColAlign("left,right,right");
	this.grid.init();
}

ProductTable.prototype.displayProducts = function (products) {
	this.grid.parse({data: products}, "js");
};
