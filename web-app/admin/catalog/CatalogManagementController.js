function CatalogManagementController() {

}

CatalogManagementController.prototype.categories = function (successCallback) {
	$.get(UrlMappings.getUrl('CatalogManagementController.categories'))
			.error(function (e) {
				throw e;
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				successCallback(resp);
			});
};

CatalogManagementController.prototype.categoryDetails = function (categoryId, successCallback) {
	$.get(UrlMappings.getUrl('CatalogManagementController.categoryDetails'), {catId: categoryId})
			.error(function (e) {
				throw e;
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				successCallback(resp);
			});
};

CatalogManagementController.getInstance = function () {
	return catalogManagementController;
};

var catalogManagementController = new CatalogManagementController();