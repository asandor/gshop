// This file was automatically generated from CategoryEditForm-layout.soy.
// Please don't edit this file by hand.

if (typeof org == 'undefined') { var org = {}; }
if (typeof org.gs == 'undefined') { org.gs = {}; }
if (typeof org.gs.admin == 'undefined') { org.gs.admin = {}; }
if (typeof org.gs.admin.catalog == 'undefined') { org.gs.admin.catalog = {}; }


org.gs.admin.catalog.categoryEditForm = function(opt_data, opt_ignored) {
  return '<div id="categoryEditForm"><table><tr><td colspan="2" class="value">' + soy.$$escapeHtml(opt_data.category.title) + '</td></tr><tr><td>ID:</td><td class="value">' + soy.$$escapeHtml(opt_data.category.extId) + '</td></tr></div>';
};
