function CategoryTree(progressBar) {

	this.progressBar = progressBar;

	this.tree = new dhtmlXTreeObject('category-tree', "100%", "100%", 0);
	this.tree.setImagePath(UrlMappings.getUrl('dhtmlXTree.imagePath') + "/");
	var me = this;
	this.tree.attachEvent("onSelect", function (id) {
		console.debug("Selected category: " + id);
		if (me.selectionListener) {
			me.selectionListener(id);
		}
	});
}

CategoryTree.prototype.loadCategories = function () {
	console.info("Starting to load Category tree");
	this.progressBar.show();
	var me = this;
	CatalogManagementController.getInstance().categories(function (rootCategory) {
		console.info("Category tree loaded");
		me.tree.loadJSONObject(rootCategory);
		me.progressBar.hide();
	});
};

