function SeoLink() {

	this.generateAllButton = $('button#generateAll');
	this.generateCategoriesButton = $('button#generateCategories');
	this.generateProductsButton = $('button#generateProducts');

	var me = this;
	this.generateAllButton.click(function () {
		me.hideButtons();
		$(this).parent().append("Generating...");
		window.location.href = UrlMappings.getUrl('SeoLinkController.generateAll')
	});
	this.generateCategoriesButton.click(function () {
		me.hideButtons();
		$(this).parent().append("Generating...");
		window.location.href = UrlMappings.getUrl('SeoLinkController.generateCategory')
	});
	this.generateProductsButton.click(function () {
		me.hideButtons();
		$(this).parent().append("Generating...");
		window.location.href = UrlMappings.getUrl('SeoLinkController.generateProduct')
	});
}

SeoLink.prototype.hideButtons = function () {
	this.generateAllButton.hide();
	this.generateCategoriesButton.hide();
	this.generateProductsButton.hide();
};

var seoLink = new SeoLink();