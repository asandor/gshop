function ModuleManager(modules) {
	this.modules = modules;
	this.moduleNames = [];
	for (var i = 0; i < this.modules.length; i++) {
		this.moduleNames.push(this.modules[i].name);
	}
}

ModuleManager.prototype.startup = function () {
	var me = this;
	InitController.getInstance().fetchInitData(this.moduleNames, function (initData) {
		me.initDataReceived(initData);
	});
};

ModuleManager.prototype.moduleInitialized = function (module) {
	console.log("Module " + module.name + " initialized");
};

ModuleManager.prototype.initDataReceived = function (initData) {
	console.log("Initializing modules");

	for (var i = 0; i < this.modules.length; i++) {
		this.modules[i].manager = this;
		this.modules[i].initialize(initData[this.moduleNames[i]]);
		this.modules[i].initialized = true;
		this.moduleInitialized(this.modules[i]);
	}

	this.onModuleInitializationComplete();
};

ModuleManager.prototype.onModuleInitializationComplete = function () {
};

ModuleManager.prototype.getModule = function (name) {
	for (var i = 0; i < this.modules.length; i++) {
		if (this.modules[i].name === name) return this.modules[i];
	}
};