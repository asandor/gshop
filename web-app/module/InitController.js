function InitController() {
}

InitController.prototype.fetchInitData = function (modules, successCallback, errorCallback) {
	var dataString = JSON.stringify({modules: modules});
	jQuery.ajax({
		url: UrlMappings.getUrl('InitController.fetchInitData'),
		type: "POST",
		data: dataString,
		contentType: "application/json; charset=utf-8",
		success: function (resp) {
			if (successCallback) successCallback(resp);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			try {
				var error = $.parseJSON(jqXHR.responseText);
				if (errorCallback) errorCallback(error.code, error.message);
			} catch (ex) {
				if (errorCallback) errorCallback();
			}
		}
	});
};

InitController.getInstance = function () {
	return initController;
};

var initController = new InitController();