function CommonModuleManager(additionalModules) {
	var commonModules = [
		new User(),
		new Basket()
	];
	ModuleManager.call(this, commonModules.concat(additionalModules))
}

inherit(CommonModuleManager, ModuleManager);