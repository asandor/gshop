"use strict";

function ObjectImageController() {

}

ObjectImageController.prototype.createProductImageElement = function (product, type) {
	var url = UrlMappings.getUrl('ObjectImageController.getImage') + "/" + product.code + '.' + product.imageExtension + '?type=' + type;
	var height = type === 'small' ? 42 : 100;
	return $('<div class="object-image" style="background-image: url(' + url + '); height:' + height + 'px">');
};

ObjectImageController.getInstance = function () {
	return objectImageController;
};

var objectImageController = new ObjectImageController();