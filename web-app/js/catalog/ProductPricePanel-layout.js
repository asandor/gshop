// This file was automatically generated from ProductPricePanel-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.catalog == 'undefined') { gshop.catalog = {}; }


gshop.catalog.productPricePanel = function(opt_data, opt_ignored, opt_ijData) {
  var output = '<div class="product-price amount-' + soy.$$escapeHtml(opt_data.productPrice.length) + '">';
  if (opt_data.productPrice.length == 1) {
    output += '<div class="column-1 last"><div class="price-title" >' + soy.$$escapeHtml(opt_ijData.loc['product.price.pricetitle']) + '</div><div class="price-value">' + soy.$$escapeHtml(opt_data.productPrice[0].price) + ' &#8364;</div></div>';
  } else {
    var priceList14 = opt_data.productPrice;
    var priceListLen14 = priceList14.length;
    for (var priceIndex14 = 0; priceIndex14 < priceListLen14; priceIndex14++) {
      var priceData14 = priceList14[priceIndex14];
      output += '<div class="column-' + soy.$$escapeHtml(priceIndex14 + 1) + '"><div class="price-title" >' + soy.$$escapeHtml(priceData14.from) + ((! (priceIndex14 == priceListLen14 - 1)) ? ' - ' + soy.$$escapeHtml(opt_data.productPrice[priceIndex14 + 1].from - 1) : '>') + '</div><div class="price-value">' + soy.$$escapeHtml(Math.round(priceData14.price * 100) / 100) + ' &#8364;</div></div>';
    }
  }
  output += '<div class="add-to-basket"><input type="text" size="10" value="1" name="amount"/> ks<button type="button">' + soy.$$escapeHtml(opt_ijData.loc['basket.add']) + '</button><div></div>';
  return output;
};
