"use strict";

function CatalogController() {

}

CatalogController.prototype.listProducts = function (categoryid, pageNumber, pageSize, successCallback) {
	$.post(UrlMappings.getUrl('CatalogController.listProducts'), { catId: categoryid, pageNumber: pageNumber, pageSize: pageSize })
			.error(function (e) {
				throw e;
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				successCallback(resp.products);
			});
};

CatalogController.getInstance = function () {
	return catalogController;
};

var catalogController = new CatalogController();