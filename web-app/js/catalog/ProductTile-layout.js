// This file was automatically generated from ProductTile-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.catalog == 'undefined') { gshop.catalog = {}; }


gshop.catalog.productTile = function(opt_data, opt_ignored) {
  return '<div class="product-tile"><div class="mouse-event-catcher"></div><div class="product-description" style="display: none;">' + soy.$$escapeHtml(opt_data.product.longDescription) + '<div class="close-description"></div></div><div class="img-border"><div class="image"></div></div><div class="title"><h3><a href="#">' + soy.$$escapeHtml(opt_data.product.title) + '</a></h3></div></div>';
};
