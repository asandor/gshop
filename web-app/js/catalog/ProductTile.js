"use strict";

function ProductTile(product, element, basket) {
	this.product = product;
	this.rootElement = $(element);
	this.basket = basket;
}

ProductTile.prototype.display = function () {
	this.rootElement.append(gshop.catalog.productTile({
		product: this.product
	}));

	var product = this.product;
	var basket = this.basket;
	this.rootElement.find('.image').append(ObjectImageController.getInstance().createProductImageElement(this.product, 'medium'));

	var priceParams = {
		product: this.product,
		mainElement: this.rootElement.find('.product-tile'),
		hoverElement: this.rootElement,
		basket: this.basket
	};

	var pricePanel = new ProductPriceDisplay(priceParams);
    var me = this;

    this.rootElement.find('.close-description').click(function(){
        me.rootElement.find('.product-description').hide('slow');

        $('#page-body').css("height", '');
    });

    function showDescription (){
        var description = me.rootElement.find('.product-description');
        if($('#page-body').height()< description.height()){
            $('#page-body').height(description.height()+80);
        }
        description.show();
    }
    this.rootElement.find('.img-border').click(function(){
        showDescription();
    });
    this.rootElement.find('h3').click(function(){
        showDescription();
    });

};

ProductTile.prototype.onHover = function () {
	//Vertical Sliding
	this.rootElement.hover(function () {
		$(".add-to-basket", this).stop().animate({bottom: '0'}, {queue: false, duration: 500});
	}, function () {
		$(".add-to-basket", this).stop().animate({bottom: '-53px'}, {queue: false, duration: 500});
	});
};