"use strict";

function ProductTileList(element, categoryId, basket) {
	this.rootElement = element;
	this.currentPage = 0;
	this.productsInRow = 4;
	this.pageSize = this.productsInRow * 6; //6 rows on 1 page
	this.categoryId = categoryId;
	this.productTileHeight = 200;
	this.displayedProducts = [];
	this.productTiles = [];
	this.noMorePages = false;
	this.runningRequest = false;
	this.basket = basket;
	$(window).scroll({me: this}, this.handleScrollEvent);
}

ProductTileList.prototype.fetchNextPage = function () {
	if (!this.noMorePages && !this.runningRequest) {
		console.log("ProductTileList: fetching page " + this.currentPage);
		this.runningRequest = true;
		var me = this;
		this.currentPage += 1;
		me.syncHeightWithDisplayedPages();
		CatalogController.getInstance().listProducts(this.categoryId, this.currentPage - 1, this.pageSize, function (products) {
			console.log("ProductTileList: received " + products.length + " products for page " + me.currentPage);
			if (products.length > 0) {
				me.appendProducts(products);
			}

			if (products.length < me.pageSize) {
				me.noMorePages = true;
				me.syncHeightWithDisplayedProducts();
			}

			me.runningRequest = false;
		});
	}
};

ProductTileList.prototype.appendProducts = function (products) {
	for (var i = 0; i < products.length; i++) {
		var tile = new ProductTile(products[i], $('<div class="single-product"></div>'), this.basket);
		this.rootElement.append(tile.rootElement);
		tile.display();

		this.displayedProducts.push(products[i]);
		this.productTiles.push(products[i]);
	}
};

ProductTileList.prototype.syncHeightWithDisplayedProducts = function () {
	var height = (this.displayedProducts.length / this.productsInRow) * this.productTileHeight;
	console.log("ProductTileList: setting list height to " + height + "px");
	this.rootElement.height(height);
};

ProductTileList.prototype.syncHeightWithDisplayedPages = function () {
	var height = this.currentPage * (this.pageSize / this.productsInRow) * this.productTileHeight;
	console.log("ProductTileList: setting list height to " + height + "px");
	this.rootElement.height(height);
};

ProductTileList.prototype.handleScrollEvent = function (evtData, e) {
	var me = evtData.data.me;
	if ($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
		me.fetchNextPage();
	}
};