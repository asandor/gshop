/*
 mainElement - element where the price panel should be appended
 productPrice - list of products' prices
 hoverElement - on mouse over this element basket panel will roll up
 */
function ProductPriceDisplay(params) {
	this.mainElement = params.mainElement;
	this.product = params.product;

	this.mainElement.append(gshop.catalog.productPricePanel({productPrice: this.product.prices}, null, {loc: document.locstr}));
	this.hoverElement = params.hoverElement || this.mainElement.find('.product-price');
	this.basket = params.basket;

	var me = this;
	this.mainElement.find('.add-to-basket button').on('click', function () {
		var amount = me.mainElement.find('input[name="amount"]').val();
		me.basket.addProduct(me.product.code, amount);
	});
	this.onHover();
}

ProductPriceDisplay.prototype.onHover = function () {
	this.hoverElement.hover(function () {
		$(".add-to-basket", this).stop().animate({bottom: '0px'}, {queue: false, duration: 150});
	}, function () {
		$(".add-to-basket", this).stop().animate({bottom: '-53px'}, {queue: false, duration: 1550});
	});
}
