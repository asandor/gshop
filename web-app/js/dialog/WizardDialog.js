var WizardDialog = function (mainElement, attrs) {
	this.displayedPanel = null;

	this.panelOffset = 0;
	this.setWidth(700);
	this.setHeight(500);

	this.successfullOrder = false;

	this.mainElement = mainElement;

	if (attrs) {
		this.title = attrs.title;
		this.modal = attrs.modal;
	} else {
		this.title = "";
	}
};

WizardDialog.SwapDirection = {
	FORWARD: "Forward",
	BACK: "Back"
};

WizardDialog.prototype.open = function () {
	console.log("Opening WizardDialog");

	var dialog = this;
	this.dialog = $(this.mainElement).dialog({
		autoOpen: false,
		modal: this.modal,
		resizable: false,
		draggable: true,
		title: this.title,
		width: this.width,
		height: this.height,
		open: function () {
			dialog.dialogOpened();
		},
		close: function () {
			dialog.dialogClosed();
		}
	});
	this.mainElement.dialog('open');
};

WizardDialog.prototype.close = function () {
	this.mainElement.dialog('close');
};

WizardDialog.prototype.dialogOpened = function () {
	if (this.displayedPanel === null) {
		console.log("Displaying first panel in empty WizardDialog");
		this.mainElement.empty();

		this.panelElement = this.createPanelElement();
		this.mainElement.append(this.panelElement);

		this.controlsPanel = $('<div class="dialog-controls">');
		this.initializeControlsPanel(this.controlsPanel);
		this.mainElement.append(this.controlsPanel);

		if (this.getFirstPanel() !== null) {
			this.displayPanel(this.getFirstPanel());
		}
	}
};

WizardDialog.prototype.initializeControlsPanel = function (controlsPanel) {
	var dialog = this;
	this.nextButton = $('<button>').button({label: document.locstr['dialog.controls.next']}).click(function () {
		dialog.swapPanels(WizardDialog.SwapDirection.FORWARD);
	});
	this.prevButton = $('<button>').button({label: document.locstr['dialog.controls.prev']}).click(function () {
		dialog.swapPanels(WizardDialog.SwapDirection.BACK);
	});
	this.closeButton = $('<button>').button({label: document.locstr['dialog.controls.close']}).click(function () {
		dialog.closeAndReset();
	});
	this.closeButton.hide();
	controlsPanel.append(this.prevButton);
	controlsPanel.append(this.nextButton);
	controlsPanel.append(this.closeButton);
	controlsPanel.height(40);
};

WizardDialog.prototype.getFirstPanel = function () {
	return null;
};

WizardDialog.prototype.dialogClosed = function () {
};

WizardDialog.prototype.swapPanels = function (direction) {
	var panelToBeDisplayed = this.getPanelToBeDisplayed(direction);

	if (panelToBeDisplayed !== null) {
		console.log("WizardDialog swapping panels in direction '" + direction + "' - next panel is: " + panelToBeDisplayed.getPanelType());

		this.disableSwapBackward();
		this.disableSwapForward();

		var animationSettings = {};
		if (direction === WizardDialog.SwapDirection.FORWARD) {
			animationSettings.left = '-=' + (this.panelWidth + this.panelOffset) + 'px';
		} else {
			animationSettings.left = '+=' + (this.panelWidth + this.panelOffset) + 'px';
		}

		var me = this;
		this.panelElement.children().first().animate(animationSettings, 450, function () {
			$(this).detach();
			me.enableSwapBackward();
			me.enableSwapForward();

			me.displayPanel(panelToBeDisplayed);
		});

	} else {
		console.log("WizardDialog cannot swap panels in direction '" + direction + "' - panel to be displayed is NULL");
	}
};

WizardDialog.prototype.getPanelToBeDisplayed = function (direction) {
	return null;
};

WizardDialog.prototype.displayPanel = function (panel) {
	console.log("WizardDialog displaying " + panel.getPanelType());

	if (!panel.isAddedToDialog()) {
		panel.added = true;
		panel.onAddToDialog();
	}

	var panelAnimationContainer = $('<div class="dialog-panels">');
	panelAnimationContainer.append(panel.getMainElement());
	this.panelElement.append(panelAnimationContainer);

	if (this.displayedPanel !== null) {
		this.displayedPanel.displayed = false;
		if (this.specialNextButton) this.specialNextButton.detach();
	}

	this.displayedPanel = panel;
	this.displayedPanel.displayed = true;
	if (this.displayedPanel === this.getFirstPanel()) {
		this.prevButton.hide();
	} else {
		this.prevButton.show();
		this.enableSwapForward();
		this.enableSwapBackward();
	}
	this.displayedPanel.onDisplay();

	this.specialNextButton = this.displayedPanel.getSpecialNextButton();
	if (this.specialNextButton) {
		this.nextButton.hide();
		this.controlsPanel.append(this.specialNextButton)
	} else {
		this.nextButton.show();
	}

	this.panelDisplayed(panel);
};

WizardDialog.prototype.createPanelElement = function () {
	var panelElement = $('<div class="dialog-panel-container"></div>');
	panelElement.height(this.panelHeight);

	return panelElement
};

WizardDialog.prototype.closeAndReset = function () {
	this.close();
	this.displayedPanel = null;
};

WizardDialog.prototype.isPanelAdded = function (panel) {
	return panel.isAddedToDialog();
};

WizardDialog.prototype.enableSwapForward = function () {
	if (this.nextButton) {
		this.nextButton.button('enable');
	}
};

WizardDialog.prototype.enableSwapBackward = function () {
	if (this.prevButton) {
		this.prevButton.button('enable');
	}
};

WizardDialog.prototype.disableSwapForward = function () {
	if (this.nextButton) {
		this.nextButton.button('disable');
	}
};

WizardDialog.prototype.disableSwapBackward = function () {
	if (this.prevButton) {
		this.prevButton.button('disable');
	}
};

WizardDialog.prototype.getPanelWidth = function () {
	return this.panelWidth;
};

WizardDialog.prototype.getPanelHeight = function () {
	return this.panelHeight;
};

WizardDialog.prototype.showCloseButton = function () {
	if (this.nextButton) this.nextButton.hide();
	if (this.prevButton) this.prevButton.hide();
	if (this.closeButton) this.closeButton.show();
};

WizardDialog.prototype.setWidth = function (width) {
	this.width = width;
	this.panelWidth = width - this.panelOffset - 10;
};

WizardDialog.prototype.setHeight = function (height) {
	this.height = height;
	this.panelHeight = height - 100;
};

WizardDialog.prototype.getNextButton = function () {
	return this.nextButton;
};

WizardDialog.prototype.getPrevButton = function () {
	return this.prevButton;
};

WizardDialog.prototype.getControlsPanel = function () {
	return this.controlsPanel;
};

WizardDialog.prototype.panelDisplayed = function (panel) {
};

WizardDialog.prototype.isOpen = function () {
	return this.mainElement.dialog('isOpen');
};