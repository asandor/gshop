'use strict';

var WizardPanel = function (wizardDialog) {
	this.dialog = wizardDialog;
	this.added = false;
	this.displayed = false;
};

WizardPanel.prototype.getPanelType = function () {
	return "WizardPanel";
};

WizardPanel.prototype.onAddToDialog = function () {
	this.mainElement = $('<div class="wizard-dialog-panel">');
	this.mainElement.width(this.getDialog().getPanelWidth());
};

WizardPanel.prototype.onRemoveFromDialog = function () {
};

WizardPanel.prototype.onDisplay = function () {
};

WizardPanel.prototype.getBasket = function () {
	return this.basket;
};

WizardPanel.prototype.getDialog = function () {
	return this.dialog;
};

WizardPanel.prototype.getMainElement = function () {
	return this.mainElement;
};

WizardPanel.prototype.isAddedToDialog = function () {
	return this.added;
};

WizardPanel.prototype.isDisplayed = function () {
	return this.displayed;
};

WizardPanel.prototype.isValid = function () {
	return true;
};

/**
 * Allows returning a next button configured by the panel. This button will replace the next button
 * on the wizard dialog until the current panel is displayed.
 */
WizardPanel.prototype.getSpecialNextButton = function () {
	return null;
};