"use strict";

function StaticPanel(dialog) {
	WizardPanel.call(this, dialog);
}

inherit(StaticPanel, WizardPanel);

StaticPanel.prototype.onAddToDialog = function () {
	StaticPanel._superClass.onAddToDialog.call(this);
	this.getMainElement().addClass('static-panel');
};

StaticPanel.prototype.getPanelType = function () {
	return "StaticPanel";
};

StaticPanel.prototype.setContent = function (content) {
	this.content = content;
	if (this.isAddedToDialog()) {
		this.renderContent();
	}
};

StaticPanel.prototype.renderContent = function () {
	this.getMainElement().empty();
	this.getMainElement().append(this.content);
};