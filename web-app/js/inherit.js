"use strict";

var inherit = function (child, parent) {
	var F = function () {
	};
	F.prototype = parent.prototype;
	child.prototype = new F();
	child._superClass = parent.prototype;
	child.prototype.constructor = child;
};