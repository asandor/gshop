"use strict";

function ViewEditForm(element, startMode, title) {
	this.rootElement = element;
	this.contentInitialized = false;
	this.title = title;
	this.model = {};
	this.listeners = [];

	this.animationSettings = {
		speed: 'fast',
		effect: 'fade'
	};

	//code and display property names used for select options
	this.optionCode = 'code';
	this.optionDisplay = 'display';

	this.messageSettings = {
		infoMessageDelay: 2000,
		errorMessageDelay: 4000
	};

	this.initializeContent();

	if (startMode === ViewEditForm.Mode.LOADING) {
		this.loadingMode();
	} else if (startMode === ViewEditForm.Mode.EDIT) {
		this.editMode();
	} else {
		this.viewMode();
	}
}

ViewEditForm.Mode = {
	VIEW: "view",
	EDIT: "edit",
	LOADING: "loading"
};

ViewEditForm.prototype.initializeFields = function () {
	var fields = {};
	var me = this;
	this.formElement.find('input, textarea, select').each(function () {
		var fieldName = $(this).attr('name');
		var viewField = $('<span>');
		fields[fieldName] = {
			rootElement: $(this).parent(),
			editField: $(this),
			viewField: viewField,
			name: fieldName
		};
		fields[fieldName].rootElement.append(viewField);
		fields[fieldName].editField.keydown(function (e) {
			if (e.which == 13) {
				me.submit();
			}
		});
	});
	fields.each = function (eachFunction) {
		for (var field in fields) {
			if (fields[field].hasOwnProperty('rootElement')) {
				eachFunction(fields[field]);
			}
		}
	};

	this.fields = fields;
};

ViewEditForm.prototype.initializeControls = function () {
	var me = this;

	var findElement = function (selector) {
		var element = me.rootElement.find(selector);
		if (element.length == 1) {
			return element.first();
		} else if (element.length > 1) {
			throw "More then 1 element '" + selector + "' found";
		} else {
			return null;
		}
	};

	var submitButtonElement = findElement('.submit');
	if (submitButtonElement) {
		this.submitButton = submitButtonElement.button();
		this.submitButton.click(function () {
			me.submit();
		});
	}

	var editButtonElement = findElement('.edit');
	if (editButtonElement) {
		this.editModeButton = editButtonElement.button();
		this.editModeButton.click(function () {
			me.editMode();
		});
	}

	var cancelButtonElement = findElement('.cancel');
	if (cancelButtonElement) {
		this.cancelButton = cancelButtonElement.button();
		this.cancelButton.click(function () {
			me.viewMode();
		});
	}

	var messageFieldElement = findElement('.message');
	if (messageFieldElement) {
		this.messageField = messageFieldElement;
	}
};

ViewEditForm.prototype.viewMode = function () {
	this.mode = ViewEditForm.Mode.VIEW;
	if (!this.contentInitialized) {
		this.initializeContent();
	}
	var me = this;
	this.fields.each(function (field) {
		field.editField.hide();
		if (me.model.hasOwnProperty(field.name)) {
			me.setViewFieldValue(field, me.model[field.name]);
		}
		field.viewField.show(me.animationSettings.effect, null, me.animationSettings.speed, null);
	});

	//remove validation error labels
	this.rootElement.find("label.error").remove();

	if (this.editModeButton) this.editModeButton.show();
	if (this.cancelButton) this.cancelButton.hide();
	if (this.submitButton) this.submitButton.hide();
	if (this.messageField) this.messageField.hide();

	this.fireModeChangeEvent();
};

ViewEditForm.prototype.editMode = function () {
	this.mode = ViewEditForm.Mode.EDIT;
	if (!this.contentInitialized) {
		this.initializeContent();
	}
	var me = this;
	this.fields.each(function (field) {
		field.viewField.hide();
		if (me.model.hasOwnProperty(field.name)) {
			me.setEditFieldValue(field, me.model[field.name]);
		}
		field.editField.show(me.animationSettings.effect, null, me.animationSettings.speed, null);
	});

	if (this.editModeButton) this.editModeButton.hide();
	if (this.cancelButton) this.cancelButton.show();
	if (this.submitButton) this.submitButton.show();
	if (this.messageField) this.messageField.hide();

	this.fireModeChangeEvent();
};

ViewEditForm.prototype.loadingMode = function () {
	this.mode = ViewEditForm.Mode.LOADING;
	this.rootElement.empty();
	this.rootElement.text("Loading..."); //TODO: progress bar
	this.contentInitialized = false;

	this.fireModeChangeEvent();
};

ViewEditForm.prototype.initializeContent = function () {
	var layoutTemplateFunction = this.layoutTemplate();
	//can be null if form structure is pre-prepared
	if (layoutTemplateFunction) {
		this.rootElement.empty();
		this.rootElement.append(this.layoutTemplate());
	}

	this.formElement = this.rootElement.find('form');

	this.initializeFields();

	this.initializeControls();

	if (this.title) {
		var tit = this.rootElement.find('.title').text(this.title);
	}

	var validatorSettings = this.getValidatorSettings();
	this.formElement.validate(validatorSettings);

	this.contentInitialized = true;
};

ViewEditForm.prototype.submit = function () {
	if (this.validate()) {
		var data = this.getData();
		this.submitHandler(data);
		this.fireAfterSubmitEvent();
	}
};

ViewEditForm.prototype.validate = function () {
	return this.formElement.valid();
};

ViewEditForm.prototype.setModel = function (model) {
	if (model) {
		this.model = model;
	} else {
		this.model = {};
	}
	this.modelUpdated();
};

ViewEditForm.prototype.syncModelToEditState = function () {
	var me = this;
	this.fields.each(function (field) {
		if (field.editField.is('select')) {
			var selectedOption = field.editField.find('option:selected');
			me.model[field.name] = {
				code: selectedOption.val(),
				display: selectedOption.text()
			}
		} else {
			me.model[field.name] = field.editField.val();
		}
	});
};

ViewEditForm.prototype.modelUpdated = function () {
	var me = this;
	this.fields.each(function (field) {
		me.setEditFieldValue(field, me.model[field.name]);
		me.setViewFieldValue(field, me.model[field.name]);
	});
};

ViewEditForm.prototype.getModel = function () {
	return this.model;
};

ViewEditForm.prototype.disable = function () {
	if (this.editModeButton) this.editModeButton.button('disable');
	if (this.cancelButton) this.cancelButton.button('disable');
	if (this.submitButton) this.submitButton.button('disable');
	this.fields.each(function (field) {
		field.editField.attr("disabled", "disabled");
	});
};

ViewEditForm.prototype.enable = function () {
	if (this.editModeButton) this.editModeButton.button('enable');
	if (this.cancelButton) this.cancelButton.button('enable');
	if (this.submitButton) this.submitButton.button('enable');
	this.fields.each(function (field) {
		field.editField.removeAttr("disabled");
	});
};

ViewEditForm.prototype.getValidatorSettings = function () {
	return {};
};

ViewEditForm.prototype.submitHandler = function (data) {

};

ViewEditForm.prototype.layoutTemplate = function () {

};

ViewEditForm.prototype.displayErrorMessage = function (message, delay) {
	if (this.messageField) {
		this.messageField.text(message);
		this.messageField.removeClass('info-message');
		this.messageField.addClass('error-message');
		this.messageField.show(this.animationSettings.speed);
		if (!delay) delay = this.messageSettings.errorMessageDelay;
		if (delay) {
			this.messageField.delay(delay).hide(this.animationSettings.speed);
		}
	}
};

ViewEditForm.prototype.displayInfoMessage = function (message, delay) {
	if (this.messageField) {
		this.messageField.text(message);
		this.messageField.addClass('info-message');
		this.messageField.removeClass('error-message');
		this.messageField.show(this.animationSettings.speed);
		if (!delay) delay = this.messageSettings.infoMessageDelay;
		if (delay) {
			this.messageField.delay(delay).hide(this.animationSettings.speed);
		}
	}
};

ViewEditForm.prototype.setViewFieldValue = function (field, value) {
	if (value) {
		field.viewField.text(value[this.optionDisplay]);
		if (field.editField.is('select')) {
		} else {
			field.viewField.text(value);
		}
	} else {
		field.viewField.text("");
	}
};

ViewEditForm.prototype.setEditFieldValue = function (field, value) {
	if (field.editField.is('select')) {
		if (value) {
			field.editField.val(value[this.optionCode]);
		} else {
			field.editField.val("");
		}
	} else {
		field.editField.val(value);
	}
};

ViewEditForm.prototype.fireModeChangeEvent = function () {
	for (var i = 0; i < this.listeners.length; i++) {
		if ('formModeChanged' in this.listeners[i]) {
			this.listeners[i].formModeChanged(this, this.mode)
		}
	}
};

ViewEditForm.prototype.fireAfterSubmitEvent = function () {
	for (var i = 0; i < this.listeners.length; i++) {
		if ('afterSubmit' in this.listeners[i]) {
			this.listeners[i].afterSubmit(this)
		}
	}
};

ViewEditForm.prototype.show = function (animate) {
	if (animate == true || animate == null) {
		this.rootElement.show(this.animationSettings.speed);
	} else {
		this.rootElement.show();
	}
};

ViewEditForm.prototype.hide = function (animate) {
	if (animate == true || animate == null) {
		this.rootElement.hide(this.animationSettings.speed);
	} else {
		this.rootElement.hide();
	}
};

ViewEditForm.prototype.remove = function () {
	this.rootElement.remove();
	this.listeners = {};
	this.model = {};
};

ViewEditForm.prototype.getData = function () {
	var data = {};
	var me = this;
	this.fields.each(function (field) {
		data[field.name] = field.editField.val();
	});
	return data;
};

ViewEditForm.prototype.registerFormListener = function (listener) {
	this.listeners.push(listener);
};