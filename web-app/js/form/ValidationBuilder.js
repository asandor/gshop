"use strict";

function ValidationBuilder() {
	this.defaultMessages = {
		maxlength: document.locstr['validation.maxlength'],
		required: document.locstr['registration.validation.emptyField'],
		email: document.locstr['registration.validation.email']
	};

	this.rules = {};
	this.messages = {};
}

ValidationBuilder.prototype.maxlength = function (fieldName, maxlength) {
	this.checkAndCreateNewField(fieldName);
	this.rules[fieldName].maxlength = maxlength;
	this.messages[fieldName].maxlength = this.defaultMessages.maxlength;
	return this;
};

ValidationBuilder.prototype.required = function (fieldName, required) {
	this.checkAndCreateNewField(fieldName);
	this.rules[fieldName].required = required;
	this.messages[fieldName].required = this.defaultMessages.required;
	return this;
};

ValidationBuilder.prototype.requiredMaxLength = function (fieldName, required, maxlength) {
	this.required(fieldName, required);
	this.maxlength(fieldName, maxlength);
	return this;
};

ValidationBuilder.prototype.custom = function (fieldName, rule, message) {
	this.rules[fieldName] = rule;
	this.messages[fieldName] = message;
	return this;
};

ValidationBuilder.prototype.email = function (fieldName) {
	this.required(fieldName, true);
	this.rules[fieldName].email = true;
	this.messages[fieldName].email = this.defaultMessages.email;
	return this;
};

ValidationBuilder.prototype.build = function () {
	return {
		debug: true,
		onsubmit: false,
		onkeyup: false,
		rules: this.rules,
		messages: this.messages
	};
};

ValidationBuilder.prototype.checkAndCreateNewField = function (fieldName) {
	if (!this.rules.hasOwnProperty(fieldName)) {
		this.rules[fieldName] = {};
		this.messages[fieldName] = {};
	}
};