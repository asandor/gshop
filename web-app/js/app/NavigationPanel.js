function NavigationPanel(element, user) {
	this.rootElement = $(element);
	var me = this;
	user.addUserListener(function (user) {
		me.userUpdated(user);
	});
	this.userUpdated(user);
}

NavigationPanel.prototype.userUpdated = function (user) {
	this.rootElement.empty();

	UrlMappings.appendLink(this.rootElement, "root", document.locstr['home.link']);

	if (user.isAuthenticated()) {
		if (user.role == User.Role.BUSINESS_CUSTOMER || user.role == User.Role.PRIVATE_CUSTOMER) {
			UrlMappings.appendLink(this.rootElement, "UserController.myAccount", document.locstr['mydata.link']);
			UrlMappings.appendLink(this.rootElement, "UserController.myOrders", document.locstr['myorders.link']);
		}
		if (user.role == User.Role.ADMIN_USER) {
			UrlMappings.appendLink(this.rootElement, "AdminController.index", document.locstr['admin.index']);
		}
	}
};


//<g:link uri="/"><g:message code="home.link"/></g:link>
//<g:link controller="user" action="myAccount"><g:message code="mydata.link"/></g:link>
//<g:link controller="user" action="myOrders"><g:message code="myorders.link"/></g:link>
