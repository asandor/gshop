function AuthenticationPanel(element, user) {
	this.rootElement = element;
	var me = this;

	user.addUserListener(this);

	this.signInPanel = new AuthenticationSignInPanel(this.rootElement.find("#signin-panel"), user);

	this.signOutPanel = $('#signout-panel');
	this.signOutPanel.find("#signout-button").button().click(function () {
		$("#signin-form").css({top: '-90px'});
		user.signOut();
	});

	this.userUpdated(user, null, null);
}

AuthenticationPanel.prototype.userUpdated = function (user, eventType, resp) {
	console.log("Updating AuthenticationPanel after User update (auth=" + user.isAuthenticated() + ")")
	if (user.isAuthenticated()) {
		this.signInPanel.hide();
		this.signOutPanel.show();
	} else {
		this.signOutPanel.hide();
		this.signInPanel.show();
	}
};

AuthenticationPanel.prototype.isRegistrationDialogOpen = function () {
	if (this.registrationDialog) {
		return this.registrationDialog.isOpen();
	}
	return false;
};

function AuthenticationSignInPanel(element, user) {
	this.rootElement = element;
	var me = this;

	this.signInForm = new SignInForm(this.rootElement.find('#signin-form'), user, false);
	this.signInForm.failureHandler = function () {
		$.gritter.add({
			title: document.locstr['error'],
			text: document.locstr['login.incorrect'],
			class_name: 'error'
		});
	};
	this.rootElement.find('#signin-form-hide-link').click(function () {
		me.signInForm.rootElement.animate({top: '-88px'}, {queue: false, duration: 500});
		me.signInInfoPanel.show();
	});

	this.signInInfoPanel = new SignInInfoPanel(this.rootElement.find("#signin-info-panel"));
	this.signInInfoPanel.loginButton.click(function () {
		me.signInInfoPanel.hide();
		me.signInForm.show(false);
		me.signInForm.rootElement.animate({top: '0px'}, {queue: false, duration: 500});
	});

	this.signInInfoPanel.registrationButton.click(function () {
		me.registrationDialog = new RegistrationDialog(user);
		me.registrationDialog.open();
	});

}

AuthenticationSignInPanel.prototype.show = function () {
	console.log("Showing AuthenticationSignInPanel in initial state");
	this.rootElement.show();
	this.signInForm.hide(false);
	this.signInInfoPanel.show();
};

AuthenticationSignInPanel.prototype.hide = function () {
	console.log("Hiding AuthenticationSignInPanel");
	this.rootElement.hide();
};

function SignInInfoPanel(element) {
	this.rootElement = element;
	var me = this;

	this.registrationButton = element.find('#registration-link');

	this.loginButton = element.find('#login-link');
}

SignInInfoPanel.prototype.show = function () {
	console.log("Showing SignInInfoPanel in initial state");
	this.rootElement.show();
};

SignInInfoPanel.prototype.hide = function () {
	console.log("Hiding SignInInfoPanel");
	this.rootElement.hide();
};
