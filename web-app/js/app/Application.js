$(document).ready(function () {
	new Application().startup();
});

function Application() {
	CommonModuleManager.call(this, []);
}

inherit(Application, CommonModuleManager);

Application.prototype.onModuleInitializationComplete = function () {
	this.basket = this.getModule('basket');

	this.user = this.getModule('user');
	this.user.addUserListener(this);

	this.authenticationPanel = new AuthenticationPanel($('#authentication-panel'), this.user);

	this.navigationPanel = new NavigationPanel($('#my-navigation'), this.user);

	this.basketDialog = null;

	var me = this;
	$('#basket-button').click(function () {
		me.getBasketDialog().open();
	});

	if ($('#products-tile-list')) {
		var tileList = new ProductTileList($('#products-tile-list'), $('#categoryId').val(), this.basket);
		tileList.fetchNextPage();
	}

};

Application.prototype.userUpdated = function (user, eventType, resp) {
	if (eventType === User.EventType.SIGN_IN || eventType === User.EventType.SIGN_OUT) {
		if (!this.isBasketDialogOpen()) {
			this.resetBasketDialog();
		}
	}
	if (eventType === User.EventType.SIGN_IN && user.role != User.Role.ADMIN_USER) {
		this.basket.initialize(resp.basket);
		if (resp.merged === true && !this.isBasketDialogOpen()) {
			this.showBasketMergeWarning();
		}
		if (user.activated === false && !this.authenticationPanel.isRegistrationDialogOpen()) {
			this.showUserNotActivatedWarning();
		}
	}
	if (eventType === User.EventType.SIGN_OUT) {
		this.basket.initialize({});
	}
	if (eventType === User.EventType.ACTIVATION) {
		this.showActivationSuccessNotification();
	}
};

Application.prototype.isBasketDialogOpen = function () {
	if (!this.basketDialog) return false;
	return this.basketDialog.isOpen();
};

Application.prototype.showBasketMergeWarning = function () {
	$.gritter.add({
		title: document.locstr['warning'],
		text: document.locstr['basket.merge-message'],
		class_name: 'warning'
	});
};

Application.prototype.showUserNotActivatedWarning = function () {
	$.gritter.add({
		title: document.locstr['warning'],
		text: document.locstr['activation.message.warning'],
		class_name: 'warning'
	});
};

Application.prototype.showActivationSuccessNotification = function () {
	$.gritter.add({
		title: document.locstr['congratulations'],
		text: document.locstr['activation.message.success'],
		class_name: 'congratulations'
	});
};

Application.prototype.getBasketDialog = function () {
	if (this.basketDialog === null) {
		this.basketDialog = new BasketDialog(this.basket, this.user);
	}
	return this.basketDialog;
};

Application.prototype.resetBasketDialog = function () {
	this.basketDialog = null;
};

Application.prototype.getBasket = function () {
	return this.basket;
};

Application.prototype.getUser = function () {
	return this.user;
};

var applicationInstance = new Application();

Application.getInstance = function () {
	return applicationInstance;
};