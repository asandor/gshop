"use strict";

function UserAddressForm(element, addressType, title) {
    this.addressType = addressType;
    ViewEditForm.call(this, element, null, title);
}

inherit(UserAddressForm, ViewEditForm);

UserAddressForm.prototype.submitHandler = function (data) {
    this.disable();

    var userFragment = {};
    if (this.addressType === 'billing') {
        userFragment.billingAddress = data;
    } else if (this.addressType === 'delivery') {
        userFragment.deliveryAddress = data;
    } else throw "Unknown address type '" + this.addressType + "'";

    var me = this;
    UserController.getInstance().updateCustomer(userFragment, function (resp) {
        me.updateSuccessful(resp);
        me.enable();
    }, function (resp) {
        me.updateFailed(resp);
        me.enable();
    });
};

UserAddressForm.prototype.updateSuccessful = function (resp) {
    this.syncModelToEditState();
    this.viewMode();
};

UserAddressForm.prototype.updateFailed = function (resp) {
    this.displayErrorMessage(document.locstr['error.serverUpdateError']);
};

UserAddressForm.prototype.layoutTemplate = function () {
    return gshop.user.userAddressForm(
        {role:this.role},
        null,
        {loc:document.locstr, countries:countries});
};

UserAddressForm.prototype.getValidatorSettings = function () {
    var builder = new ValidationBuilder();
    builder.requiredMaxLength('street', true, 50)
        .requiredMaxLength('number', true, 10)
        .requiredMaxLength('postalCode', true, 10)
        .requiredMaxLength('city', true, 50)
        .required('country', true)
    return builder.build();
};