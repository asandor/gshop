// This file was automatically generated from UserAddressForm-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.user == 'undefined') { gshop.user = {}; }


gshop.user.userAddressForm = function(opt_data, opt_ignored, opt_ijData) {
  var output = '<div class="user-address-form"><div class="header"><h2 class="title"></h2><button class="edit">' + soy.$$escapeHtml(opt_ijData.loc['edit']) + '</button><button class="cancel">' + soy.$$escapeHtml(opt_ijData.loc['cancel']) + '</button><button class="submit">' + soy.$$escapeHtml(opt_ijData.loc['save']) + '</button></div><div class="message"></div><form><table><tbody class="user-address"><tr><td class="form-field-label">' + soy.$$escapeHtml(opt_ijData.loc['userdata.address.street']) + '</td><td class="value"><input type="text" class="form-field-textinput" name="street"/></td></tr><tr><td class="form-field-label">' + soy.$$escapeHtml(opt_ijData.loc['userdata.address.number']) + '</td><td class="value"><input type="text" class="form-field-textinput" name="number"/> </td></tr><tr><td class="form-field-label">' + soy.$$escapeHtml(opt_ijData.loc['userdata.address.postalCode']) + '</td><td class="value"><input type="text" class="form-field-textinput" name="postalCode"/></td></tr><tr><td class="form-field-label">' + soy.$$escapeHtml(opt_ijData.loc['userdata.address.city']) + '</td><td class="value"><input type="text" class="form-field-textinput" name="city"/></td></tr><tr><td class="form-field-label">' + soy.$$escapeHtml(opt_ijData.loc['userdata.address.country']) + '</td><td class="value"><select class="form-field-combo" name="country"><option value=""></option>';
  var countryList20 = opt_ijData.countries;
  var countryListLen20 = countryList20.length;
  for (var countryIndex20 = 0; countryIndex20 < countryListLen20; countryIndex20++) {
    var countryData20 = countryList20[countryIndex20];
    output += '<option value="' + soy.$$escapeHtml(countryData20.code) + '">' + soy.$$escapeHtml(countryData20.display) + '</option>';
  }
  output += '</select></td></tr><tr><td class="form-field-label" id="comment">' + soy.$$escapeHtml(opt_ijData.loc['userdata.address.comment']) + '</td><td class="value" colspan="3"><textarea class="form-field-textinput" name="comment"></textarea></td></tr></tbody></table></form></div>';
  return output;
};
