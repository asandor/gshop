"use strict";

function UserDataForm(element, role) {
    this.role = role;
    ViewEditForm.call(this, element, null, document.locstr['userdata.personaldata.section']);
}

inherit(UserDataForm, ViewEditForm);

UserDataForm.prototype.submitHandler = function (data) {
    var me = this;
    me.disable();
    UserController.getInstance().updateCustomer(data, function (resp) {
        me.updateSuccessful(resp);
        me.enable();
    }, function (resp) {
        me.updateFailed(resp);
        me.enable();
    });
};

UserDataForm.prototype.updateSuccessful = function (resp) {
    this.syncModelToEditState();
    this.viewMode();
    this.model.fireUserUpdatedEvent();
};

UserDataForm.prototype.updateFailed = function (resp) {
    this.displayErrorMessage(document.locstr['error.serverUpdateError']);
};

UserDataForm.prototype.layoutTemplate = function () {
    return gshop.user.userDataForm(
        {role:this.role},
        null,
        {loc:document.locstr});
};

UserDataForm.prototype.getValidatorSettings = function () {
    var builder = new ValidationBuilder();
    builder.requiredMaxLength('phoneNumber', false, 20);
    if (this.role === 'PrivateCustomer') {
        builder.requiredMaxLength('firstName', true, 30)
            .requiredMaxLength('lastName', true, 30);
    }
    if (this.role === 'BusinessCustomer') {
        builder.requiredMaxLength('companyName', true, 50)
            .requiredMaxLength('contactPerson', true, 50)
            .requiredMaxLength('companyId', true, 30)
            .requiredMaxLength('taxId', true, 30)
            .requiredMaxLength('vat', true, 30);
    }

    return builder.build();
};