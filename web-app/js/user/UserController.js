"use strict";

function UserController() {

}

UserController.prototype.updateCustomer = function (userData, successCallback, errorCallback) {
	if (!userData.user) userData = {user: userData};
	var userDataString = JSON.stringify(userData);
	$.ajax({
		url: UrlMappings.getUrl('UserController.updateCustomer'),
		type: "POST",
		data: userDataString,
		contentType: "application/json; charset=utf-8",
		success: function (resp) {
			console.log("UserController: customer update successful");
			if (successCallback) successCallback(resp);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("UserController: server returned error on customer update (" + textStatus + ", " + errorThrown + ")");
			if (errorCallback) errorCallback(textStatus);
		}
	});
};

UserController.prototype.removeDeliveryAddress = function (successCallback, errorCallback) {
	$.ajax({
		url: UrlMappings.getUrl('UserController.removeDeliveryAddress'),
		type: "POST",
		data: "{}",
		contentType: "application/json; charset=utf-8",
		success: function (resp) {
			console.log("UserController: removeDeliveryAddress successful");
			if (successCallback) successCallback(resp);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log("UserController: server returned error on delivery address removal (" + textStatus + ", " + errorThrown + ")");
			if (errorCallback) errorCallback(textStatus);
		}
	});
};

UserController.prototype.customerDetails = function (successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('UserController.customerDetails'))
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				if (successCallback) successCallback(resp);
			});
};

UserController.prototype.updatePassword = function (oldPwd, newPwd, successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('UserController.updatePassword'), {oldPwd: oldPwd, newPwd: newPwd})
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (successCallback) successCallback(resp);
			});
};

UserController.prototype.signIn = function (username, password, rememberMe, successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('UserController.signIn'), {username: username, password: password, rememberMe: rememberMe})
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "SignIn response '" + resp.toString() + "' cannot be parsed";
				}
				if (successCallback) successCallback(resp);
			});
};

UserController.prototype.signOut = function (successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('UserController.signOut'))
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (successCallback) successCallback(resp);
			});
};

UserController.prototype.registerCustomer = function (userData, successCallback, errorCallback) {
	var dataString = JSON.stringify(userData);
	jQuery.ajax({
		url: UrlMappings.getUrl('UserController.registerCustomer'),
		type: "POST",
		data: dataString,
		contentType: "application/json; charset=utf-8",
		success: function (resp) {
			if (successCallback) successCallback(resp);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			try {
				var error = $.parseJSON(jqXHR.responseText);
				if (errorCallback) errorCallback(error.code, error.message);
			} catch (ex) {
				if (errorCallback) errorCallback();
			}
		}
	});
};

UserController.prototype.activateCustomer = function (activationCode, successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('UserController.activateCustomer'), {activationCode: activationCode})
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (successCallback) successCallback();
			});
};

UserController.getInstance = function () {
	return userController;
};

var userController = new UserController();