"use strict";

function ActivationPanel(dialog, user) {
	WizardPanel.call(this, dialog);
	this.user = user;
	this.successHandler = null;
	this.failureHandler = null;
}

inherit(ActivationPanel, WizardPanel);

ActivationPanel.prototype.onAddToDialog = function () {
	ActivationPanel._superClass.onAddToDialog.call(this);

	this.getMainElement().append(gshop.user.activationPanel(null, null, {loc: document.locstr}));
	this.activationCodeField = this.getMainElement().find('.activation-code');
	this.errorMessageField = this.getMainElement().find('.error-message');
};

ActivationPanel.prototype.onDisplay = function () {
	this.getDialog().prevButton.hide();
};

ActivationPanel.prototype.getSpecialNextButton = function () {
	if (!this.myNextButton) {
		var me = this;
		this.myNextButton = $('<button>' + locstr['activation.submit'] + '</button>').button();
		this.myNextButton.click(function () {
			me.submitActivationCode();
		});
	}
	return this.myNextButton;
};

ActivationPanel.prototype.submitActivationCode = function () {
	var me = this;
	this.user.activate(this.activationCodeField.val(), function () {
		if (me.successHandler) me.successHandler();
	}, function () {
		if (me.failureHandler) {
			me.failureHandler();
		} else {
            me.errorMessageField.show();
			me.errorMessageField.text(document.locstr['activation.message.failure']);
			me.errorMessageField.delay(3000).hide('slow');
		}
	});
};

ActivationPanel.prototype.getPanelType = function () {
	return "ActivationPanel";
};