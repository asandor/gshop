// This file was automatically generated from ActivationPanel-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.user == 'undefined') { gshop.user = {}; }


gshop.user.activationPanel = function(opt_data, opt_ignored, opt_ijData) {
  return '\t<div class="message">' + soy.$$escapeHtml(opt_ijData.loc['activation.enterCode']) + '</div><input type="text" class="activation-code"/><div class="error-message" style="display:none;"></div>';
};
