"use strict";

function PasswordForm(element) {
    ViewEditForm.call(this, element, 'edit');
}

inherit(PasswordForm, ViewEditForm);

PasswordForm.prototype.submitHandler = function (data) {
    this.disable();

    var me = this;
    UserController.getInstance().updatePassword(data.oldPwd, data.newPwd, function (resp) {
        me.updateSuccessful(resp);
        me.enable();
    }, function (resp) {
        me.updateFailed(resp);
        me.enable();
    });
};

PasswordForm.prototype.updateSuccessful = function (resp) {
    this.setModel(null);
    this.displayInfoMessage(document.locstr['userdata.passwordUpdateSuccessful']);
};

PasswordForm.prototype.updateFailed = function (resp) {
    this.displayErrorMessage(document.locstr['error.serverUpdateError']);
};

PasswordForm.prototype.layoutTemplate = function () {
    return gshop.user.passwordForm(null, null, {loc:document.locstr});
};

PasswordForm.prototype.getValidatorSettings = function () {
    var builder = new ValidationBuilder();
    builder.custom('oldPwd', {required:true, minlength:5},
        {required:document.locstr['registration.validation.emptyField']})
        .custom('newPwd', {required:true, minlength:5},
        {required:document.locstr['registration.validation.emptyField'], minlength:document.locstr['registration.validation.password_length']})
        .custom('confirmPwd', {required:true, equalTo:'input[name="newPwd"]'},
        {required:document.locstr['registration.validation.emptyField'], equalTo:document.locstr['registration.validation.password_match']});
    return builder.build();
};