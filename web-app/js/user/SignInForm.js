function SignInForm(element, user, useTemplate) {
	this.user = user;
	this.useTemplate = useTemplate;
	ViewEditForm.call(this, element, 'edit');
	this.successHandler = null;
	this.failureHandler = null;
}

inherit(SignInForm, ViewEditForm);

SignInForm.prototype.submitHandler = function (data) {
	var me = this;
	this.user.signIn(data.username, data.password, data.rememberMe, this.successHandler, this.failureHandler);
};

SignInForm.prototype.layoutTemplate = function () {
	if (this.useTemplate) {
		return gshop.user.signInForm(null, null, {loc: document.locstr});
	} else {
		//initializing from html dom
		return null;
	}
};