"use strict";

function RegistrationDialog(user) {
	WizardDialog.call(this, $('<div class="registration-dialog" style="overflow: hidden">'), {title: document.locstr['RegistrationDialog.title']});
	this.user = user;

	this.setWidth(500);
	this.setHeight(380);

	this.registrationPanel = new RegistrationPanel(this, this.user);
	this.activationPanel = new ActivationPanel(this, this.user);
	var me = this;
	this.activationPanel.successHandler = function () {
		me.close();
	};
}

inherit(RegistrationDialog, WizardDialog);

RegistrationDialog.prototype.getFirstPanel = function () {
	return this.registrationPanel;
};

RegistrationDialog.prototype.getPanelToBeDisplayed = function (direction) {
	if (direction === WizardDialog.SwapDirection.FORWARD) {
		if (this.displayedPanel === this.registrationPanel) {
			return this.activationPanel;
		}
	}
	return null;
};