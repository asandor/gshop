function User() {
	Module.call(this, 'user');
	this.listeners = [];
}

inherit(User, Module);

User.EventType = {
	PROPERTY_CHANGE: 0,
	SIGN_IN: 1,
	SIGN_OUT: 2,
	ACTIVATION: 3
};

User.Role = {
	BUSINESS_CUSTOMER: 'BusinessCustomer',
	PRIVATE_CUSTOMER: 'PrivateCustomer',
	ADMIN_USER: 'AdminUser'
}

User.prototype.initialize = function (userData) {
	if (userData) {
		for (var prop in userData) {
			this[prop] = userData[prop];
		}
	}
};

User.prototype.isAuthenticated = function () {
	return this.hasOwnProperty('username') && this.username != null && this.username.length > 0;
};

User.prototype.isActivated = function () {
	return this.hasOwnProperty('activated') && this.activated != null && this.activated === true;
};

User.prototype.signIn = function (username, password, rememberMe, successCallback, failureCallback) {
	var me = this;
	UserController.getInstance().signIn(username, password, rememberMe, function (resp) {
		for (prop in resp.user) {
			me[prop] = resp.user[prop];
		}

		me.fireUserUpdatedEvent(User.EventType.SIGN_IN, resp);
		if (successCallback) successCallback(resp);
	}, function () {
		me.username = null;
		if (failureCallback) failureCallback();
	});
};

User.prototype.signOut = function (successCallback, failureCallback) {
	var me = this;
	UserController.getInstance().signOut(function () {
		for (var prop in me) {
			if (prop !== 'listeners') {
				delete me[prop];
			}
		}

		me.fireUserUpdatedEvent(User.EventType.SIGN_OUT);
		if (successCallback) successCallback();
	}, function () {
		if (failureCallback) failureCallback();
	});
};

User.prototype.register = function (userData, successCallback, failureCallback) {
	var me = this;
	UserController.getInstance().registerCustomer(userData, function (resp) {
		for (var prop in resp.user) {
			me[prop] = resp.user[prop];
		}

		me.fireUserUpdatedEvent(User.EventType.SIGN_IN, resp);
		if (successCallback) successCallback();
	}, function (code, message) {
		if (failureCallback) failureCallback(code, message);
	});
};

User.prototype.activate = function (activationCode, successCallback, failureCallback) {
	var me = this;
	UserController.getInstance().activateCustomer(activationCode, function () {
		me.activated = true;
		me.fireUserUpdatedEvent(User.EventType.ACTIVATION);
		if (successCallback) successCallback();
	}, function () {
		if (failureCallback) failureCallback();
	});
};

User.prototype.addUserListener = function (listener) {
	this.listeners.push(listener);
};

User.prototype.fireUserUpdatedEvent = function (eventType, resp) {
	for (var i = 0; i < this.listeners.length; i++) {
		if (typeof this.listeners[i] == 'function') {
			this.listeners[i](this, eventType, resp);
		} else {
			this.listeners[i].userUpdated(this, eventType, resp);
		}
	}
};