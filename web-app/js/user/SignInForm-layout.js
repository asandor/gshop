// This file was automatically generated from SignInForm-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.user == 'undefined') { gshop.user = {}; }


gshop.user.signInForm = function(opt_data, opt_ignored, opt_ijData) {
  return '<div class="singin-form"><div class="message"></div><form><table><tr><td>' + soy.$$escapeHtml(opt_ijData.loc['login.username']) + '</td><td><input type="text" name="username"></td></tr><tr><td>' + soy.$$escapeHtml(opt_ijData.loc['login.password']) + '</td><td><input type="password" name="password"></td></tr><tr><td colspan="2">' + soy.$$escapeHtml(opt_ijData.loc['login.rememberme']) + ' <input type="checkbox" name="rememberMe"/></td></tr></table></form></div>';
};
