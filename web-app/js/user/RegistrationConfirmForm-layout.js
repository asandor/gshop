// This file was automatically generated from RegistrationConfirmForm-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.user == 'undefined') { gshop.user = {}; }


gshop.user.registrationConfirmForm = function(opt_data, opt_ignored, opt_ijData) {
  return '<div class="registration-confirmation-entry"><div class="message">' + soy.$$escapeHtml(opt_ijData.loc['registration.confirmationCode']) + '</div><form><label>' + soy.$$escapeHtml(opt_ijData.loc['userdata.confirmationCode']) + '</label><input type="text" class="form-field-textinput" name="confirmationCode"/></form><button class="submit">' + soy.$$escapeHtml(opt_ijData.loc['save']) + '</button><div class="message">' + soy.$$escapeHtml(opt_ijData.loc['registration.confirmationCode.resend.text']) + '</div><button class="resend">' + soy.$$escapeHtml(opt_ijData.loc['registration.confirmationCode.resend.button']) + '</button></div><div class="registration-confirmation-result" style="display:none"><div class="message"></div></div>';
};
