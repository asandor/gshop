"use strict";

function RegistrationPanel(dialog, user) {
	WizardPanel.call(this, dialog);
	this.user = user;
}

inherit(RegistrationPanel, WizardPanel);

RegistrationPanel.prototype.onAddToDialog = function () {
	RegistrationPanel._superClass.onAddToDialog.call(this);

	this.getMainElement().append(gshop.user.registrationPanel(null, null, {loc: document.locstr}));

	var me = this;
	this.privateCustomerForm = new RegistrationForm(this.getMainElement().find('.private-customer-form'), this.user, 'PrivateCustomer');
	this.privateCustomerForm.submitHandler = function (data) {
		me.onFormSubmit(data)
	};
//	this.privateCustomerForm.setModel({
//		email: 'adam.sandor@test.sk',
//		password: 'aaaaa',
//		passwordConfirm: 'aaaaa',
//		firstName: 'Adam',
//		lastName: 'Sandor'
//	});
	this.businessCustomerForm = new RegistrationForm(this.getMainElement().find('.business-customer-form'), this.user, 'BusinessCustomer');
	this.businessCustomerForm.submitHandler = function (data) {
		me.onFormSubmit(data)
	};
	this.activateForm(this.privateCustomerForm);

	this.getMainElement().find('input[name="role-select"]').click(function () {
		if ($(this).val() === 'PrivateCustomer') {
			me.activateForm(me.privateCustomerForm);
		} else if ($(this).val() === 'BusinessCustomer') {
			me.activateForm(me.businessCustomerForm);
		}
	});

	this.messageField = this.getMainElement().find('.rp-message');
};

RegistrationPanel.prototype.onFormSubmit = function (data) {
	var me = this;
	this.user.register(data, function () {
		me.getDialog().swapPanels(WizardDialog.SwapDirection.FORWARD);
	}, function (code, message) {
		if (code) {
			console.log("Registration error [" + code + "]: " + message);
			me.activeForm.displayErrorMessage(message);
		} else {
			me.activeForm.displayErrorMessage(document.locstr['registration.failure']);
		}
	});
};

RegistrationPanel.prototype.activateForm = function (form) {
	if (this.activeForm !== form) {
		if (form === this.privateCustomerForm) {
			this.businessCustomerForm.hide(false);
			this.privateCustomerForm.show();
		} else if (form === this.businessCustomerForm) {
			this.privateCustomerForm.hide(false);
			this.businessCustomerForm.show();
		}
		this.activeForm = form;
	}
};

RegistrationPanel.prototype.validate = function () {
	return this.activeForm.validate();
};

RegistrationPanel.prototype.getSpecialNextButton = function () {
	if (!this.myNextButton) {
		var me = this;
		this.myNextButton = $('<button>' + locstr['registration.submit'] + '</button>').button()
		this.myNextButton.click(function () {
			me.activeForm.submit();
		});
	}
	return this.myNextButton;
};

RegistrationPanel.prototype.getPanelType = function () {
	return "RegistrationPanel";
};