// This file was automatically generated from RegistrationPanel-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.user == 'undefined') { gshop.user = {}; }


gshop.user.registrationPanel = function(opt_data, opt_ignored, opt_ijData) {
  return '\t<div class="role-select-panel"><div class="role"><input type="radio" name="role-select" value="PrivateCustomer" checked>' + soy.$$escapeHtml(opt_ijData.loc['registration.privateCustomer']) + '</div><div class="role"><input type="radio" name="role-select" value="BusinessCustomer">' + soy.$$escapeHtml(opt_ijData.loc['registration.businessCustomer']) + '</div></div><div class="private-customer-form"></div><div class="business-customer-form"></div>';
};
