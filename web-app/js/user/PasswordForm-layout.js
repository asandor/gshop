// This file was automatically generated from PasswordForm-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.user == 'undefined') { gshop.user = {}; }


gshop.user.passwordForm = function(opt_data, opt_ignored, opt_ijData) {
  return '\t<div class="message"></div><div class="header"><h2 class="title">' + soy.$$escapeHtml(opt_ijData.loc['userdata.passwordTitle']) + '</h2><button class="submit">' + soy.$$escapeHtml(opt_ijData.loc['save']) + '</button></div><form><table><tr><td class="form-field-label"><label>' + soy.$$escapeHtml(opt_ijData.loc['userdata.oldpassword']) + '</label></td><td><input type="password" class="form-field-textinput" name="oldPwd"/></td></tr><tr><td class="form-field-label"><label>' + soy.$$escapeHtml(opt_ijData.loc['userdata.newpassword']) + '</label></td><td><input type="password" class="form-field-textinput" name="newPwd"/></td></tr><tr><td class="form-field-label"><label>' + soy.$$escapeHtml(opt_ijData.loc['userdata.passwordConfirm']) + '</label></td><td><input type="password" class="form-field-textinput" name="confirmPwd"/></td></tr></table></form>';
};
