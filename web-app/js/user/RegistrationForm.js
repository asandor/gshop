function RegistrationForm(element, user, role) {
    this.user = user;
    this.role = role;
    ViewEditForm.call(this, element, 'edit');
}

inherit(RegistrationForm, ViewEditForm);

RegistrationForm.prototype.layoutTemplate = function () {
    return gshop.user.registrationForm({role:this.role}, null, {loc:document.locstr});
};

RegistrationForm.prototype.getValidatorSettings = function () {
    var builder = new ValidationBuilder();
    var equalTo = this.role == 'PrivateCustomer' ?
        '.registration-form.private input[name="password"]' :
        '.registration-form.business input[name="password"]';
    builder.email('email')
        .requiredMaxLength('phoneNumber', true, 20)
        .custom('password', {required:true, minlength:5},
        {required:document.locstr['registration.validation.emptyField'], minlength:document.locstr['registration.validation.password_length']})
        .custom('passwordConfirm', {required:true, equalTo:equalTo},
        {required:document.locstr['registration.validation.emptyField'], equalTo:document.locstr['registration.validation.password_match']});

    if (this.role === 'PrivateCustomer') {
        builder.requiredMaxLength('firstName', true, 30)
            .requiredMaxLength('lastName', true, 30)
    } else if (this.role === 'BusinessCustomer') {
        builder.requiredMaxLength('companyName', true, 50)
            .requiredMaxLength('contactPerson', true, 50)
            .requiredMaxLength('companyId', true, 30)
            .requiredMaxLength('taxId', true, 30)
            .requiredMaxLength('vat', true, 30)
    }
    return builder.build();
};