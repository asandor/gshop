// This file was automatically generated from PaymentPanel-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.basket == 'undefined') { gshop.basket = {}; }


gshop.basket.paymentPanel = function(opt_data, opt_ignored) {
  return '<div id="paymentMethod"><div class="shipment-method-selection-wrapper"><h2>' + soy.$$escapeHtml(opt_data.loc['shipment.type']) + '</h2><div class="shipment-method-selection"> </div></div><div class="payment-method-selection-wrapper"><h2>' + soy.$$escapeHtml(opt_data.loc['payment.type']) + '</h2><div class="payment-method-selection"></div></div></div>';
};
