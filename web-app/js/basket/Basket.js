'use strict';

function Basket() {
	Module.call(this, 'basket');

	this.listeners = [];
	this.numberOfRequestsPending = 0;
}

inherit(Basket, Module);

Basket.prototype.initialize = function (basketData) {
	if (basketData.items) {
		this.items = basketData.items;
	} else {
		this.items = [];
	}

	if (basketData.price) {
		this.price = basketData.price;
	}

	if (basketData.appliedDiscounts) {
		this.appliedDiscounts = basketData.appliedDiscounts;
	} else {
		this.appliedDiscounts = [];
	}

	if (basketData.noDiscountPrice) {
		this.noDiscountPrice = basketData.noDiscountPrice;
	}
};

Basket.prototype.registerBasketEventListener = function (listener) {
	this.listeners.push(listener);
};

Basket.prototype.addProduct = function (productCode, amount) {
	console.log("Adding " + amount + " pieces of product '" + productCode + "' to basket");
	var me = this;
	me.numberOfRequestsPending += 1;
	me.fireServerRequestEvent();
	$.post(UrlMappings.getUrl('BasketController.addProduct'), { 'productCode': productCode, 'amount': amount })
			.error(function (e) {
				$.gritter.add({
					title: document.locstr['warning'],
					class_name: 'warning',
					text: document.locstr['basket.newItemNOTAdded-message']
				});
				me.handleServerError(e);
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				me.price = resp.price;
				me.noDiscountPrice = resp.noDiscountPrice;
				me.appliedDiscounts = resp.appliedDiscounts;

				var existingItemIndex = me.getItemIndexByProductCode(productCode);
				var item = resp.item;
				if (existingItemIndex === null) {
					me.items.push(item);
					me.fireItemAddedEvent(item);
				} else {
					me.items[existingItemIndex] = item;
					me.fireItemUpdatedEvent(existingItemIndex, item);
				}

				$.gritter.add({
					title: document.locstr['congratulations'],
					class_name: 'congratulations',
					time: 5000,
					image: item.product.imageUrlMedium,
					text: document.locstr['basket.newItemAdded-message']
				});
			})
			.complete(function (e) {
				me.numberOfRequestsPending -= 1;
				me.fireServerRequestEvent();
			});
};

Basket.prototype.changeItemAmount = function (productCode, amount, inc, dec) {
	var me = this;
	me.numberOfRequestsPending += 1;
	me.fireServerRequestEvent();

	var params = {'productCode': productCode};
	if (inc) {
		console.log("Increasing amount of '" + productCode + "' by " + inc);
		params.inc = inc;
	}
	if (dec) {
		console.log("Decreasing amount of '" + productCode + "' by " + dec);
		params.dec = dec;
	}
	if (amount) {
		console.log("Setting amount of '" + productCode + "' to " + amount);
		params.amount = amount;
	}

	$.post(UrlMappings.getUrl('BasketController.changeItemAmount'), params)
			.error(function (e) {
				me.handleServerError(e);
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				me.price = resp.price;
				me.noDiscountPrice = resp.noDiscountPrice;
				me.appliedDiscounts = resp.appliedDiscounts;

				var existingItemIndex = me.getItemIndexByProductCode(productCode);
				var item = resp.item;
				if (existingItemIndex === null) {
					throw "Local item '" + productCode + "' missing";
				}

				me.items[existingItemIndex] = item;
				me.fireItemUpdatedEvent(existingItemIndex, item);
			})
			.complete(function (e) {
				me.numberOfRequestsPending -= 1;
				me.fireServerRequestEvent();
			});
};

Basket.prototype.removeItem = function (productCode) {
	console.log("Removing item '" + productCode + "' from basket");

	var me = this;
	me.numberOfRequestsPending += 1;
	me.fireServerRequestEvent();
	$.post(UrlMappings.getUrl('BasketController.removeProduct'), { 'productCode': productCode })
			.error(function (e) {
				me.handleServerError(e);
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}
				me.price = resp.price;
				me.noDiscountPrice = resp.noDiscountPrice;
				me.appliedDiscounts = resp.appliedDiscounts;

				var itemIndex = me.getItemIndexByProductCode(productCode);
				if (itemIndex === null) {
					throw "Basket item with product code '" + productCode + "' not found in local basket!";
				}
				var item = me.items.splice(itemIndex, 1);
				me.fireItemRemovedEvent(item, itemIndex);
			})
			.complete(function (e) {
				me.numberOfRequestsPending -= 1;
				me.fireServerRequestEvent();
			});
};

Basket.prototype.clearBasket = function (clearOnServer) {
	console.log("Clearing basket");

	var me = this;
	if (clearOnServer) {
		me.numberOfRequestsPending += 1;
		me.fireServerRequestEvent();
		$.post(UrlMappings.getUrl('BasketController.clearBasket'))
				.error(function (e) {
					me.handleServerError(e);
				})
				.success(function (resp) {
					if (!(resp instanceof Object)) {
						throw "Response '" + resp.toString() + "' cannot be parsed";
					}
					me.price = resp.price;
					me.noDiscountPrice = resp.noDiscountPrice;
					me.appliedDiscounts = resp.appliedDiscounts;

					me.items = [];
					me.fireBasketUpdatedEvent();
				})
				.complete(function (e) {
					me.numberOfRequestsPending -= 1;
					me.fireServerRequestEvent();
				});
	} else {
		me.price = {
			base: 0.00,
			vatPercent: 0,
			vatAmount: 0.00,
			baseWithVat: 0.00
		};
		me.items = [];
		me.noDiscountPrice = me.price;
		me.appliedDiscounts = [];

		me.fireBasketUpdatedEvent();
	}
};

Basket.prototype.getItemByProductCode = function (productCode) {
	var i = this.getItemIndexByProductCode(productCode);
	if (i === null) {
		return null;
	}
	return this.items[i];
};

Basket.prototype.getItemIndexByProductCode = function (productCode) {
	for (var i = 0; i < this.items.length; i++) {
		if (this.items[i].product.code === productCode) return i;
	}
	return null;
};

Basket.prototype.handleServerError = function (e) {
	console.log("Received error response from server: " + e.message);
};

Basket.prototype.addBasketListener = function (listener) {
	this.listeners.push(listener);
};

Basket.prototype.removeBasketListener = function (listener) {
	var index = this.listeners.indexOf(listener);
	this.listeners.splice(index, 1);
};

Basket.prototype.fireItemRemovedEvent = function (item, itemIndex) {
	for (var i = 0; i < this.listeners.length; i++) {
		if ('itemRemovedFromBasket' in this.listeners[i]) {
			this.listeners[i].itemRemovedFromBasket(item, itemIndex);
		}
	}
};

Basket.prototype.fireItemAddedEvent = function (item) {
	for (var i = 0; i < this.listeners.length; i++) {
		if ('itemAddedToBasket' in this.listeners[i]) {
			this.listeners[i].itemAddedToBasket(item);
		}
	}
};

Basket.prototype.fireItemUpdatedEvent = function (existingItemIndex, item) {
	for (var i = 0; i < this.listeners.length; i++) {
		if ('itemUpdated' in this.listeners[i]) {
			this.listeners[i].itemUpdated(existingItemIndex, item);
		}
	}
};

/**
 * Indicates that the basket underwent changes that don't fit into the other
 * event categories, so it has to be re-rendered.
 */
Basket.prototype.fireBasketUpdatedEvent = function () {
	for (var i = 0; i < this.listeners.length; i++) {
		if ('basketUpdated' in this.listeners[i]) {
			this.listeners[i].basketUpdated();
		}
	}
};

Basket.prototype.fireServerRequestEvent = function () {
	for (var i = 0; i < this.listeners.length; i++) {
		if (this.numberOfRequestsPending === 0 && 'serverRequestsFinished' in this.listeners[i]) {
			this.listeners[i].serverRequestsFinished();
		} else if (this.numberOfRequestsPending > 0 && 'serverRequestStarted' in this.listeners[i]) {
			this.listeners[i].serverRequestStarted();
		}
	}
};

Basket.prototype.isEmpty = function () {
	if (this.items == null) return true;
	if (this.items.length == 0) return true;

	var zeroAmount = true;
	for (var i = 0; i < this.items.length; i++) {
		if (this.items[i].amount > 0) zeroAmount = false;
	}

	return zeroAmount == true;
}

Basket.prototype.hasZeroAmountItem = function () {
	for (var i = 0; i < this.items.length; i++) {
		if (this.items[i].amount == 0) return true;
	}
	return false;
}

Basket.prototype.getItemsCount = function () {
	var amount = 0;
	for (var i = 0; i < this.items.length; i++) {
		amount = amount + this.items[i].amount;
	}
	return amount;
}

Basket.prototype.initializeProperties = function (basketData) {
	if (basketData.items) {
		this.items = basketData.items;
	}
	if (basketData.price) {
		this.price = basketData.price;
	}
	if (basketData.discountInfo) {
		this.discountInfo = basketData.discountInfo;
	}
};