// This file was automatically generated from BasketPrice-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.basket == 'undefined') { gshop.basket = {}; }


gshop.basket.price = function(opt_data, opt_ignored, opt_ijData) {
  var output = '<div class="basket-price-display"><table><tbody><tr class="price-value"><td class="name">' + soy.$$escapeHtml(opt_ijData.loc['basket.noDiscountPrice.base']) + ':</td><td class="value">' + soy.$$escapeHtml(opt_data.noDiscountPrice.baseWithVat) + '</td></tr>';
  var discountList8 = opt_data.appliedDiscounts;
  var discountListLen8 = discountList8.length;
  for (var discountIndex8 = 0; discountIndex8 < discountListLen8; discountIndex8++) {
    var discountData8 = discountList8[discountIndex8];
    output += '<tr class="price-value applied-discount"><td class="name">' + soy.$$escapeHtml(discountData8.title) + ':</td><td class="value">' + soy.$$escapeHtml(discountData8.description) + '</td></tr>';
  }
  output += '<tr class="price-value"><td class="name">' + soy.$$escapeHtml(opt_ijData.loc['basket.price.base']) + ':</td><td class="value">' + soy.$$escapeHtml(opt_data.price.base) + '</td></tr><tr class="price-value"><td class="name">' + soy.$$escapeHtml(opt_ijData.loc['basket.price.vatAmount']) + ':</td><td class="value">' + soy.$$escapeHtml(opt_data.price.vatAmount) + '</td></tr><tr class="price-value final-price"><td class="name">' + soy.$$escapeHtml(opt_ijData.loc['basket.price.baseWithVat']) + ':</td><td class="value">' + soy.$$escapeHtml(opt_data.price.baseWithVat) + '</td></tr></tbody></table></div>';
  return output;
};
