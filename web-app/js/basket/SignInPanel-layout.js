// This file was automatically generated from SignInPanel-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.user == 'undefined') { gshop.user = {}; }


gshop.user.signInPanel = function(opt_data, opt_ignored, opt_ijData) {
  return '<div class="singin-panel"><input type="radio" name="sign-in-opt" class="signInOption" checked/><h2>' + soy.$$escapeHtml(opt_ijData.loc['basket.signin.option']) + '</h2><div class="basket-signin-form"></div><input type="radio" name="sign-in-opt" class="registerOption"/><h2>' + soy.$$escapeHtml(opt_ijData.loc['basket.register.option']) + '</h2></div>';
};
