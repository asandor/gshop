'use strict';

var SummaryPanel = function (basket, user, basketDialog, paymentPanel, userDetailsPanel) {
	BasketPanel.call(this, basket, basketDialog);
	this.user = user;
	this.paymentPanel = paymentPanel;
	this.userDetailsPanel = userDetailsPanel;
};

inherit(SummaryPanel, BasketPanel);

SummaryPanel.prototype.onDisplay = function (width, height) {
	SummaryPanel._superClass.onDisplay.call(this, width, height);
	console.debug("Rendering SummaryPanel");
	this.getMainElement().empty();

	var me = this;
	OrderController.getInstance().summary(
			this.paymentPanel.getSelectedPaymentMethod().uid,
			this.paymentPanel.getSelectedShipmentMethod().uid,
			function(orderSummary) {
				me.getMainElement().append(gshop.basket.summaryPanel({
					orderSummary: orderSummary
				}, null, {loc: document.locstr}));
			});

	this.orderButton = this.getMainElement().find('#placeOrderButton').button();
	this.orderButton.click(function () {
		me.getDialog().placeOrder();
	});
};

SummaryPanel.prototype.renderSuccessMessage = function (result) {
	this.orderButton.hide();
	this.getMainElement().empty();
	this.getMainElement().append(gshop.basket.summaryPanelMessage({loc: document.locstr, msg: 'success', result: result}));
};

SummaryPanel.prototype.renderFailureMessage = function () {
	//TODO: better to show growl popup and leave panel state clickable
	this.orderButton.hide();
	this.getMainElement().empty();
	this.getMainElement().append(gshop.basket.summaryPanelMessage({loc: document.locstr, msg: 'failure', result: null}));
};

SummaryPanel.prototype.getSpecialNextButton = function () {
	return this.orderButton;
};