"use strict";

var SignInPanel = function (basket, basketDialog, user) {
	BasketPanel.call(this, basket, basketDialog);
	this.user = user;
	this.myNextButton = $('<button>').button({label: document.locstr['basket.signInPanel.signInButtonTitle']});
};

inherit(SignInPanel, BasketPanel);

SignInPanel.Option = {
	REGISTER: 1,
	SIGN_IN: 0
};

SignInPanel.prototype.onAddToDialog = function () {
	SignInPanel._superClass.onAddToDialog.call(this);
	var me = this;

	this.getMainElement().append(gshop.user.signInPanel(null, null, {loc: document.locstr}));
	this.signInOptionControl = this.getMainElement().find('.signInOption');
	this.registerOptionControl = this.getMainElement().find('.registerOption');
	this.signInOptionControl.click(function () {
		me.myNextButton.button('option', 'label', document.locstr['basket.signInPanel.signInButtonTitle']);
	});
	this.registerOptionControl.click(function () {
		me.myNextButton.button('option', 'label', document.locstr['basket.signInPanel.registerButtonTitle']);
	});

	this.signInForm = new SignInForm(this.getMainElement().find('.basket-signin-form'), this.user, true);
	this.signInForm.successHandler = function () {
		me.getDialog().swapPanels(WizardDialog.SwapDirection.FORWARD);
	};
	this.signInForm.failureHandler = function () {
		me.signInForm.displayErrorMessage(document.locstr['login.incorrect']);
	};
	this.myNextButton.click(function () {
		if (me.getOption() === SignInPanel.Option.SIGN_IN) {
			me.signInForm.submit();
		} else {
			me.getDialog().swapPanels(WizardDialog.SwapDirection.FORWARD);
		}
	});
};

SignInPanel.prototype.getOption = function () {
	if (this.signInOptionControl.is(":checked")) {
		return SignInPanel.Option.SIGN_IN;
	} else {
		return SignInPanel.Option.REGISTER;
	}
};

SignInPanel.prototype.onDisplay = function () {
	this.getDialog().disableSwapForward();
	this.getDialog().enableSwapBackward();
};

SignInPanel.prototype.getSpecialNextButton = function () {
	return this.myNextButton;
};

SignInPanel.prototype.getPanelType = function () {
	return "SignInPanel";
};