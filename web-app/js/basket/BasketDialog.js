"use strict";

var BasketDialog = function (basket, user) {
	WizardDialog.call(this, $('<div class="basket-dialog" style="overflow: hidden">'), {
		title: document.locstr['basket.title'],
		modal: true
	});
	this.basket = basket;
	this.user = user;

	this.initializePanelInstances();
};

inherit(BasketDialog, WizardDialog);

BasketDialog.prototype.initializePanelInstances = function () {
	console.log("Initializing BasketDialog panels");
	this.productListPanel = new ProductListPanel(this.basket, this);
	this.userDetailsPanel = new UserDetailsPanel(this.user, this);
	this.paymentPanel = new PaymentPanel(this.basket, this);
	this.summaryPanel = new SummaryPanel(this.basket, this.user, this, this.paymentPanel, this.userDetailsPanel);
	this.signInPanel = new SignInPanel(this.basket, this, this.user);
	this.activationPanel = new ActivationPanel(this, this.user);
	this.registrationPanel = new RegistrationPanel(this, this.user);
	var me = this;
	this.activationPanel.successHandler = function () {
		me.swapPanels(WizardDialog.SwapDirection.FORWARD);
	};
};

BasketDialog.prototype.initializeControlsPanel = function (controlsPanel) {
	BasketDialog._superClass.initializeControlsPanel.call(this, controlsPanel);
};

BasketDialog.prototype.getFirstPanel = function () {
	return this.productListPanel;
};

BasketDialog.prototype.getPanelToBeDisplayed = function (direction) {
	if (direction === WizardDialog.SwapDirection.FORWARD) {
		if (this.displayedPanel === this.productListPanel) {
			if (this.user.isAuthenticated()) {
				if (this.user.isActivated()) {
					return this.userDetailsPanel;
				} else {
					return this.activationPanel;
				}
			} else {
				return this.signInPanel;
			}
		}
		if (this.displayedPanel === this.userDetailsPanel) {
			return this.paymentPanel;
		}
		if (this.displayedPanel === this.paymentPanel) {
			return this.summaryPanel;
		}
		if (this.displayedPanel === this.signInPanel && this.user.isAuthenticated()) {
			return this.userDetailsPanel;
		}
		if (this.displayedPanel === this.activationPanel && this.user.isAuthenticated() && this.user.isActivated()) {
			return this.userDetailsPanel;
		}
		if (this.displayedPanel === this.signInPanel && this.signInPanel.getOption() === SignInPanel.Option.REGISTER) {
			return this.registrationPanel;
		}
		if (this.displayedPanel === this.registrationPanel && this.user.isAuthenticated()) {
			return this.activationPanel;
		}
	} else if (WizardDialog.SwapDirection.BACK) {
		if (this.displayedPanel === this.userDetailsPanel) {
			return this.productListPanel;
		}
		if (this.displayedPanel === this.paymentPanel) {
			return this.userDetailsPanel;
		}
		if (this.displayedPanel === this.summaryPanel) {
			return this.paymentPanel;
		}
		if (this.displayedPanel === this.signInPanel) {
			return this.productListPanel;
		}
		if (this.displayedPanel === this.registrationPanel) {
			return this.signInPanel;
		}
	}
	return null;
};

BasketDialog.prototype.closeAndReset = function () {
	if (this.successfullOrder) {
		this.basket.clearBasket(false);
		this.successfullOrder = false;
	}
	BasketDialog._superClass.closeAndReset.call(this);
};

BasketDialog.prototype.placeOrder = function () {
	console.log("BasketDialog: placing order");

	var dialog = this;
	OrderController.getInstance().placeOrder(
			this.paymentPanel.getSelectedPaymentMethod().uid,
			this.paymentPanel.getSelectedShipmentMethod().uid,
			function (result) {
				console.log("BasketDialog: order placed successfully");
				dialog.showCloseButton();
				dialog.summaryPanel.renderSuccessMessage(result);
				dialog.successfullOrder = true;
			}, function (e) {
				console.log("BasketDialog: server returned error on order placement (" + e + ")");
				dialog.showCloseButton();
				dialog.summaryPanel.renderFailureMessage();
			});
};