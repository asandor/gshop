"use strict";

function UserDetailsPanel(user, basketDialog) {
	WizardPanel.call(this, basketDialog);
	this.user = user;
}

inherit(UserDetailsPanel, WizardPanel);

UserDetailsPanel.prototype.onAddToDialog = function () {
	UserDetailsPanel._superClass.onAddToDialog.call(this);

	this.renderContent();

	this.initFormData();
};

UserDetailsPanel.prototype.renderContent = function () {
	this.getMainElement().append(gshop.basket.userDetailsPanel(null, null, {loc: document.locstr}));
	this.userDataForm = new UserDataForm(this.getMainElement().find('.user-data-form'), this.user.role);
	this.billingAddressForm = new UserAddressForm(this.getMainElement().find('.user-billing-address-form'), 'billing', document.locstr['userdata.billingdata.billingAddress']);
	this.deliveryAddressForm = new UserAddressForm(this.getMainElement().find('.user-delivery-address-form'), 'delivery', document.locstr['userdata.billingdata.deliveryAddress']);
	this.deliveryAddressCheckbox = this.getMainElement().find('input[name="deliveryAddressActive"]');

	this.userDataForm.registerFormListener(this);
	this.billingAddressForm.registerFormListener(this);

	var me = this;
	this.deliveryAddressCheckbox.click(function (e) {
		if ($(this).is(":checked")) {
			if (!me.user.hasOwnProperty('deliveryAddress')) me.user.deliveryAddress = {};
			me.deliveryAddressForm.setModel(me.user.deliveryAddress);
			me.deliveryAddressForm.editMode();
			me.deliveryAddressForm.show();
		} else {
			UserController.getInstance().removeDeliveryAddress(function () {
				me.user.deliveryAddress = null;
				me.user.fireUserUpdatedEvent();
				me.deliveryAddressForm.hide();
			});
		}
	});
};

UserDetailsPanel.prototype.onDisplay = function () {
	if (!this.user.billingAddress) {
		this.billingAddressForm.editMode();
	}
	this.updateSwapForward();
};

UserDetailsPanel.prototype.formModeChanged = function (form, mode) {
	this.updateSwapForward();
};

UserDetailsPanel.prototype.updateSwapForward = function () {
	if (this.user.billingAddress
			&& this.userDataForm.mode === ViewEditForm.Mode.VIEW
			&& this.billingAddressForm.mode === ViewEditForm.Mode.VIEW
			&& this.deliveryAddressForm.mode === ViewEditForm.Mode.VIEW) {
		this.getDialog().enableSwapForward();
	} else {
		this.getDialog().disableSwapForward();
	}
};

UserDetailsPanel.prototype.initFormData = function () {
	this.userDataForm.setModel(this.user);
	this.billingAddressForm.setModel(this.user.billingAddress);
	if (this.user.deliveryAddress) {
		this.deliveryAddressCheckbox.attr('checked', 'checked');
		this.deliveryAddressForm.setModel(this.user.deliveryAddress);
		this.deliveryAddressForm.show();
	} else {
		this.deliveryAddressCheckbox.removeAttr('checked');
		this.deliveryAddressForm.setModel(null);
		this.deliveryAddressForm.hide();
	}
};

UserDetailsPanel.prototype.getPanelType = function () {
	return "UserDetailsPanel";
};