// This file was automatically generated from SummaryPanel-message-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.basket == 'undefined') { gshop.basket = {}; }


gshop.basket.summaryPanelMessage = function(opt_data, opt_ignored) {
  return '<div id="placeOrderStatus">' + ((opt_data.msg == 'success') ? '<h1>' + soy.$$escapeHtml(opt_data.loc['order.placeOrderSuccess']) + '</h1><br><h2>' + soy.$$escapeHtml(opt_data.loc['order.orderNumber']) + ': ' + soy.$$escapeHtml(opt_data.result) + ' </h2>' : '') + ((opt_data.msg == 'failure') ? '<h1>' + soy.$$escapeHtml(opt_data.loc['order.placeOrderFailure']) + '<h1>' : '') + '</div>';
};
