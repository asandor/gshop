"use strict";

var PaymentPanel = function (basket, basketDialog) {
	BasketPanel.call(this, basket, basketDialog);
	this.selectedShipmentMethod = null;
};

inherit(PaymentPanel, BasketPanel);

PaymentPanel.prototype.onAddToDialog = function (width, height) {
	PaymentPanel._superClass.onAddToDialog.call(this, width, height);
	console.log("Render PaymentPanel");

	var paymentPanel = this;
	$.post(UrlMappings.getUrl('OrderController.orderMethods'))
			.error(function (e) {
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}
				paymentPanel.paymentMethods = resp.paymentMethods;
				paymentPanel.shipmentMethods = resp.shipmentMethods;
				paymentPanel.validCombinations = resp.validCombinations;

				paymentPanel.selectedShipmentMethod = null;
				paymentPanel.getMainElement().empty();
				paymentPanel.renderContents();
				paymentPanel.updateRadioState();
			})
			.complete(function () {
			});
};

PaymentPanel.prototype.onDisplay = function () {
	this.updateSwapButtonState();
};

PaymentPanel.prototype.renderContents = function () {
	this.mainElement.append(gshop.basket.paymentPanel({
		loc: document.locstr,
		paymentMethods: this.paymentMethods,
		shipmentMethods: this.shipmentMethods,
		validCombinations: this.validCombinations
	}));

	this.renderShipmentMethods();

	this.mainElement.width(this.width);
};

PaymentPanel.prototype.renderShipmentMethods = function () {
	var shipmentMethodParent = this.mainElement.find('.shipment-method-selection');
	var paymentPanel = this;
	for (var i = 0; i < this.shipmentMethods.length; i++) {
		var shipmentMethod = this.shipmentMethods[i];
		var radio = this.renderOrderMethod(shipmentMethod, 'shipmentgroup', shipmentMethodParent);
		radio.click(function (e) {
			paymentPanel.shipmentMethodSelected(this.value);
		});
	}
};

PaymentPanel.prototype.updateRadioState = function () {
	var paymentMethodParent = this.mainElement.find('.payment-method-selection');
	var paymentMethodWraper = this.mainElement.find('.payment-method-selection-wrapper');
	paymentMethodParent.empty();
	paymentMethodWraper.hide();
	this.selectedPaymentMethod = null;

	var paymentPanel = this;
	if (this.selectedShipmentMethod !== null) {
		for (var i = 0; i < this.validCombinations.length; i++) {
			var vc = this.validCombinations[i];
			if (vc.shipmentMethod === this.selectedShipmentMethod.uid) {
				paymentMethodWraper.show();
				var paymentMethod = this.getMethodByUid(this.paymentMethods, vc.paymentMethod);
				var radio = this.renderOrderMethod(paymentMethod, 'paymentgroup', paymentMethodParent);
				radio.click(function (e) {
					paymentPanel.paymentMethodSelected(this.value);
				});
			}
		}
	}
};

PaymentPanel.prototype.paymentMethodSelected = function (uid) {
	this.selectedPaymentMethod = this.getMethodByUid(this.paymentMethods, uid);
	this.updateSwapButtonState();
};

PaymentPanel.prototype.shipmentMethodSelected = function (uid) {
	this.selectedShipmentMethod = this.getMethodByUid(this.shipmentMethods, uid);
	this.selectedPaymentMethod = null;
	this.updateRadioState();
	this.updateSwapButtonState();
};

PaymentPanel.prototype.updateSwapButtonState = function () {
	if (this.selectedPaymentMethod != null && this.selectedShipmentMethod != null) {
		this.getDialog().enableSwapForward();
	} else {
		this.getDialog().disableSwapForward();
	}
}

PaymentPanel.prototype.renderOrderMethod = function (method, group, parent) {
	var radio = $('<input type="radio" name="' + group + '">').attr('value', method.uid);
	var nameElement = $('<span class="ordermethod-radio-title">').append(method.name + ' (' + method.price.base + ') ')
	parent.append(radio).append(nameElement).append($('<br>'));
	return radio;
};

PaymentPanel.prototype.getMethodByUid = function (methodList, uid) {
	for (var i = 0; i < methodList.length; i++) {
		if (methodList[i].uid === uid) return methodList[i];
	}
	return null;
};

PaymentPanel.prototype.getPanelType = function () {
	return "PaymentPanel";
};

PaymentPanel.prototype.getSelectedShipmentMethod = function () {
	return this.selectedShipmentMethod;
};

PaymentPanel.prototype.getSelectedPaymentMethod = function () {
	return this.selectedPaymentMethod;
};