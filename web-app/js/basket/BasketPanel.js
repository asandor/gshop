'use strict';

var BasketPanel = function (basket, basketDialog) {
	WizardPanel.call(this, basketDialog);
	this.basket = basket;
};

inherit(BasketPanel, WizardPanel);