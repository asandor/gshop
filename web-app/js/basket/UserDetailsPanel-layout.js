// This file was automatically generated from UserDetailsPanel-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.basket == 'undefined') { gshop.basket = {}; }


gshop.basket.userDetailsPanel = function(opt_data, opt_ignored, opt_ijData) {
  return '<div class="user-details-panel"><div class="user-data-form"></div><div class="user-billing-address-wrapper"><div class="user-billing-address-form"> </div><div class="deliveryAddressActivePane">' + soy.$$escapeHtml(opt_ijData.loc['userdata.deliveryAddressNotAsBillingAddress']) + '<input type="checkbox" name="deliveryAddressActive"/></div></div><div class="user-delivery-address-form" style="display: none"></div></div>';
};
