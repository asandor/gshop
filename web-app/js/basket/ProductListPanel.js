"use strict";

var ProductListPanel = function (basket, basketDialog) {
	BasketPanel.call(this, basket, basketDialog);
	this.basket.addBasketListener(this);
	this.pricePanelHeight = 100;
};

inherit(ProductListPanel, BasketPanel);

ProductListPanel.prototype.itemAddedToBasket = function (item) {
	if (this.isAddedToDialog()) {
		var row = this.renderBasketItemTableRow($('<tr>'), item);
		this.basketItemTable.append(row);
		this.renderBasketPrice();
		this.updateSwapButtons();
	}
};

ProductListPanel.prototype.itemUpdated = function (index, item) {
	if (this.isAddedToDialog()) {
		var currentRow = $(this.basketItemTable.find('tr')[index]);
		currentRow.empty();
		this.renderBasketItemTableRow(currentRow, item);
		this.renderBasketPrice();
		this.updateSwapButtons();
	}
};

ProductListPanel.prototype.itemRemovedFromBasket = function (item, index) {
	if (this.isAddedToDialog()) {
		var currentRow = $(this.basketItemTable.find('tr')[index]);
		currentRow.remove();
		this.renderBasketPrice();
		this.updateSwapButtons();
	}
};

ProductListPanel.prototype.basketUpdated = function () {
	if (this.isAddedToDialog()) {
		this.render();
		this.updateSwapButtons();
	}
};

ProductListPanel.prototype.serverRequestStarted = function () {
	if (this.isAddedToDialog()) {
		this.progressBar.show();
	}
};

ProductListPanel.prototype.serverRequestsFinished = function () {
	if (this.isAddedToDialog()) {
		this.progressBar.hide();
	}
};

ProductListPanel.prototype.onAddToDialog = function () {
	ProductListPanel._superClass.onAddToDialog.call(this);
	this.render();
};

ProductListPanel.prototype.render = function () {
	this.mainElement.empty();

	this.basketItemTableHeader = this.renderBasketItemTableHeader();
	this.basketItemTable = this.renderBasketItemTable();
	this.tableScrollWrapper = $('<div class="scroll-wrapper">');
	this.tableScrollWrapper.height(this.height - this.pricePanelHeight);
	this.tableScrollWrapper.css('overflow-y', 'auto');

	this.mainElement.append(this.basketItemTableHeader);
	this.mainElement.append(this.tableScrollWrapper.append(this.basketItemTable));

	this.basketPrice = $('<div class="basket-price-display">');
	this.renderBasketPrice();
	this.mainElement.append(this.basketPrice);

	this.progressBar = $('<img src="' + UrlMappings.getUrl('imageDir') + '/spinner.gif">');
	this.mainElement.append(this.progressBar);

//		var clearButton = $('<button>').button({label: 'Clear'}).click(function () {
//			this.basket.clearBasket();
//		});
//		this.mainElement.append(clearButton);

	if (this.basket.numberOfRequestsPending === 0) {
		this.progressBar.hide();
	}
};

ProductListPanel.prototype.renderBasketItemTable = function () {
	var table = $('<table class="basket-item-table">');

	var className = '';
	for (var i = 0; i < this.basket.items.length; i++) {
		if (i % 2) {
			className = 'even';
		} else {
			className = 'odd';
		}
		if (i == this.basket.items.length - 1) {
			className += ' last';
		}
		var row = this.renderBasketItemTableRow($('<tr class="' + className + '">'), this.basket.items[i]);
		table.append(row);
	}
	return table;
};

ProductListPanel.prototype.renderBasketItemTableHeader = function () {
	var table = $('<table class="basket-item-table">');
	var thead = $("<thead>").append($('<th class="thumbnail">'))//image column
			.append($('<th class="product">').html(document.locstr['basket.table.product.title']))
			.append($('<th class="amount">').html(document.locstr['basket.table.amount']))
			.append($('<th class="pricePerPiece">').html(document.locstr['basket.table.product.price']))
			.append($('<th class="tax">').html(document.locstr['basket.table.price.vat']))
			.append($('<th class="discount">').html(document.locstr['basket.table.discount']))
			.append($('<th class="sum">').html(document.locstr['basket.table.price.baseWithVat']))
			.append($('<th class="delete">'));
	table.append(thead);
	return table;
};

ProductListPanel.prototype.renderBasketItemTableRow = function (row, item) {
	row.append($('<td class="thumbnail">').html('<img src="' + item.product.imageUrlSmall + '"/>'));
	row.append($('<td class="product" style="text-align: left;">').append($('<div class="title">').text(item.product.title))
			.append($('<div class="code">').text(item.product.code)));

	var productListPanel = this;
	var amountCell = $('<td class="amount">');
	var buttons = $(document.createElement('span')).addClass('buttons');
	var addButton = $('<button>').addClass('up').button({text: false, icons: {primary: "ui-icon-carat-1-n"}});
	addButton.click({productCode: item.product.code, inc: 1}, function (evt) {
		productListPanel.changeItemAmountButtonClicked(evt.data.productCode, evt.data.inc, null);
	});

	if (item.amount > 0) {
		var removeButton = $('<button>').addClass('down').button({text: false, icons: {primary: "ui-icon-carat-1-s"}});
		removeButton.click({productCode: item.product.code, dec: 1}, function (evt) {
			productListPanel.changeItemAmountButtonClicked(evt.data.productCode, null, evt.data.dec);
		});
	}

	amountCell.append(removeButton);
	amountCell.append($('<span class="basket-item-amount">').text(item.amount));
	amountCell.append(addButton);

	row.append(amountCell);

	row.append($('<td class="pricePerPiece">').html(item.product.price));
	row.append($('<td class="tax">').html(item.price.vatPercent));

	var discountTd = $('<td class="discount">');
	row.append(discountTd);
	if (item.discountDescription) {
		discountTd.html(item.discountDescription);
	}

	row.append($('<td class="sum">').html(item.price.baseWithVat));
	var removeItemButton = $('<button>').button({text: false, icons: {primary: "ui-icon-close"}});
	removeItemButton.click({productCode: item.product.code}, function (evt) {
		productListPanel.removeItemButtonClicked(evt.data.productCode);
	});
	row.append($('<td class="delete" style="padding-right: 10px;">').append(removeItemButton));
	row.hover(
			function () {
				row.stop(true).addClass('row-hover');
				row.find('.thumbnail').stop(true).animate({backgroundColor: '#ffffff'}, '400')
			},
			function () {
				row.stop(true).removeClass('row-hover');
				row.find('.thumbnail').stop(true).animate({backgroundColor: '#ffffff'}, '350');
			});

	return row;
};

ProductListPanel.prototype.renderBasketPrice = function () {
	this.basketPrice.empty();
	this.basketPrice.append(gshop.basket.price({
		price: this.basket.price,
		noDiscountPrice: this.basket.noDiscountPrice,
		appliedDiscounts: this.basket.appliedDiscounts
	}, null, {
		loc: document.locstr
	}));
};

ProductListPanel.prototype.changeItemAmountButtonClicked = function (productCode, inc, dec) {
	this.getBasket().changeItemAmount(productCode, null, inc, dec);
};

ProductListPanel.prototype.removeItemButtonClicked = function (productCode) {
	this.getBasket().removeItem(productCode);
};

ProductListPanel.prototype.getPanelType = function () {
	return "ProductListPanel";
};

ProductListPanel.prototype.onDisplay = function () {
	SummaryPanel._superClass.onDisplay.call(this);
	this.updateSwapButtons();
};

ProductListPanel.prototype.updateSwapButtons = function () {
	this.getDialog().disableSwapBackward();
	if (this.getBasket().isEmpty() || this.getBasket().hasZeroAmountItem()) {
		this.getDialog().disableSwapForward();
	} else {
		this.getDialog().enableSwapForward();
	}
};