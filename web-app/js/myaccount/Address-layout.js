// This file was automatically generated from Address-layout.soy.
// Please don't edit this file by hand.

if (typeof gshop == 'undefined') { var gshop = {}; }
if (typeof gshop.myaccount == 'undefined') { gshop.myaccount = {}; }


gshop.myaccount.address = function(opt_data, opt_ignored) {
  return '\t<table><tr><td>' + soy.$$escapeHtml(opt_data.address.name) + '</td></tr>' + ((opt_data.address.companyName) ? '<tr><td>' + soy.$$escapeHtml(opt_data.address.companyName) + '</td></tr>' : '') + '<tr><td>' + soy.$$escapeHtml(opt_data.address.postalCode) + ' ' + soy.$$escapeHtml(opt_data.address.city) + '</td></tr><tr><td>' + soy.$$escapeHtml(opt_data.address.street) + ' ' + soy.$$escapeHtml(opt_data.address.number) + '</td></tr><tr><td>' + soy.$$escapeHtml(opt_data.address.country.display) + '</td></tr></table>';
};
