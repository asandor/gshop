function OrderDetailWidget(element) {
	this.mainElement = element;
}

OrderDetailWidget.prototype.displayOrder = function (order) {
	this.mainElement.empty();

	if (order != null) {
		this.mainElement.append(gshop.myaccount.orderDetailWidget({order: order}, null, {loc: document.locstr}));

		this.productList = new OrderProductTable(this.mainElement.find('.product-list'));
		this.productList.displayProducts(order.products);
	}
};