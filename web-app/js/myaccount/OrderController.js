"use strict";

function OrderController() {
}

OrderController.prototype.overview = function (successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('OrderController.overview'))
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				if (successCallback) successCallback(resp);
			});
};

OrderController.prototype.details = function (orderId, successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('OrderController.details'), {orderId: orderId})
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				if (successCallback) successCallback(resp);
			});
};

OrderController.prototype.placeOrder = function(paymentMethodId, shipmentMethodId, successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('OrderController.placeOrder'), {paymentMethodId: paymentMethodId, shipmentMethodId: shipmentMethodId})
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (successCallback) successCallback(resp);
			});
};

OrderController.prototype.summary = function(paymentMethodId, shipmentMethodId, successCallback, errorCallback) {
	$.post(UrlMappings.getUrl('OrderController.summary'), {paymentMethodId: paymentMethodId, shipmentMethodId: shipmentMethodId})
			.error(function (e) {
				if (errorCallback) errorCallback();
			})
			.success(function (resp) {
				if (!(resp instanceof Object)) {
					throw "Response '" + resp.toString() + "' cannot be parsed";
				}

				if (successCallback) successCallback(resp);
			});
};

OrderController.getInstance = function () {
	return orderController;
};

var orderController = new OrderController();