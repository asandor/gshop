$(document).ready(function () {
	new MyOrders().startup();
});

function MyOrders() {
	CommonModuleManager.call(this, []);
}

inherit(MyOrders, CommonModuleManager);

MyOrders.prototype.onModuleInitializationComplete = function () {
	var me = this;
	this.user = this.getModule('user');
	this.user.addUserListener(this);

	this.orderTable = new OrderTable($('#orderTable'));
	this.orderDetailWidget = new OrderDetailWidget($('#orderDetailWidget'));
	this.orderDetailDialog = this.orderDetailWidget.mainElement;
	this.orderDetailDialog.dialog({
		autoOpen: false,
		modal: false,
		resizable: true,
		draggable: true,
		width: 700,
		height: 580,
		close: function () {
			me.dialogClosed();
		},
		open: function () {
			me.dialogOpened();
		}
	});

	OrderController.getInstance().overview(function (resp) {
		me.orderTable.displayOverview(resp);
	}, function () {
		$.gritter.add({
			title: document.locstr['error'],
			text: document.locstr['order.load.error'],
			class_name: 'error'
		});
	});

	this.orderTable.setSelectionListener(function (orderId) {
		me.orderSelected(orderId);
	});
};

MyOrders.prototype.orderSelected = function (orderId) {
	this.orderDetailDialog.dialog('open');
};

MyOrders.prototype.dialogOpened = function () {
	var orderId = this.orderTable.getSelectedOrderId();

	this.orderDetailWidget.displayOrder(null);

	var me = this;
	OrderController.getInstance().details(orderId, function (resp) {
		me.orderDetailWidget.displayOrder(resp);
	});
};

MyOrders.prototype.dialogClosed = function () {
	this.orderTable.deselectAllRows();
};

MyOrders.prototype.userUpdated = function (user, eventType, resp) {
	if (eventType === User.EventType.SIGN_OUT) {
		window.location = UrlMappings.getUrl('UserController.MyOrders');
	}
};

var instance = new MyOrders();

MyOrders.getInstance = function () {
	return instance;
};