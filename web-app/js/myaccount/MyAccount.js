$(document).ready(function () {
	new MyAccount().startup();
});

function MyAccount() {
	CommonModuleManager.call(this, []);
}

inherit(MyAccount, CommonModuleManager);

MyAccount.prototype.onModuleInitializationComplete = function () {
	var me = this;

	this.user = this.getModule('user');
	this.user.addUserListener(this);

	this.userDataForm = new UserDataForm($('.user-data-form'), this.user.role);
	this.billingAddressForm = new UserAddressForm($('.user-billing-address-form'), 'billing', document.locstr['userdata.billingdata.billingAddress']);
	this.deliveryAddressForm = new UserAddressForm($('.user-delivery-address-form'), 'delivery', document.locstr['userdata.billingdata.deliveryAddress']);
	this.passwordForm = new PasswordForm($('.user-password-form'));

	this.updateFormsFromUser();

	this.deliveryAddressCheckbox = $('input[name="deliveryAddressActive"]')
	this.deliveryAddressCheckbox.click(function () {
		me.deliveryAddressCheckboxClicked(me.deliveryAddressCheckbox);
	});
};

MyAccount.prototype.userUpdated = function (user, eventType, resp) {
	if (eventType === User.EventType.SIGN_OUT) {
		window.location = UrlMappings.getUrl('UserController.myAccount');
	} else {
		this.updateFormsFromUser();
	}
};

MyAccount.prototype.updateFormsFromUser = function () {
	var userDisplayName = this.user.role === 'PrivateCustomer' ? this.user.firstName + ' ' + this.user.lastName : this.user.companyName;
	$('#username').text(userDisplayName + ' (' + this.user.username + ')');

	this.userDataForm.setModel(this.user);
	this.billingAddressForm.setModel(this.user.billingAddress);
	if (this.user.deliveryAddress) {
		this.deliveryAddressForm.setModel(this.user.deliveryAddress);
		this.deliveryAddressForm.show();
	}
};

MyAccount.prototype.deliveryAddressCheckboxClicked = function (checkbox) {
	if (checkbox.is(":checked")) {
		if (!this.user.deliveryAddress)
			this.user.deliveryAddress = {};
		this.deliveryAddressForm.setModel(this.user.deliveryAddress);
		this.deliveryAddressForm.editMode();
		this.deliveryAddressForm.show();
	} else {
		var me = this;
		UserController.getInstance().removeDeliveryAddress(function () {
			me.user.deliveryAddress = null;
			me.user.fireUserUpdatedEvent();
			me.deliveryAddressForm.hide();
		});
	}
};

MyAccount.prototype.showUserNotActivatedWarning = function () {
	$.gritter.add({
		title: document.locstr['warning'],
		text: document.locstr['activation.message.warning'],
		class_name: 'warning'
	});
};

MyAccount.prototype.showActivationSuccessNotification = function () {
	$.gritter.add({
		title: document.locstr['congratulations'],
		text: document.locstr['activation.message.success'],
		class_name: 'congratulations'
	});
};