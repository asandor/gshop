function OrderProductTable(element) {
	this.rootElement = $(element);
	this.tableElement = $('<table cellpadding="0" cellspacing="0" border="0" class="order-table display"></table>');

	var me = this;
}

OrderProductTable.prototype.displayProducts = function (productList) {
	this.rootElement.empty();
	this.rootElement.append(this.tableElement);
	this.tableElement.dataTable({
		"bPaginate": true,
		"bLengthChange": false,
        "iDisplayLength": 12,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
		"oLanguage": {
            "oPaginate": {
                "sNext": locstr['datatables.sNext'],
                "sPrevious": locstr['datatables.sPrevious']
            },
			"sLengthMenu": locstr['datatables.sLengthMenu'],
			"sZeroRecords": locstr['ordertable.message.zeroRecords'],
			"sInfo": locstr['datatables.sInfo'],
			"sInfoEmpty": locstr['datatables.sInfoEmpty'],
			"sInfoFiltered": locstr['datatables.sInfoFiltered'],
			"sProcessing": locstr['datatables.sProcessing'],
			"sLoadingRecords": locstr['datatables.sLoadingRecords'],
			"sSearch": locstr['datatables.sSearch']
		},
		aaData: productList,
		aoColumns: [
			{   sTitle: '',
				mRender: function (data, type, full) {
					return '<img src="' + data + '">';
				}
			},
			{ sTitle: locstr['BasketProduct.amount'] },
			{ sTitle: locstr['BasketProduct.code'] },
			{ sTitle: locstr['BasketProduct.title'] },
			{ sTitle: locstr['BasketProduct.price'] },
			{ sTitle: locstr['BasketProduct.discount'] }
		]
	});
};
