function OrderTable(element) {
	this.rootElement = $(element);
	this.tableElement = $('<table cellpadding="0" cellspacing="0" border="0" class="order-table display"></table>');
	this.selectionListener = null;
	this.selectedRow = null;
}

OrderTable.prototype.displayOverview = function (orderList) {
	this.rootElement.append(this.tableElement);
	this.table = this.tableElement.dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
		"oLanguage": {
            "oPaginate": {
                "sNext": locstr['datatables.sNext'],
                "sPrevious": locstr['datatables.sPrevious']
            },
			"sLengthMenu": locstr['datatables.sLengthMenu'],
			"sZeroRecords": locstr['ordertable.message.zeroRecords'],
			"sInfo": locstr['datatables.sInfo'],
			"sInfoEmpty": locstr['datatables.sInfoEmpty'],
			"sInfoFiltered": locstr['datatables.sInfoFiltered'],
			"sProcessing": locstr['datatables.sProcessing'],
			"sLoadingRecords": locstr['datatables.sLoadingRecords'],
			"sSearch": locstr['datatables.sSearch']
		},
		aaData: orderList,
		aoColumns: [
			{ sTitle: locstr['Order.dateCreated'] },
			{ sTitle: locstr['Order.uid'] },
			{ sTitle: locstr['ordertable.column.itemCount'] },
			{ sTitle: locstr['Order.price'] },
			{ sTitle: locstr['Order.status'], mRender: function (data, type, full) {
				return data.text;
			} }
		]
	});
	var me = this;
	this.tableElement.find($('tbody')).click(function (event) {
		me.deselectAllRows();

		me.selectedRow = $(event.target.parentNode);
		me.selectedRow.addClass('row_selected');

		if (me.selectionListener) me.selectionListener(me.getSelectedOrderId());
	});
};

OrderTable.prototype.getSelectedOrderId = function () {
	return this.selectedRow.find('td:nth-child(2)').text()
};

OrderTable.prototype.deselectAllRows = function () {
	$(this.table.fnSettings().aoData).each(function () {
		$(this.nTr).removeClass('row_selected');
	});
};

OrderTable.prototype.displayError = function () {
	$.gritter.add({
		title: document.locstr['error'],
		text: document.locstr['order.load.error'],
		class_name: 'error'
	});
};

OrderTable.prototype.setSelectionListener = function (listener) {
	this.selectionListener = listener;
};

