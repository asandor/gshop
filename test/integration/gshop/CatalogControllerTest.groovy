package gshop

import grails.converters.JSON
import org.codehaus.groovy.grails.commons.GrailsApplication
import gshop.product.Category
import gshop.product.Product
import gshop.product.StockMode
import org.junit.Before
import org.junit.Test

import static gshop.mock.MockDataHelper.createCategory
import static gshop.mock.MockDataHelper.createProduct

class CatalogControllerTest {

	DatabaseCatalogService databaseCatalogService

	GrailsApplication grailsApplication

	@Before
	void createTestData() {
		setupTestData()
	}

	@Test
	void testIncorrectCategoryId() {
		def result = listProducts([catId: 'wrongId'])
		assert result.products.size() == 0
	}

	@Test
	void testPageThroughCategoryPage1() {
		def result = listProducts([catId: 'SWR_C_2', pageNumber: '0', pageSize: '35', sortField: 'CREATED'])

		assert result.products.size() == 35
		assert result.products[0].code == 'SP_1'
		assert result.products[34].code == 'SP_35'
	}

	@Test
	void testPageThroughCategoryPage3() {
		def result = listProducts([catId: 'SWR_C_2', pageNumber: '2', pageSize: '35', sortField: 'CREATED'])

		assert result.products.size() == 30
		assert result.products[0].code == 'SP_71'
		assert result.products[29].code == 'SP_100'
	}

	@Test
	void testSortProductsByTitle() {
		def result = listProducts([catId: 'SWR_C_2', pageNumber: '0', pageSize: '3', sortField: 'TITLE'])

		assert result.products.size() == 3
		assert result.products[0].code == 'SP_1'
		assert result.products[1].code == 'SP_10'
		assert result.products[2].code == 'SP_100'
	}

	@Test
	void testInvalidParams() {
		def result = listProducts([catId: 'SWR_C_2', pageNumber: 'aaa', pageSize: 'bbb', sortField: 'x'])
		assert result.products.size() == grailsApplication.config.shop.catalog.product.defaultPageSize
	}

	@Test
	void testRenderingOfProductProperties() {
		def result = listProducts([catId: 'SWR_C_1', pageNumber: '0', pageSize: '1'])
		def product = result.products[0];

		assert product.code == '4320_13_tc'
		assert product.title == 'Swarovski Korálik'
		assert product.shortDescription == 'Pekný Swarowski Korálik'
		assert product.longDescription == 'Dlhý popis Swawroski Koráliku, bla, bla, fdsfd fdsf ds fsd fds f dsf sd fsd fs df sdf sdfdsffdf'
		assert product.imageExtension == 'jpg'
		assert product.url == '/product/4320_13_tc'
		assert product.prices == [
				[from: 0, price: 5],
				[from: 5, price: 4.5],
				[from: 11, price: 4]
		]
	}

	private listProducts(Map params) {
		CatalogController controller = new CatalogController()
		controller.catalogService = databaseCatalogService
		params.each { k, v -> controller.params[k] = v }
		controller.request.addPreferredLocale(new Locale("sk", "SK"))
		controller.listProducts()
		println "$params:$controller.response.contentAsString"
		return JSON.parse(controller.response.contentAsString)
	}

	private static void setupTestData() {
		gshop.mock.MockDataHelper.createCategory(extId: "SWR_MAIN", title: 'Swarovski Elements', image: 'swelements_tc.jpg', index: 0)
		gshop.mock.MockDataHelper.createCategory(extId: "SWR_C_1", parentId: "SWR_MAIN", title: 'Prívesky Swarovski', image: 'sw_privesky_tc.jpg', index: 3)
		gshop.mock.MockDataHelper.createCategory(extId: "SWR_C_2", parentId: "SWR_MAIN", title: 'Gombíky Swarovski', image: 'sw_gombiky_tc.gif', index: 1)
		gshop.mock.MockDataHelper.createCategory(extId: "SWR_C_3", parentId: "SWR_MAIN", title: 'Prišívateľné štrasy Swarovski', image: 'prisivatelne_strasy_sw_tc.jpg', index: 5)
		gshop.mock.MockDataHelper.createCategory(extId: "SWR_C_4", parentId: "SWR_MAIN", title: 'Filigrány Swarovski', image: 'fil_sw_tc.jpg', index: 8)

		def prod1 = new Product(
				active: true,
				code: '4320_13_tc',
				price: 5,
				stockAmount: 50,
				stockMode: StockMode.OUT_OF_STOCK_NOT_ORDERABLE
		)
		prod1.addToStrings(localeId: 'sk', title: 'Swarovski Korálik', shortDescription: 'Pekný Swarowski Korálik', longDescription: 'Dlhý popis Swawroski Koráliku, bla, bla, fdsfd fdsf ds fsd fds f dsf sd fsd fs df sdf sdfdsffdf')
		prod1.addToStrings(localeId: 'en', title: 'Swarovski Bead', shortDescription: 'Nice Swarovski Bead', longDescription: 'long description of swarovski bead product blabsdkjfh fdskjfh sdklfj')
		prod1.addToVolumeDiscounts(fromAmount: 5, toAmount: 10, percentage: 10)
		prod1.addToVolumeDiscounts(fromAmount: 11, percentage: 20)
		prod1.save(failOnError: true)
		Category.findByExtId("SWR_C_1").addToProducts(prod1)

		Category category = Category.findByExtId('SWR_C_2');
		(1..100).each { i -> gshop.mock.MockDataHelper.createProduct(category, i) }
	}
}
