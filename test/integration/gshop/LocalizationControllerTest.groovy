package gshop

import org.junit.Test

import javax.servlet.http.HttpServletResponse

class LocalizationControllerTest {

	@Test
	void correctLocaleFileReturned() {
		LocalizationController localizationController = new LocalizationController()
		localizationController.request.addPreferredLocale(new Locale("sk", "SK"))
		localizationController.messages()

		String jsFileContent = localizationController.response.contentAsString
		assert jsFileContent.contains("'warning':'Upozornenie'")
	}

	@Test
	void correctHeadersSet() {
		LocalizationController localizationController = new LocalizationController()
		localizationController.request.addPreferredLocale(new Locale("sk", "SK"))
		localizationController.messages()

		HttpServletResponse response = localizationController.response
		assert response.getHeaderValue("Content-Length") != null
		assert Integer.parseInt(response.getHeaderValue("Content-Length").toString()) > 0
		assert response.getHeaderValue("Content-Type") == 'text/javascript;charset=UTF-8'
	}
}

