package gshop.basket

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import gshop.ClientDataTransformer
import gshop.discount.DiscountGroupLoader
import gshop.discount.GroupDiscount
import gshop.product.Product
import gshop.product.discount.VolumeDiscount
import gshop.security.Customer
import gshop.security.DiscountGroup
import gshop.security.PrivateCustomer
import gshop.security.UserService
import gshop.shop.PricingService
import org.junit.Before
import org.junit.Test
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer

import java.text.DecimalFormat

import static org.mockito.Matchers.any
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

@TestFor(BasketService)
@Mock([PrivateCustomer, Basket, BasketItem, Product, VolumeDiscount])
class PricingServiceTest {

	PricingService pricingService

	ClientDataTransformer clientDataTransformer

	GroupDiscount groupDiscount

	DiscountGroup discountGroup

	Customer customer

	static final BigDecimal VAT = 0.2

	@Before
	void initPricingService() {
		customer = new PrivateCustomer()

		discountGroup = new DiscountGroup()
		discountGroup.discountPercentage = 0.1

		def volumeDiscount = new gshop.discount.VolumeDiscount(VAT)
		groupDiscount = new GroupDiscount()
		groupDiscount.discountGroupLoader = org.mockito.Mockito.mock(DiscountGroupLoader)

		pricingService = new PricingService()
		pricingService.vat = VAT
		pricingService.discounts = [volumeDiscount, groupDiscount]
		pricingService.userService = org.mockito.Mockito.mock(UserService)
		org.mockito.Mockito.when(pricingService.userService.getCurrentCustomer()).thenReturn(customer)

		clientDataTransformer = org.mockito.Mockito.mock(ClientDataTransformer)
		org.mockito.Mockito.when(clientDataTransformer.renderPriceValue(org.mockito.Matchers.any(BigDecimal.class))).then(new Answer<String>() {
			@Override
			String answer(InvocationOnMock invocationOnMock) throws Throwable {
				DecimalFormat f = DecimalFormat.getNumberInstance(Locale.ENGLISH)
				f.setMaximumFractionDigits(2)
				f.setMinimumFractionDigits(2)
				BigDecimal price = invocationOnMock.arguments[0] as BigDecimal
				return f.format(price.doubleValue())
			}
		})
	}

	@Test
	void calculateBasketPriceWithoutDiscounts() {
		org.mockito.Mockito.when(groupDiscount.discountGroupLoader.loadDiscountGroups(customer)).thenReturn([])

		Product productWithoutVolumeDiscounts = new Product(price: 10)

		Basket basket = new Basket()
		basket.basketItems = [
				new BasketItem(product: productWithoutVolumeDiscounts, amount: 10),
				new BasketItem(product: productWithoutVolumeDiscounts, amount: 5)
		]

		pricingService.calculateBasketPrices(basket, clientDataTransformer)

		assert basket.price.baseWithVat == 150
		assert basket.price.vatPercent == VAT
		assert basket.price.vatAmount == 25
		assert basket.price.base == 125
	}

	@Test
	void calculateBasketPriceWithVolumeDiscount() {
		org.mockito.Mockito.when(groupDiscount.discountGroupLoader.loadDiscountGroups(customer)).thenReturn([])

		Product productWithVolumeDiscount = new Product(price: 10)
		productWithVolumeDiscount.volumeDiscounts = [
				new VolumeDiscount(fromAmount: 5, percentage: 10)
		]

		Basket basket = new Basket()
		basket.basketItems = [
				new BasketItem(product: productWithVolumeDiscount, amount: 10),
				new BasketItem(product: productWithVolumeDiscount, amount: 4)
		]

		pricingService.calculateBasketPrices(basket, clientDataTransformer)

		assert basket.price.baseWithVat == 130
		assert basket.price.vatPercent == VAT
		assert basket.price.vatAmount == 21.67
		assert basket.price.base == 108.33
	}

	@Test
	void calculateBasketPriceWithVolumeAndGroupDiscounts() {
		org.mockito.Mockito.when(groupDiscount.discountGroupLoader.loadDiscountGroups(customer)).thenReturn([discountGroup])

		Product productWithVolumeDiscount = new Product(price: 10)
		productWithVolumeDiscount.volumeDiscounts = [
				new VolumeDiscount(fromAmount: 5, percentage: 10)
		]
		Product productWithoutVolumeDiscounts = new Product(price: 10)

		Basket basket = new Basket()
		basket.basketItems = [
				new BasketItem(product: productWithVolumeDiscount, amount: 10),
				new BasketItem(product: productWithoutVolumeDiscounts, amount: 10)
		]

		pricingService.calculateBasketPrices(basket, clientDataTransformer)

		assert basket.price.baseWithVat == 81 + 90
		assert basket.price.vatPercent == VAT
		assert basket.price.vatAmount == 28.5
		assert basket.price.base == 142.5
	}
}
