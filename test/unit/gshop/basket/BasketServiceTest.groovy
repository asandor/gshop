package gshop.basket

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import gshop.product.Product
import gshop.security.PrivateCustomer
import gshop.security.UserService
import org.junit.Before
import org.junit.Test

import static org.mockito.Mockito.mock
import static org.mockito.Mockito.when

@TestFor(BasketService)
@Mock([PrivateCustomer, Basket, BasketItem, Product])
class BasketServiceTest {

	private BasketService basketService

	private UserService userService

	@Before
	void initServices() {
		userService = org.mockito.Mockito.mock(UserService)
		org.mockito.Mockito.when(userService.getCurrentCustomer()).thenReturn(new PrivateCustomer(username: 'TestUser'))

		basketService = new BasketService()
		basketService.userService = userService
	}

	@Test
	void "Create Default basket for current customer"() {
		Basket basket = basketService.getDefaultBasketOfCurrentUser()

		assert basket.type == Basket.DEFAULT_BASKET_TYPE
		assert basket.customer.username == 'TestUser'
	}

	@Test
	void "Add to non-existing default Basket"() {
		Product testProduct = new Product(code: 'test-product-1', price: 1.5)
		Basket basket = basketService.createDefaultBasket()

		BasketItem item = basketService.addProductToBasket(testProduct, 5, basket)

		assert item.amount == 5
		assert item.product.code == testProduct.code
		assert basket.basketItems.size() == 1
	}

	@Test
	void "Add to existing BasketItem"() {
		Basket basket = basketService.createDefaultBasket()
		Product testProduct = new Product(code: 'test-product-1', price: 1.5)
		BasketItem basketItem = new BasketItem(product: testProduct, amount: 5)
		basket.addToBasketItems(basketItem)

		BasketItem item = basketService.addProductToBasket(testProduct, 5, basket)

		assert basket.basketItems.size() == 1
		assert item.is(basketItem)
		assert item.amount == 10
		assert item.product.is(testProduct)
	}

	@Test
	void "Change amount of BasketItem"() {
		Basket basket = basketService.createDefaultBasket()
		Product testProduct = new Product(code: 'test-product-1', price: 1.5)
		BasketItem basketItem = new BasketItem(product: testProduct, amount: 5)
		basket.addToBasketItems(basketItem)

		BasketItem item = basketService.changeItemAmount(basketItem, 44, basket)

		assert basket.basketItems.size() == 1
		assert item.is(basketItem)
		assert item.amount == 44
	}
}
