package gshop.i18n

import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class LocalizationJsSrcGeneratorTest {

	LocalizationJsSrcGenerator localizationJsSrcGenerator = new LocalizationJsSrcGenerator()

	@Rule
	public TemporaryFolder testDirectory = new TemporaryFolder()

	@Test
	void correctJavascriptGenerated() {
		Properties properties = new Properties()
		properties.put("dialog.ok", "OK")
		properties.put("dialog.cancel", "Cancel")
		properties.put("escapedString", "Someone's something")

		String localizationFileContent = localizationJsSrcGenerator.createLocalizationFileContent(properties)
		String correctJs = "document.locstr = {\n" +
				"\t'dialog.cancel':'Cancel',\n" +
				"\t'dialog.ok':'OK',\n" +
				"\t'escapedString':'Someone\\'s something'\n" +
				"};\n" +
				"locstr = document.locstr;"
		assert localizationFileContent == correctJs
	}

	@Test
	void javascriptFileGeneratedForEachInputLanguage() {
		File inputDirectory = testDirectory.newFolder()

		File englishFile = new File(inputDirectory, "messages_en.properties")
		englishFile.withWriter("UTF-8") { writer ->
			Properties properties = new Properties()
			properties.put("dialog.ok", "OK")
			properties.put("dialog.cancel", "Cancel")
			properties.store(writer, null)
		}

		File slovakFile = new File(inputDirectory, "messages_sk.properties")
		slovakFile.withWriter("UTF-8") { writer ->
			Properties properties = new Properties()
			properties.put("dialog.ok", "Ok")
			properties.put("dialog.cancel", "Zrušiť")
			properties.store(writer, null)
		}

		File outputDirectory = testDirectory.newFolder()
		localizationJsSrcGenerator.generateJavascriptLocalizationFiles(inputDirectory, outputDirectory)

		File englishJsFile = outputDirectory.listFiles().find { it.name == 'messages_en.js' }
		assert englishJsFile != null, "No english file in directory $outputDirectory (files: ${outputDirectory.listFiles()})"
		assert englishJsFile.text.length() > 0
		File slovakJsFile = outputDirectory.listFiles().find { it.name == 'messages_sk.js' }
		assert slovakJsFile != null, "No slovak file in directory $outputDirectory (files: ${outputDirectory.listFiles()})"
		assert slovakJsFile.text.length() > 0
	}

}
