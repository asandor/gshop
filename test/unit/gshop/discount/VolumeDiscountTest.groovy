package gshop.discount

import grails.test.mixin.Mock
import gshop.basket.BasketItem
import gshop.product.Product
import org.junit.Before
import org.junit.Test

@Mock([Product])
public class VolumeDiscountTest {

	private static final BigDecimal VAT = 0.2
	private static final BigDecimal PRICE_WITH_VAT = 5.5

	private Product absoluteDiscountProduct
	private Product percentDiscountProduct

	private VolumeDiscount volumeDiscount = new VolumeDiscount(VAT)

	@Before
	void setupProduct() {
		absoluteDiscountProduct = new Product(
				active: true,
				code: '4320_13_tc',
				price: PRICE_WITH_VAT,
		)
		absoluteDiscountProduct.volumeDiscounts = [
				new gshop.product.discount.VolumeDiscount(fromAmount: 5, toAmount: 10, fixValue: 5),
				new gshop.product.discount.VolumeDiscount(fromAmount: 11, fixValue: 4.5)
		]
		percentDiscountProduct = new Product(
				active: true,
				code: '4320_13_tc',
				price: PRICE_WITH_VAT,
		)
		percentDiscountProduct.volumeDiscounts = [
				new gshop.product.discount.VolumeDiscount(fromAmount: 5, toAmount: 10, percentage: 10),
				new gshop.product.discount.VolumeDiscount(fromAmount: 11, percentage: 20)
		]
	}

	@Test
	void amountBelowVolumeDiscount() {
		BasketItem basketItem = new BasketItem(product: absoluteDiscountProduct, amount: 1)

		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 5.5
		assert basketItem.price.vatAmount == 0.92
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 4.58

		basketItem.amount = 4
		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 22
		assert basketItem.price.vatAmount == 3.67
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 18.33
	}

	@Test
	void amountInFirstAbsoluteVolumeDiscount() {
		BasketItem basketItem = new BasketItem(product: absoluteDiscountProduct, amount: 5)

		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 25
		assert basketItem.price.vatAmount == 4.17
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 20.83

		basketItem.amount = 7
		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 35
		assert basketItem.price.vatAmount == 5.83
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 29.17
	}

	@Test
	void amountInMaximumAbsoluteVolumeDiscount() {
		BasketItem basketItem = new BasketItem(product: absoluteDiscountProduct, amount: 11)

		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 49.5
		assert basketItem.price.vatAmount == 8.25
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 41.25

		basketItem.amount = 15
		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 67.5
		assert basketItem.price.vatAmount == 11.25
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 56.25
	}

	@Test
	void amountInFirstPercentageVolumeDiscount() {
		BasketItem basketItem = new BasketItem(product: percentDiscountProduct, amount: 5)

		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 24.75
		assert basketItem.price.vatAmount == 4.13
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 20.63

		basketItem.amount = 7
		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 34.65
		assert basketItem.price.vatAmount == 5.78
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 28.88
	}

	@Test
	void amountInMaximumPercentageVolumeDiscount() {
		BasketItem basketItem = new BasketItem(product: percentDiscountProduct, amount: 11)

		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 48.40
		assert basketItem.price.vatAmount == 8.07
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 40.33

		basketItem.amount = 20
		volumeDiscount.apply(basketItem, null, null, null)

		assert basketItem.price.baseWithVat == 88.00
		assert basketItem.price.vatAmount == 14.67
		assert basketItem.price.vatPercent == 0.2
		assert basketItem.price.base == 73.33
	}
}
