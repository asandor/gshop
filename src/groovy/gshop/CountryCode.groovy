package gshop

enum CountryCode {

	SK(new Locale("sk_SK")),
	CZ(new Locale("cs_CZ")),
	SI(new Locale("sl_SI")),
	RS(new Locale("sr")),
	BR(new Locale("pt_BR")),
	AT(new Locale("de_AT")),
	DE(Locale.GERMANY),
	FR(Locale.FRANCE)

	Locale locale

	CountryCode(Locale locale) {
		this.locale = locale
	}
}
