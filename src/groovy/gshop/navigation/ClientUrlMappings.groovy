package gshop.navigation

import gshop.security.AdminUser
import gshop.security.User

class ClientUrlMappings {

	static Map<String, String> getClientUrlMappings(appTagLib, User user) {
		Map urlMappings = [
				'BasketController.content': appTagLib.createLink(controller: 'basket', action: 'content'),
				'BasketController.addProduct': appTagLib.createLink(controller: 'basket', action: 'addProduct'),
				'BasketController.removeProduct': appTagLib.createLink(controller: 'basket', action: 'removeProduct'),
				'BasketController.changeItemAmount': appTagLib.createLink(controller: 'basket', action: 'changeItemAmount'),
				'BasketController.clearBasket': appTagLib.createLink(controller: 'basket', action: 'clearBasket'),
				'UserController.customerDetails': appTagLib.createLink(controller: 'user', action: 'customerDetails'),
				'UserController.registerCustomer': appTagLib.createLink(controller: 'user', action: 'registerCustomer'),
				'UserController.updateCustomer': appTagLib.createLink(controller: 'user', action: 'updateCustomer'),
				'UserController.removeDeliveryAddress': appTagLib.createLink(controller: 'user', action: 'removeDeliveryAddress'),
				'UserController.updatePassword': appTagLib.createLink(controller: 'user', action: 'updatePassword'),
				'UserController.activateCustomer': appTagLib.createLink(controller: 'user', action: 'activateCustomer'),
				'UserController.signIn': appTagLib.createLink(controller: 'user', action: 'signIn'),
				'UserController.signOut': appTagLib.createLink(controller: 'user', action: 'signOut'),
				'UserController.myAccount': appTagLib.createLink(controller: 'user', action: 'myAccount'),
				'UserController.myOrders': appTagLib.createLink(controller: 'user', action: 'myOrders'),
				'OrderController.orderMethods': appTagLib.createLink(controller: 'order', action: 'orderMethods'),
				'OrderController.placeOrder': appTagLib.createLink(controller: 'order', action: 'placeOrder'),
				'OrderController.overview': appTagLib.createLink(controller: 'order', action: 'overview'),
				'OrderController.details': appTagLib.createLink(controller: 'order', action: 'details'),
				'OrderController.summary': appTagLib.createLink(controller: 'order', action: 'summary'),
				'OrderManagementController.updateState': appTagLib.createLink(controller: 'orderManagement', action: 'updateState'),
				'ObjectImageController.getImage': appTagLib.createLink(controller: 'objectImage', action: 'getImage'),
				'CatalogController.listProducts': appTagLib.createLink(controller: 'catalog', action: 'listProducts'),
				'InitController.fetchInitData': appTagLib.createLink(controller: 'init', action: 'fetchInitData'),
				'root': appTagLib.createLink(uri: '/'),
				'imageDir': appTagLib.resource(dir: 'images')
		]
		if (user instanceof AdminUser) {
			urlMappings += [
					'AdminController.index': appTagLib.createLink(controller: 'admin', action: 'index'),
					'SeoLinkController.generateAll': appTagLib.createLink(controller: 'seoLink', action: 'generateAll'),
					'SeoLinkController.generateCategory': appTagLib.createLink(controller: 'seoLink', action: 'generateCategory'),
					'SeoLinkController.generateProduct': appTagLib.createLink(controller: 'seoLink', action: 'generateProduct'),
					'CatalogManagementController.categories': appTagLib.createLink(controller: 'catalogManagement', action: 'categories'),
					'CatalogManagementController.categoryDetails': appTagLib.createLink(controller: 'catalogManagement', action: 'categoryDetails'),
					'dhtmlXTree.imagePath': appTagLib.createLink(uri: '/images/tree'),
					'dhtmlXGrid.imagePath': appTagLib.createLink(uri: '/images/dxhtmlgrid')
			]
		}
		return urlMappings;
	}
}
