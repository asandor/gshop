package gshop.navigation

/**
 * Created on 2/22/12 8:07 PM by Adam Sandor 
 */
class ErrorMessage {
	
	String code
	List arguments

	ErrorMessage(String code, Object... arguments) {
		this.code = code
		this.arguments = arguments.toList()
	}
}
