function UrlMappings() {
	this.mappings = ${mappings};
}

UrlMappings.prototype.getUrl = function (key) {
	return this.mappings[key];
};

var urlMappings = new UrlMappings();

UrlMappings.getUrl = function (key) {
	if (urlMappings) {
		return urlMappings.getUrl(key);
	} else {
		return null
	}
};

UrlMappings.appendLink = function (element, urlKey, text) {
	if (urlMappings) {
		$(element).append($("<a>").attr("href", UrlMappings.getUrl(urlKey)).append(text));
	}
};
