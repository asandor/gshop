package gshop.localization

/**
 * Static class returning locale of current session.
 */
class SessionLocale {

	private static ThreadLocal<Locale> locales = new ThreadLocal<Locale>()

	static void set(Locale locale) {
		locales.set(locale)
	}

	static Locale get() {
		if (locales.get() != null) {
			return locales.get()
		} else {
			return Locale.getDefault()
		}
	}
}
