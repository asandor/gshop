package gshop.order

enum OrderState {

	WAITING,
	IN_PROGRESS,
	DELIVERED
}
