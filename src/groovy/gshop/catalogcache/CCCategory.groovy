package gshop.catalogcache

import gshop.product.Category

class CCCategory extends CCCatalogObject {

	CCCategory() {
	}

	CCCategory(Category category) {
		dbId = category.id
		index = category.index
		smallImageName = category.smallImageName
		mediumImageName = category.mediumImageName
		category.strings.each { catstr -> titleByLocaleId[catstr.localeId] = catstr.title }
	}

	Integer index = 0

	String smallImageName
	String mediumImageName

	List<CCCategory> children

	List<CCProduct> products

}
