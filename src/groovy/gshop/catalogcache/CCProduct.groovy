package gshop.catalogcache

import gshop.product.Product
import gshop.product.StockMode

class CCProduct extends CCCatalogObject {

	CCProduct() {
	}

	CCProduct(Product product) {
		code = product.code
		imageExtension = product.imageExtension
		price = product.price
		activeFrom = product.activeFrom
		activeTo = product.activeTo
		stockAmount = product.stockAmount
		stockMode = product.stockMode
		dateCreated = product.dateCreated
		lastUpdated = product.lastUpdated
		active = product.active

		volumeDiscounts = product.volumeDiscounts.collect { vd ->
			new CCVolumeDiscount(
					fixValue: vd.fixValue,
					percentage: vd.percentage,
					fromAmount: vd.fromAmount,
					toAmount: vd.toAmount
			)
		}

		product.strings.each { pstr -> titleByLocaleId[pstr.localeId] = pstr.title }
	}

	List<CCVolumeDiscount> volumeDiscounts

	String code

	String imageExtension = 'jpg'

	BigDecimal price

	Date activeFrom
	Date activeTo

	Integer stockAmount
	StockMode stockMode

	Date dateCreated
	Date lastUpdated

}
