package gshop.catalogcache

class CCCatalogObject extends CCObject {

	boolean active

	Map<String, String> titleByLocaleId = [:]

	String getTitle(String localeId) {
		return titleByLocaleId[localeId]
	}
}
