package gshop.catalog

@grails.validation.Validateable
class Paging {
	SortableField sortField
	SortOrder sortOrder
	Integer pageSize
	Integer pageNumber

	enum SortableField {
		TITLE, PRICE, CREATED, INDEX
	}

	enum SortOrder {
		ASC, DESC
	}
}
