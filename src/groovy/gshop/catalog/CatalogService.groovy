package gshop.catalog

import gshop.product.Category
import gshop.product.Product

public interface CatalogService {

	Category getCategoryById(Long id)

	Category getCategoryBySEO(String seoLink, String localeId)

	Collection<Category> getTopCategories(Paging paging, String localeId)

	Collection<Category> getSubCategories(Category parent, Paging paging, String localeId)

	Product getProductById(Long id)

	Product getProductBySEO(String seoLink, String localeId)

	Collection<Product> getProductsInCategory(Category category, Paging paging, String localeId)

}