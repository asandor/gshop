package gshop.i18n

import groovy.json.StringEscapeUtils

import java.util.regex.Matcher
import java.util.regex.Pattern

class LocalizationJsSrcGenerator {

	private final Pattern MSG_FILENAME_PATTERN = Pattern.compile(/messages_(..)\.properties/)

	String createLocalizationFileContent(Properties properties) {
		StringBuilder output = new StringBuilder()
		output.append("document.locstr = {\n")

		int counter = properties.keySet().size()
		properties.keySet().sort().each { String key ->
			String value = StringEscapeUtils.escapeJavaScript(properties.getProperty(key))
			output.append("\t'$key':'$value'")
			counter--
			if (counter > 0) output.append(",")
			output.append("\n")
		}
		output.append("};\n").append("locstr = document.locstr;")
	}

	void generateJavascriptLocalizationFiles(File inputDirectory, File outputDirectory) {
		File[] messageBundleFiles = inputDirectory.listFiles(new FileFilter() {
			boolean accept(File file) {
				file.name =~ MSG_FILENAME_PATTERN
			}
		})
		messageBundleFiles.each { File mbf ->
			println "Processing file $mbf"
			Properties bundleEntries = new Properties()
			mbf.withReader("UTF-8") { reader -> bundleEntries.load(reader) }

			String javascript = createLocalizationFileContent(bundleEntries)
			String langCode = getLanguageCodeFromFileName(mbf.name)

			File jsSrcFile = new File(outputDirectory, "messages_${langCode}.js")
			println "Compiling Javascript localization file $jsSrcFile for language $langCode"
			jsSrcFile.withWriter("UTF-8") { writer -> writer.append(javascript) }
		}

	}

	private String getLanguageCodeFromFileName(String fileName) {
		Matcher matcher = MSG_FILENAME_PATTERN.matcher(fileName)
		matcher.find()
		return matcher.group(1)
	}
}
