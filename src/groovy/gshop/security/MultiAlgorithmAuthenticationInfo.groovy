package gshop.security

interface MultiAlgorithmAuthenticationInfo {

	String getAlgorithmName()
}
