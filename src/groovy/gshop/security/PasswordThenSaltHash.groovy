package gshop.security

import org.apache.shiro.crypto.UnknownAlgorithmException
import org.apache.shiro.crypto.hash.SimpleHash

import java.security.MessageDigest

/**
 * Hashes by putting the password first and appending the salt. SimpleHash works the
 * other way around which is incompatible with OxidEshop hashes.
 * To minimize impact, I only use this switched method for MD5 hashes, as those
 * are the ones imported from Oxid.
 */
class PasswordThenSaltHash extends SimpleHash {

	PasswordThenSaltHash(String algorithmName, Object source, Object salt) {
		super(algorithmName, source, salt)
	}

	@Override
	protected byte[] hash(byte[] bytes, byte[] salt, int hashIterations) throws UnknownAlgorithmException {
		MessageDigest digest = getDigest(getAlgorithmName());
		if (salt != null) {
			digest.reset();
			digest.update(bytes);
		}
		byte[] hashed = digest.digest(salt);
		return hashed;
	}
}
