package gshop.security

class UsernameExistsException extends RuntimeException {

	String username

	UsernameExistsException(String username) {
		this.username = username
	}

	@Override
	String getMessage() {
		"User '$username' already exists in database"
	}
}
