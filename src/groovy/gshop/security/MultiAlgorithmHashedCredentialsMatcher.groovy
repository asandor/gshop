package gshop.security

import org.apache.shiro.authc.AuthenticationInfo
import org.apache.shiro.authc.AuthenticationToken
import org.apache.shiro.authc.SaltedAuthenticationInfo
import org.apache.shiro.authc.credential.HashedCredentialsMatcher
import org.apache.shiro.crypto.hash.SimpleHash

class MultiAlgorithmHashedCredentialsMatcher extends HashedCredentialsMatcher {

	@Override
	protected Object hashProvidedCredentials(AuthenticationToken token, AuthenticationInfo info) {
		Object salt = null;
		String hashAlgorithm = getHashAlgorithmName()
		if (info instanceof SaltedAuthenticationInfo) {
			salt = ((SaltedAuthenticationInfo) info).getCredentialsSalt();
		}
		if (info instanceof MultiAlgorithmAuthenticationInfo) {
			hashAlgorithm = info.algorithmName ?: hashAlgorithm
		}
		if (hashAlgorithm == 'MD5') {
			//used for legacy users imported from Oxid Eshop
			return new PasswordThenSaltHash('MD5', token.credentials, salt)
		} else {
			return new SimpleHash(hashAlgorithm, token.credentials, salt, hashIterations);
		}
	}
}
