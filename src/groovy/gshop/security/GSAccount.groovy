package gshop.security

import org.apache.shiro.authc.SimpleAccount
import org.apache.shiro.util.ByteSource

class GSAccount extends SimpleAccount implements MultiAlgorithmAuthenticationInfo {

	private String algorithmName;

	GSAccount(Object principal, Object credentials, String realmName, ByteSource salt, String algorithmName) {
		super(principal, credentials, salt, realmName)
		this.algorithmName = algorithmName
	}

	@Override
	String getAlgorithmName() {
		return algorithmName
	}
}
