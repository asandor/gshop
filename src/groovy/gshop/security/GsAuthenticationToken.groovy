package gshop.security

import org.apache.shiro.authc.UsernamePasswordToken

class GsAuthenticationToken extends UsernamePasswordToken {

	private String activationCode

	GsAuthenticationToken(String username, String password, boolean rememberMe, String activationCode) {
		super(username, password, rememberMe)
		this.activationCode = activationCode
	}

	GsAuthenticationToken(String username, String password, String activationCode) {
		super(username, password)
		this.activationCode = activationCode
	}

	String getActivationCode() {
		return activationCode
	}
}
