package gshop.product

/**
 * Superclass for Products and Categories. This is class is not an entity because though the properties declared here are shared between Category and Product
 * they don't represent a logical connection between the two entities. Product and Category just happen to have the same properties so it's good to handle
 * these properties in a superclass, but from a database perspective I don't want to put them in the same table because this situation can change in the future.
 */
abstract class ShopObject {

	String extId

	boolean active

	String getTitle(String localeId) {
		strings.find { it.localeId == localeId }?.title
	}

	String getShortDescription(String localeId) {
		strings.find { it.localeId == localeId }?.shortDescription
	}

	String getLongDescription(String localeId) {
		strings.find { it.localeId == localeId }?.longDescription
	}
}
