package gshop.product

public enum ObjectImageType {

	SMALL('small', 'small', 42, 42),
	MEDIUM('medium', 'medium', 100, 100),
	LARGE('large', 'large', 200, 200)

	/**
	 * Used in URL parameter
	 */
	String code
	/**
	 * Directory in which images of this type are stored (only name without path)
	 */
	String directory

	Integer width
	Integer height

	ObjectImageType(String code, String directory, Integer width, Integer height) {
		this.code = code
		this.directory = directory
		this.width = width
		this.height = height
	}

	static ObjectImageType getByCode(String code) {
		ObjectImageType.values().find { it.code == code}
	}
}