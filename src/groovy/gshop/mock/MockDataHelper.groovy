package gshop.mock

import gshop.product.Category
import gshop.product.Product
import gshop.product.StockMode

class MockDataHelper {

	static Random rnd = new Random(1010)

	static Category createCategory(Map params) {
		Category cat = new Category([extId: params.extId, active: true, smallImageName: params.image, mediumImageName: params.image, index: params.index])
		if (params.parentId) Category.findByExtId(params.parentId).addToChildren(cat)
		cat.addToStrings(localeId: 'sk', title: params.title)
		return cat.save(failOnError: true)
	}

	static Product createProduct(Category cat, int spIndex, boolean createVolumnDiscounts = false) {
		def prod = new Product(
				active: true,
				code: "SP_$spIndex",
				price: rnd.nextInt(10) + 1,
				stockAmount: 5,
				stockMode: StockMode.OUT_OF_STOCK_NOT_ORDERABLE)
		prod.addToStrings(localeId: 'sk', title: "Korálik číslo $spIndex", shortDescription: "Krátky popis $spIndex", longDescription: 'Dlhý popis Swawroski Koráliku, bla, bla, fdsfd fdsf ds fsd fds f dsf sd fsd fs df sdf sdfdsffdf')
		prod.addToStrings(localeId: 'en', title: "Sample Product #$spIndex", shortDescription: "Short description of sample product #$spIndex", longDescription: 'Dlhý popis Swawroski Koráliku, bla, bla, fdsfd fdsf ds fsd fds f dsf sd fsd fs df sdf sdfdsffdf')
		if (createVolumnDiscounts) {
			prod.addToVolumeDiscounts(toAmount: 5, percentage: 20)
			prod.addToVolumeDiscounts(fromAmount: 6, toAmount: 10, percentage: 30)
			prod.addToVolumeDiscounts(fromAmount: 11, percentage: 30)
		}
		prod = prod.save(failOnError: true)
		cat.addToProducts(prod)
		return prod
	}

}