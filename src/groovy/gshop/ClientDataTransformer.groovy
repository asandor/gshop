package gshop

import gshop.basket.Basket
import gshop.basket.BasketItem
import gshop.basket.Price
import gshop.order.PaymentMethod
import gshop.order.ShipmentMethod
import gshop.product.ObjectImageType
import gshop.security.BusinessCustomer
import gshop.security.Customer
import gshop.security.PrivateCustomer
import gshop.security.User
import org.springframework.beans.factory.annotation.Value

/**
 * Transforms various data structures into a form that can be sent to the client
 */
class ClientDataTransformer {

	@Value('${shop.currency}')
	Currency currency

	def shopTagLib
	def grailsTagLib

	Map transformUser(User user) {
		def clientUser = [
				username: user.username,
				role: user.class.simpleName,
		]
		if (user instanceof Customer) {
			Customer customer = user;
			clientUser.phoneNumber = customer.phoneNumber
			clientUser.activated = customer.activated
			if (customer.deliveryAddresses) {
				clientUser.deliveryAddress = transformAddress(customer.deliveryAddresses.iterator().next())
			}
			if (customer.billingAddress) {
				clientUser.billingAddress = transformAddress(customer.billingAddress)
			}
			if (customer instanceof BusinessCustomer) {
				clientUser.companyName = customer.companyName
				clientUser.taxId = customer.taxId
				clientUser.companyId = customer.companyId
				clientUser.vat = customer.vat
				clientUser.contactPerson = customer.contactPerson
			}
			if (customer instanceof PrivateCustomer) {
				clientUser.firstName = customer.firstName
				clientUser.lastName = customer.lastName
			}
		}
		return clientUser;
	}

	Map transformBasket(Basket basket, boolean skipItems = false, String productCode = null) {
		def clientBasket = [:]
		if (basket.price) {
			clientBasket.price = renderPrice(basket.price)
			clientBasket.noDiscountPrice = renderPrice(basket.noDiscountPrice)
		}
		if (basket.appliedDiscounts) {
			clientBasket.appliedDiscounts = basket.appliedDiscounts.collect { discount ->
				[title: discount.title, description: discount.description]
			}
		}

		if (skipItems) {
			//skip rendering items
		} else if (productCode) { //render only a single item
			clientBasket['item'] = transformBasketItem(basket.basketItems.find { it.product.code == productCode })
		} else { //render all items
			clientBasket['items'] = basket.basketItems.collect { item ->
				transformBasketItem(item)
			}
		}
		return clientBasket;
	}

	Map transformBasketItem(BasketItem item) {
		def clientBasketItem = [
				product: [
						code: item.product.code,
						title: shopTagLib.title(object: item.product),
						price: renderPriceValue(item.product.price),
						imageUrlSmall: shopTagLib.objectImageUrl(object: item.product, type: ObjectImageType.SMALL),
						imageUrlMedium: shopTagLib.objectImageUrl(object: item.product, type: ObjectImageType.MEDIUM)
				],
				amount: item.amount
		]
		if (item.price) {
			clientBasketItem.price = renderPrice(item.price)
		}
		if (item.discountDescription) {
			clientBasketItem.discountDescription = item.discountDescription
		}
		return clientBasketItem
	}

	Map transformShipmentMethod(ShipmentMethod shipmentMethod) {
		[
				name: shopTagLib.printInLocale(strings: shipmentMethod.name),
				price: renderPrice(shipmentMethod.calculatedPrice),
				uid: shipmentMethod.uid
		]
	}

	Map transformPaymentMethod(PaymentMethod paymentMethod) {
		[
				name: shopTagLib.printInLocale(strings: paymentMethod.name),
				price: renderPrice(paymentMethod.calculatedPrice),
				uid: paymentMethod.uid
		]
	}

	Map transformAddress(Address address) {
		def result = [
				country: [
						code: address.country.name(),
						display: shopTagLib.countryDisplay(country: address.country)
				],
				city: address.city,
				postalCode: address.postalCode,
				street: address.street,
				number: address.number,
				comment: address.comment
		]
		if (address.firstName || address.lastName || address.companyName) {
			result.name = renderAddressName(address)
			if (address.firstName) result.firstName = address.firstName
			if (address.lastName) result.lastName = address.lastName
			if (address.companyName) result.companyName = address.companyName
		}
		return result
	}

	private String renderAddressName(Address address) {
		StringBuilder name = new StringBuilder()
		if (address.companyName) {
			name.append(address.companyName)
			if (address.firstName || address.lastName) {
				name.append(' (')
				if (address.firstName && address.lastName) name.append(address.firstName).append(' ').append(address.lastName)
				else if (address.firstName) name.append(address.firstName)
				else if (address.lastName) name.append(address.lastName)
				name.append(')')
			}
		} else {
			if (address.firstName && address.lastName) name.append(address.firstName).append(' ').append(address.lastName)
			else if (address.firstName) name.append(address.firstName)
			else if (address.lastName) name.append(address.lastName)
		}
	}

	Map renderPrice(Price price) {
		[
				base: renderPriceValue(price.base),
				vatPercent: renderPercent(price.vatPercent),
				vatAmount: renderPriceValue(price.vatAmount),
				baseWithVat: renderPriceValue(price.baseWithVat)
		]
	}

	String renderPriceValue(BigDecimal value) {
		grailsTagLib.formatNumber(number: value, type: 'number', minFractionDigits: 2, maxFractionDigits: 2) + ' ' + currency.symbol
	}

	String renderPercent(BigDecimal value) {
		grailsTagLib.formatNumber(number: value, type: 'percent', minFractionDigits: 2, maxFractionDigits: 2)
	}

	/*private void verifyAccessorThread() {
		if (accessorThread != null) {
			println "Verifying accessor thread: $accessorThread.id == ${Thread.currentThread().id}"
			assert accessorThread.id == Thread.currentThread().id
		} else {
			accessorThread = Thread.currentThread()
		}
	}*/

}
