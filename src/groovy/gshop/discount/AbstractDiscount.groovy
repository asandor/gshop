package gshop.discount

import gshop.ClientDataTransformer
import gshop.basket.Basket
import gshop.basket.BasketItem
import gshop.product.Product
import gshop.security.Customer

abstract class AbstractDiscount implements Discount {

	@Override
	void apply(Product product, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer) {
	}

	@Override
	void apply(Basket basket, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer) {
	}

	@Override
	void apply(BasketItem basketItem, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer) {
	}
}
