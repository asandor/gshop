package gshop.discount

import gshop.ClientDataTransformer
import gshop.basket.Basket
import gshop.basket.BasketItem
import gshop.product.Product
import gshop.security.Customer

public interface Discount {

	void apply(Product product, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer)

	void apply(Basket basket, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer)

	void apply(BasketItem basketItem, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer)
}