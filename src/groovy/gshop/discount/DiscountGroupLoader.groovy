package gshop.discount

import gshop.security.Customer
import gshop.security.DiscountGroup

class DiscountGroupLoader {

	List<DiscountGroup> loadDiscountGroups(Customer customer) {
		return DiscountGroup.createCriteria().list {
			customers {
				idEq(customer.id)
			}
		}
	}
}
