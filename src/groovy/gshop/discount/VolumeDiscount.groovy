package gshop.discount

import gshop.ClientDataTransformer
import gshop.basket.BasketItem
import gshop.basket.Price
import gshop.security.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.util.Assert

class VolumeDiscount extends AbstractDiscount {

	private BigDecimal vat;

	@Autowired
	VolumeDiscount(@Value('${shop.vat}') BigDecimal vat) {
		this.vat = vat
	}

	@Override
	void apply(BasketItem item, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer) {
		Assert.notNull(item)

		BigDecimal originalPrice = item.product.price * item.amount
		BigDecimal discountedPrice = item.product.price * item.amount
		BigDecimal discountValue = 0

		def discount = item.product.volumeDiscounts?.find { vd ->
			item.amount >= vd.fromAmount && (vd.toAmount == null || item.amount <= vd.toAmount)
		}
		if (discount != null) {
			if (discount.fixValue) {
				discountedPrice = discount.fixValue * item.amount
				discountValue = originalPrice - discountedPrice
			} else if (discount.percentage) {
				BigDecimal discountPercentage = discount.percentage / 100.0
				discountedPrice = originalPrice - (originalPrice * discountPercentage)
				discountValue = originalPrice - discountedPrice
			} else {
				throw new IllegalStateException("VolumeDiscount $discount for product $item.product has neither fixValue or percentage")
			}
		}

		BigDecimal netPrice = discountedPrice / (1 + vat)
		BigDecimal vatAmount = discountedPrice - netPrice
		item.price = new Price(netPrice, vat, vatAmount, discountedPrice)
		if (clientDataTransformer) {
			item.discountDescription = "-" + clientDataTransformer.renderPriceValue(discountValue)
		}
	}
}
