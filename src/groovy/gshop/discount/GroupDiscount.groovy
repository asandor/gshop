package gshop.discount

import groovy.util.logging.Log4j
import gshop.ClientDataTransformer
import gshop.basket.Basket
import gshop.security.Customer
import gshop.security.DiscountGroup

@Log4j
class GroupDiscount extends AbstractDiscount {

	DiscountGroupLoader discountGroupLoader = new DiscountGroupLoader()

	@Override
	void apply(Basket basket, Customer customer, Locale locale, ClientDataTransformer clientDataTransformer) {
		if (customer == null) return

		def discountGroups = discountGroupLoader.loadDiscountGroups(customer)

		if (discountGroups) {
			DiscountGroup highestDiscountGroup = discountGroups.max { it.discountPercentage }
			log.info("${highestDiscountGroup.discountPercentage * 100}% GroupDiscount applied to Basket")

			BigDecimal originalBase = basket.price.base
			BigDecimal priceBase = basket.price.base - (basket.price.base * highestDiscountGroup.discountPercentage)
			basket.price.modifyBase(priceBase)

			if (basket.appliedDiscounts == null) basket.appliedDiscounts = []
			String description = clientDataTransformer == null ? null :
				"-" + clientDataTransformer.renderPriceValue(originalBase - priceBase) +
						" (" + clientDataTransformer.renderPercent(highestDiscountGroup.discountPercentage) + ")"

			basket.appliedDiscounts << new Basket.DiscountInfo(
					title: highestDiscountGroup.getName(locale.language),
					description: description
			)
		}
	}
}